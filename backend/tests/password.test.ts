import { computePasswordHash } from "../src/security/password";

describe("testing password hashing", () => {
  test("valid hash", () => {
    expect(
      computePasswordHash("test", "6c5c12c0bc6ada2f62c8e60131ec151aca364884")
    ).toBe(
      "8871bb479885de8eb354ccd96d476cc818f70792aed9035fe7e0e68480572ab11ce69efb00c4b3f0000a556392ff418eaa46748600aadd33f31075288ca9c7b5"
    );
  });

  test("invalid hash", () => {
    expect(
      computePasswordHash("abc", "6c5c12c0bc6ada2f62c8e60131ec151aca364884")
    ).not.toBe(
      "8871bb479885de8eb354ccd96d476cc818f70792aed9035fe7e0e68480572ab11ce69efb00c4b3f0000a556392ff418eaa46748600aadd33f31075288ca9c7b5"
    );
  });
});
