import { AuthenticateError, Client, PublishPacket, Subscription } from "aedes";
import { JwtPayload, verify } from "jsonwebtoken";
import { db } from "../db";
import Device from "../db/model/device";
import { getUserIDBySession } from "../security/authentication";

/**
 * Implementation of the authentication for new MQTT clients.
 *
 * ### Format of the Client ID:
 * - if authenticating a device: `device#{MAC-Address}#{some-random-string}`
 * - if authenticating a user: `user#{user id}#{some-random-string}`
 *
 * The random string is needed to prevent issues caused by multiple connections.
 * It can be a random number, a UUID or anything else.
 *
 * @param client MQTT client, which tries to authenticate
 * @param _username username of the user - **currently not used in this implementation**
 * @param tokenBuffer Session ID of a user or JWT of a device
 * @param callback callback for successful or unsuccessful authentication
 */
export async function authenticate(
  client: Client,
  _username: Readonly<string>,
  tokenBuffer: Readonly<Buffer>,
  callback: (error: AuthenticateError | null, success: boolean | null) => void
): Promise<void> {
  if (tokenBuffer == undefined) {
    callback(_authError, false);
    return;
  }

  const token = tokenBuffer.toString();

  let valid = false;
  switch (client.id.split("#")[0]) {
    case "user":
      valid = await _validateUser(token, client.id);
      break;
    case "device":
      valid = await _validateDevice(token, client.id);
      break;
  }

  if (valid) {
    callback(null, true);
  } else {
    // if wrong type -> disallow
    callback(_authError, false);
  }
}

/**
 * Validating if client are allowed to publish data on a topic.
 *
 * ### Format of the Client ID:
 * > @see {@link authenticate}
 *
 * ### Validation rules:
 * - Devices can only publish on topics about themselves.
 * - User can only publish on topics about devices owned by them.
 * - Everybody can send ping signals
 *
 * @param client MQTT client, which tries to publish
 * @param packet data packet, the client tries to publish
 * @param callback allow or disallow the client to publish on this topic
 */
export async function authorizePublish(
  client: Client,
  packet: PublishPacket,
  callback: (error?: Error | null) => void
): Promise<void> {
  // allow on ping for everyone
  console.log(packet.topic);
  if(packet.topic == "ping") {
    callback(null);
    return;
  }

  const deviceMac: string = packet.topic.split("/")[0];
  const clientID: string = client.id.split("#")[1];

  let allow = false;

  switch (client.id.split("#")[0]) {
    case "user":
      allow = await _isUserOwner(clientID, deviceMac);
      break;
    case "device":
      allow = clientID == deviceMac;
      break;
  }

  if (allow) {
    callback(null);
  } else {
    callback(new Error("You are not allowed to publish on this topic"));
  }
}

/**
 * Validating if clients are allowed to subscribe to a topic.
 *
 * ### Format of the Client ID:
 * > @see {@link authenticate}
 *
 * ### Validation rules:
 * - Devices can only subscribe to topics about themselves.
 * - User can only subscribe to topics about devices owned by them.
 *
 * @param client MQTT client, which tries to subscribe
 * @param subscription subscription the client tries to create
 * @param callback allow or disallow the client to subscribe to this topic
 */
export async function authorizeSubscribe(
  client: Client,
  subscription: Subscription,
  callback: (error: Error | null, subscription?: Subscription | null) => void
): Promise<void> {
  const deviceMac: string = subscription.topic.split("/")[0];
  const clientID: string = client.id.split("#")[1];
  let allow = false;

  switch (client.id.split("#")[0]) {
    case "user":
      allow = await _isUserOwner(clientID, deviceMac);
      break;
    case "device":
      allow = clientID == deviceMac;
      break;
  }

  if (allow) {
    callback(null, subscription);
  } else {
    callback(new Error("You are not allowed to subscribe to this topic"));
  }
}

/**
 * Helper function to check, if a device and the JWT is valid.
 * It verifies the token and then fetches the device by its MAC-Address from the db.
 *
 * @param token JWT of the device
 * @param clientID ID of the MQTT Client
 * @returns whether the device and its JWT is valid
 */
async function _validateDevice(
  token: string,
  clientID: string
): Promise<boolean> {
  if (process.env.JWT_SECRET == undefined) {
    console.error("NO JWT SECRET FOUND");
    return false;
  }

  let parsed: string | JwtPayload;
  try {
    parsed = verify(token, process.env.JWT_SECRET ?? "");
  } catch (e) {
    return false;
  }

  if (typeof parsed != "object") {
    return false;
  }

  const device = await db
    .getRepository(Device)
    .createQueryBuilder()
    .select(["id", "mac"])
    .where("mac = :mac", { mac: parsed.sub })
    .cache(2 * 60 * 1000)
    .getRawOne();

  return (
    device != null &&
    device.mac == parsed.sub &&
    device.mac == clientID.split("#")[1]
  );
}

/**
 * Helper function to check, if the session id and the corresponding user are valid.
 *
 * @param token Session ID of the user
 * @param clientID ID of the MQTT client
 * @returns whether the user and the session id is valid
 */
async function _validateUser(token: string, clientID: string) {
  const userID = await getUserIDBySession(token);

  return (
    userID != undefined &&
    !Number.isNaN(userID) &&
    userID == parseInt(clientID.split("#")[1])
  );
}

/**
 * Helper function to check, if the user, given by the userID,
 * is the owner of the device with the given MAC-Address.
 *
 * @param userID ID of the user
 * @param mac MAC-Address of the device
 * @returns whether the user is owner of the device
 */
async function _isUserOwner(userID: string, mac: string): Promise<boolean> {
  const device = await db
    .getRepository(Device)
    .createQueryBuilder()
    .select(["userID"])
    .where("mac = :mac", { mac })
    .cache(true)
    .getRawOne();

  return device?.userID == userID;
}

/**
 * Return code for failed authentication.
 */
const _authError: AuthenticateError = {
  name: "AuthenticateError",
  message: "Authentication failed",
  returnCode: 4,
};
