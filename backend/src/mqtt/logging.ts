import { AedesPublishPacket, Client, Subscription } from "aedes";

/**
 * Debugging output for the MQTT connected event.
 * @param client new-connected client
 */
export function onConnect(client: Client): void {
  if (client === null) return; // caused by broker for internal use
  console.log("client connected", client?.id);
}

/**
 * Debugging output for the MQTT disconnected event.
 * @param client disconnected client
 */
export function onDisconnect(client: Client): void {
  if (client === null) return; // caused by broker for internal use
  console.log("client disconnected", client?.id);
}

/**
 * Debugging output for the MQTT onSubscribe event.
 * @param subscriptions list of topic the client just subscribed to
 * @param client mqtt client object
 */
export function onSubscribe(
  subscriptions: Subscription[],
  client: Client
): void {
  if (client === null) return; // caused by broker for internal use
  console.log(
    "subscribe",
    subscriptions.map((s) => s.topic).join("\n"),
    client?.id
  );
}

/**
 * Debugging output for the MQTT onUnsubscribe event
 * @param subscriptions list of topic the client just unsubscribed from
 * @param client mqtt client object
 */
export function onUnsubscribe(subscriptions: string[], client: Client): void {
  if (client === null) return; // caused by broker for internal use
  console.log("unsubscribe", subscriptions.join("\n"), client?.id);
}

/**
 * Debugging output for the MQTT onPublish event
 * @param packet data packet send by the client
 * @param client mqtt client that published the data
 */
export function onPublish(packet: AedesPublishPacket, client: Client): void {
  if (client === null) return; // caused by broker for internal use
  console.log("publish", packet.topic, client?.id, packet.payload.toString());
}
