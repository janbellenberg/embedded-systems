import aedes from "aedes";
import { createServer } from "net";
import { createServer as createWebSocketServer } from "aedes-server-factory";
import * as constraints from "./constraints";
import * as logging from "./logging";

const mqttPort = parseInt(process.env.MQTT_PORT ?? "1883");
const wsPort = parseInt(process.env.MQTT_WS_PORT ?? "1884");

/**
 * Creates a new MQTT server and a MQTT-WS server.
 */
export async function createMqttServer(): Promise<void> {
  // create new aedes instance with security constraints
  const instance = aedes({
    authorizePublish: constraints.authorizePublish,
    authorizeSubscribe: constraints.authorizeSubscribe,
    authenticate: constraints.authenticate,
  });

  const mqttServer = createServer(instance.handle);
  const wsServer = createWebSocketServer(instance, { ws: true });

  // start MQTT server
  await new Promise<void>((resolve) =>
    mqttServer.listen(mqttPort, "0.0.0.0", resolve)
  ).then(() => console.log(`> MQTT Server available at port ${mqttPort}`));

  // start MQTT server via WebSockets
  // -> needed by the web-version of the app,
  //    because no custom TCP packages can be sent from a browser
  await new Promise<void>((resolve) => wsServer.listen(wsPort, resolve)).then(
    () => console.log(`> MQTT Server (via WS) available at port ${wsPort}`)
  );

  // enable logging of events when in debug mode
  if (process.env["DEBUGGING"] !== undefined) {
    instance.on("client", logging.onConnect);
    instance.on("clientDisconnect", logging.onDisconnect);
    instance.on("subscribe", logging.onSubscribe);
    instance.on("unsubscribe", logging.onUnsubscribe);
    instance.on("publish", logging.onPublish);
  }
}
