import "dotenv/config";
import express from "express";
import { db } from "./db";
import { addApolloMiddleware } from "./graphql";
import { createMqttServer } from "./mqtt";
import { getRedisConnection } from "./redis";
import * as routes from "./rest";

// check if all needed env's were set.
if (process.env.JWT_SECRET === undefined) {
  throw new Error("JWT_SECRET is not defined");
}

process.on("uncaughtException", function (error) {
  console.log(error.stack);
});

/**
 * Entrypoint for the backend server. Creates connections to mariadb and redis and sets up the server.
 */
export async function main(): Promise<void> {
  await db
    .initialize()
    .then(() => console.log("> SQL connected"))
    .catch((error: unknown) => {
      console.log(error);
      process.exit(1);
    });

  // connect to redis db
  await getRedisConnection()
    .then(() => console.log("> Redis connected"))
    .catch((error: unknown) => {
      console.log(error);
      process.exit(1);
    });

  // initialize mqtt server
  await createMqttServer().catch((error: unknown) => {
    console.log(error);
    process.exit(1);
  });

  // initialize express server
  const app = express();
  app.use(express.json());

  // initialize routes
  app.get("/health", routes.health);

  // initialize GraphQL Server
  await addApolloMiddleware(app);

  const port = parseInt(process.env.PORT ?? "8080");
  app.listen({ port }, () => {
    console.log(
      `🚀 Server ready at http://${process.env["DOMAIN"]}:${port}/api`
    );
  });
}

console.log("Starting server...");

main().catch((error: unknown) => {
  console.log(error);
});
