import gql from "graphql-tag";

/**
 * GraphQL Schema
 */
const typeDefs = gql`
  scalar Void

  type Query {
    user: User!
    device(id: Int!): Device!
    program(id: Int!): Program!
    info: Info!
    validateJWT(token: String!): Boolean!
  }

  type Mutation {
    login(username: String!, password: String!): Session!
    logout: Void

    addUser(username: String!, password: String!): Session!
    updateUser(password: String, darkmode: Boolean): User!
    deleteUser: Void

    addDevice(mac: String!): Void
    activateDevice(mac: String!): String!
    updateDevice(id: ID!, name: String): Device!
    deleteDevice(id: ID!): Void

    addProgram(name: String!): Program!
    updateProgram(id: ID!, name: String, content: String): Program!
    deleteProgram(id: ID!): Void
  }

  type Session {
    sessionID: String!
  }

  type Info {
    appVersion: Float!
  }

  type User {
    id: Int!
    username: String!
    darkmode: Boolean!
    devices: [Device!]!
    programs: [Program!]!
  }

  type Device {
    id: Int!
    name: String!
    mac: String!
  }

  type Program {
    id: Int!
    name: String!
    content: String!
  }
`;

export default typeDefs;
