import {
  activateDevice,
  addDevice,
  deleteDevice,
  updateDevice,
} from "./mutations/device";
import { addProgram, deleteProgram, updateProgram } from "./mutations/program";
import { login, logout } from "./mutations/session";
import { addUser, deleteUser, updateUser } from "./mutations/user";
import { device, devicesOfUser, validateJWT } from "./queries/device";
import { info } from "./queries/info";
import { program, programsOfUser } from "./queries/program";
import { user } from "./queries/user";
import { Void } from "./scalar-void";

/**
 * Matching between the real resolver functions and the typedefs.
 */
export const resolvers = {
  Void: Void,
  Query: {
    user,
    device,
    program,
    info,
    validateJWT,
  },
  Mutation: {
    login,
    logout,
    addUser,
    updateUser,
    deleteUser,
    addDevice,
    activateDevice,
    updateDevice,
    deleteDevice,
    addProgram,
    updateProgram,
    deleteProgram,
  },
  User: {
    devices: devicesOfUser,
    programs: programsOfUser,
  },
};
