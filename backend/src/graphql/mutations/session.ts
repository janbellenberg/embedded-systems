import { GraphQLResolveInfo } from "graphql";
import { db } from "../../db";
import User from "../../db/model/user";
import { getRedisConnection } from "../../redis";
import {
  computePasswordHash,
  generateSessionID,
} from "../../security/password";
import AppContext from "../context";
import AuthenticationError from "../errors/authentication";

/**
 * GraphQL mutation to perform login.
 *
 * ### GraphQL Parameters
 * - `username`
 * - `password`
 *
 * @example
 * ```graphql
 * mutation Login($username: String!, $password: String!) {
 *  login(username: $username, password: $password) {
 *    sessionID
 *  }
 * }
 * ```
 *
 * @throws `AuthenticationError` if user submitted the wrong credentials.
 *
 * @param _parent parent request - **not used**
 * @param args parameters from the request
 * @param _context request context - **not used**
 * @param _info meta data from the request - **not used**
 * @returns Session ID for future authentication
 */
export async function login(
  _parent: unknown,
  args: LoginParams,
  _context: AppContext,
  _info: GraphQLResolveInfo
): Promise<Session> {
  // get user from req and load from db
  const { username, password } = args;
  const user = await db
    .getRepository(User)
    .createQueryBuilder()
    .select(["id", "password", "salt"])
    .where("username like :username", { username })
    .getRawOne();

  if (user == null) {
    throw new AuthenticationError("Benutzername oder Passwort ist falsch");
  }

  // check password
  const computedHash = computePasswordHash(password, user.salt);

  if (computedHash !== user.password) {
    throw new AuthenticationError("Benutzername oder Passwort ist falsch");
  }

  // generate session
  const sessionID = generateSessionID();

  // store session details in redis
  (await getRedisConnection()).execute([
    "SET",
    `session#${sessionID}`,
    user.id.toString(),
    "EX",
    "2592000", // expires after one month
  ]);

  // send session id via res
  return { sessionID };
}

/**
 * GraphQL mutation to perform logout.
 *
 * **No submission of the user id is necessary, because it uses the session id**
 *
 * @example
 * ```graphql
 * mutation Logout {
 *  logout
 * }
 * ```
 *
 * @throws `AuthenticationError` if no user is logged in.
 *
 * @param _parent parent request - **not used**
 * @param _args parameters from the request - **not used**
 * @param _context request context
 * @param _info meta data from the request - **not used**
 */
export async function logout(
  _parent: unknown,
  _args: unknown,
  context: AppContext,
  _info: GraphQLResolveInfo
): Promise<void> {
  const { sessionID, userID } = context;

  if (userID === undefined || sessionID === undefined) {
    throw new AuthenticationError("Bitte melden Sie sich an");
  }

  (await getRedisConnection()).execute(["DEL", `session#${sessionID}`]);
}

/**
 * Parameters for the login, including the username and the password.
 */
export interface LoginParams {
  username: string;
  password: string;
}

/**
 * Format for the created session as a result of login.
 */
export interface Session {
  sessionID: string;
}
