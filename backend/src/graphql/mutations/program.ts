import { GraphQLResolveInfo } from "graphql";
import { db } from "../../db";
import { getUser } from "../../db/dao/user";
import Program from "../../db/model/program";
import ByID from "../arg-types/by-id";
import AppContext from "../context";
import AuthenticationError from "../errors/authentication";
import NotFoundError from "../errors/notfound";
import { program as getProgram } from "../queries/program";

/**
 * GraphQL mutation for adding a new program with no blocks.
 *
 * ### GraphQL Parameters:
 * - `name` name of the program
 *
 * @example
 * ```graphql
 * mutation AddProgram($name: String!) {
 *  addProgram(name: $name) {
 *    id
 *  }
 * }
 * ```
 *
 * @throws `AuthenticationError` when user is not logged in.
 * @throws `NotFoundError` if the user was not found.
 *
 * @param _parent parent request - **not used**
 * @param args parameters from the request
 * @param context request context
 * @param _info meta data from the request - **not used**
 * @returns created program
 */
export async function addProgram(
  _parent: unknown,
  args: AddParams,
  context: AppContext,
  _info: GraphQLResolveInfo
): Promise<Program> {
  const { name } = args;
  const { userID } = context;

  // structure for an empty program
  const empty = {
    blocks: [],
  };

  // check if logged in
  if (userID == undefined) {
    throw new AuthenticationError("Sie sind nicht angemeldet.");
  }

  // fetch user from db
  const user = await getUser(userID, ["User.id"]);

  if (user == null) {
    throw new NotFoundError("Ihr Konto wurde nicht gefunden");
  }

  // create new program and store it on db,
  // then fetch its auto-generated ID and add it to the object
  // to return it
  const program: Program = new Program();
  program.name = name;
  program.content = JSON.stringify(empty);
  program.user = user;

  program.id = (await db.getRepository(Program).insert(program)).raw.insertId;

  return program;
}

/**
 * GraphQL mutation for updating a program.
 *
 * ### GraphQL Parameter:
 * - `id` ID of the program (to identify the program, not to be updated!)
 * - `name` new name of the program **(optional)**
 * - `content` new content of the program in JSON format  **(optional)**
 *
 * @example
 * ```graphql
 * mutation UpdateProgram($id: Int!, $name: String, $content: String) {
 *  updateProgram(id: $id, name: $name, content: $content) {
 *    id
 *    name
 *    content
 *  }
 * }
 * ```
 * @throws `AuthenticationError` if user is not logged in.
 * @throws `NotFoundError` if no program with the given ID exists.
 *
 * @param _parent parent request - **not used**
 * @param args parameters from the request
 * @param context request context
 * @param info meta data from the request
 * @returns updated program with specified fields
 */
export async function updateProgram(
  _parent: unknown,
  args: UpdateParams,
  context: AppContext,
  info: GraphQLResolveInfo
): Promise<Program> {
  const { userID } = context;
  const { id, name, content } = args;

  // check if logged in
  if (userID == undefined) {
    throw new AuthenticationError("Sie sind nicht angemeldet.");
  }

  // fetch current version from the db
  const program = await db.getRepository(Program).findOneBy({
    id: id,
    user: {
      id: userID,
    },
  });

  if (program == null) {
    throw new NotFoundError("Das Programm konnte nicht gefunden werden.");
  }

  // update name if given
  if (name != undefined) {
    program.name = name;
  }

  // update content if given
  if (content != undefined) {
    program.content = content;
  }

  // send updated entity to db
  await db.getRepository(Program).save(program);

  // call query resolver to fetch current version
  return getProgram(undefined, { id }, context, info);
}

/**
 * GraphQL mutation for deleting a program.
 *
 * ### GraphQL Parameters:
 * - `id` ID of the program
 *
 * @example
 * ```graphql
 * mutation DeleteProgram($id: Int!) {
 *  deleteProgram(id: $id)
 * }
 * ```
 *
 * @throws `AuthenticationError` if user is not logged in.
 * @throws `NotFoundError` if no program with the given ID exists.
 *
 * @param _parent parent request - **not used**
 * @param args parameters from the request
 * @param context request context
 * @param _info meta data from the request - **not used**
 */
export async function deleteProgram(
  _parent: unknown,
  args: ByID,
  context: AppContext,
  _info: GraphQLResolveInfo
): Promise<void> {
  const { userID } = context;
  const { id } = args;

  // check if logged in
  if (userID == undefined) {
    throw new AuthenticationError("Sie sind nicht angemeldet.");
  }

  // fetch current version from db
  // and then send delete request
  const program = await db.getRepository(Program).findOneBy({
    id: id,
    user: {
      id: userID,
    },
  });

  if (program == null) {
    throw new NotFoundError("Das Program konnte nicht gefunden werden");
  }

  await db.getRepository(Program).delete(id);
}

/**
 * File-specific interface for the mutation params,
 * which includes the name as a string.
 */
export interface AddParams {
  name: string;
}

/**
 * File-specific interface for mutation params,
 * which includes the ID, the optional name as a string and the optional content as JSON string.
 */
export interface UpdateParams {
  id: number;
  name?: string;
  content?: string;
}
