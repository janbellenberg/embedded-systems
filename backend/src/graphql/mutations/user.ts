import { GraphQLResolveInfo } from "graphql";
import { db } from "../../db";
import User from "../../db/model/user";
import { getRedisConnection } from "../../redis";
import {
  computePasswordHash,
  generateSalt,
  generateSessionID,
} from "../../security/password";
import AppContext from "../context";
import AuthenticationError from "../errors/authentication";
import DuplicateError from "../errors/duplicate";
import NotFoundError from "../errors/notfound";
import { LoginParams, Session } from "./session";
import { user as getUser } from "../queries/user";

/**
 * GraphQL mutation for creating a new user account and perform login.
 *
 * ### GraphQL Parameter:
 * - `username` Benutzername [String]
 * - `password` Password [String]
 *
 * @example
 * ```graphql
 * mutation addUser($username: String!, $password: String!) {
 *  addUser(username: $username, password: $password) {
 *    sessionID
 *  }
 * }
 * ```
 *
 * @throws `DuplicateError` if a user with the given username already exists.
 *
 * @param _parent parent request - **not used**
 * @param args parameters from the request
 * @param _context request context - **not used**
 * @param _info meta data from the request - **not used**
 */
export async function addUser(
  _parent: unknown,
  args: LoginParams,
  _context: unknown,
  _info: GraphQLResolveInfo
): Promise<Session> {
  const { username, password } = args;

  // create new user object
  const user: User = new User();
  user.username = username;
  user.salt = generateSalt();
  user.password = computePasswordHash(password, user.salt);

  // try adding it to the db
  try {
    user.id = (await db.getRepository(User).insert(user)).raw.insertId;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  } catch (e: any) {
    if (e.code === "ER_DUP_ENTRY") {
      throw new DuplicateError("Der Nutzername existiert bereits.");
    }

    throw e;
  }

  if(user.id === undefined) {
    throw new Error();
  }

  // generate new session
  const sessionID = generateSessionID();

  // store session in redis
  (await getRedisConnection()).execute([
    "SET",
    `session#${sessionID}`,
    user.id.toString(),
    "EX",
    "2592000", // one month to expire
  ]);

  // send session id as result
  return { sessionID };
}

/**
 * GraphQL mutation for updating the currently logged in user.
 *
 * ### GraphQL Parameter:
 * - `password` new password **optional** [String]
 * - `darkmode` whether the darkmode should be enabled **optional** [Boolean]
 *
 * @example
 * ```graphql
 * mutation UpdatePassword($password: String) {
 *  updateUser(password: $password)
 * }
 * ```
 *
 * @example
 * ```graphql
 * mutation UpdateDesign($darkmode: Boolean) {
 *  updateUser(darkmode: $darkmode) {
 *    darkmode
 *  }
 * }
 * ```
 *
 * @throws `AuthenticationError` if user is not logged in.
 * @throws `NotFoundError` if logged in user doesn't exist.
 *
 * @param _parent parent request - **not used**
 * @param args parameters from the request
 * @param context request context
 * @param info meta data from the request
 * @returns Updated instance of the device with requested parameters.
 */
export async function updateUser(
  _parent: unknown,
  args: UpdateParams,
  context: AppContext,
  info: GraphQLResolveInfo
): Promise<User> {
  const { userID } = context;
  const { password, darkmode } = args;

  // check if logged in
  if (userID == undefined) {
    throw new AuthenticationError("Sie sind nicht angemeldet.");
  }

  // fetch user from db
  const user: User | null = await db
    .getRepository(User)
    .findOneBy({ id: userID });

  if (user == null) {
    throw new NotFoundError("Ihr Konto wurde nicht gefunden");
  }

  // update password if given
  if (password) {
    user.salt = generateSalt();
    user.password = computePasswordHash(password, user.salt);
  }

  // update darkmode enabled if given
  if (darkmode != undefined) {
    user.darkmode = darkmode;
  }

  // persist changes
  await db.getRepository(User).save(user);

  // call query resolver for fetching requested fields
  return getUser(undefined, { userID }, context, info);
}

/**
 * GraphQL mutation for deleting the currently logged in user.
 *
 * **No GraphQL Parameters**
 *
 * @example
 * ```graphql
 * mutation DeleteUser() {
 *  deleteUser
 * }
 * ```
 *
 * @throws `AuthenticationError` if user is not logged in.
 *
 * @param _parent parent request - **not used**
 * @param _args parameters from the request - **not used**
 * @param context request context
 * @param _info meta data from the request - **not used**
 */
export async function deleteUser(
  _parent: unknown,
  _args: unknown,
  context: AppContext,
  _info: GraphQLResolveInfo
): Promise<void> {
  const { userID } = context;

  // check if logged in
  if (userID == undefined) {
    throw new AuthenticationError("Sie sind nicht angemeldet.");
  }

  // delete user
  await db.getRepository(User).delete(userID);
}

/**
 * File-specific interface for mutation params,
 * which includes the optional password and the optional darkmode.
 */
export interface UpdateParams {
  password?: string;
  darkmode?: boolean;
}
