import { GraphQLResolveInfo } from "graphql";
import { sign } from "jsonwebtoken";

import Device from "../../db/model/device";
import ByID from "../arg-types/by-id";
import AppContext from "../context";
import { getUser } from "../../db/dao/user";
import { db } from "../../db";
import { device as getDevice } from "../queries/device";
import { getRedisConnection } from "../../redis";
import AuthenticationError from "../errors/authentication";
import ValidationError from "../errors/validation";
import DuplicateError from "../errors/duplicate";
import NotFoundError from "../errors/notfound";

/**
 * GraphQL mutation for adding a new device by its mac address.
 *
 * ### GraphQL Parameter:
 * - `mac` MAC-Address [String]
 *
 * @example
 * ```graphql
 * mutation AddDevice($mac: String!) {
 *  addDevice(mac: $mac)
 * }
 * ```
 *
 * @throws `AuthenticationError` when user is not logged in (userID is undefined).
 * @throws `ValidationError` if MAC Address is not valid. (valid example: **aa:bb:cc:dd:ee:ff**).
 *
 * @param _parent parent request - **not used**
 * @param args parameters from the request
 * @param context request context
 * @param _info meta data from the request - **not used**
 */
export async function addDevice(
  _parent: unknown,
  args: AddParams,
  context: AppContext,
  _info: GraphQLResolveInfo
): Promise<void> {
  const { mac } = args;
  const { userID } = context;

  // check, if user is logged in
  if (userID == undefined) {
    throw new AuthenticationError("Sie sind nicht angemeldet.");
  }

  // validate mac
  const regex = /^(([a-fA-F0-9]{2}:){5}[a-fA-F0-9]{2})$/;
  if (!regex.test(mac)) {
    throw new ValidationError("MAC-Adresse ist nicht korrekt");
  }

  // check if device already exists
  const { count } = await db
    .getRepository(Device)
    .createQueryBuilder()
    .select("count(*)", "count")
    .where("mac = :mac", { mac })
    .getRawOne();

  if (count != 0) {
    throw new DuplicateError("Das Gerät existiert bereits.");
  }

  // store mac in redis, until it will be activated by the device
  await (
    await getRedisConnection()
  ).execute([
    "SET",
    `device#${mac}`,
    userID.toString(),
    "EX",
    "300", // 5 minutes to expiration
  ]);
}

/**
 * GraphQL mutation for activating a device.
 * This mutation should only be called by a device.
 *
 * ### GraphQL Parameter:
 * - `mac` MAC-Address [String]
 *
 * @example
 * ```graphql
 * mutation ActivateDevice($mac: String!) {
 *  activateDevice(mac: $mac)
 * }
 * ```
 *
 * @throws `Error` when no `JWT_SECRET` was defined in the envs.
 * @throws `ValidationError` if MAC Address is not valid. (valid example: **aa:bb:cc:dd:ee:ff**).
 * @throws `NotFoundError` if MAC Address was not registered within the last five minutes.
 * @throws `NotFoundError` if the user was not found anymore.
 * @throws `DuplicateError` if a device with the MAC Address already exists.
 *
 * @param _parent parent request - **not used**
 * @param args parameters from the request
 * @param _context request context - **not used**
 * @param _info meta data from the request - **not used**
 *
 * @returns JWT for authentication
 */
export async function activateDevice(
  _parent: unknown,
  args: AddParams,
  _context: AppContext,
  _info: GraphQLResolveInfo
): Promise<string> {
  const { mac } = args;

  // verify that there is a JWT_SECRET defined
  if (process.env.JWT_SECRET == undefined) {
    throw new Error("Internal error");
  }

  // validate mac
  const regex = /^(([a-fA-F0-9]{2}:){5}[a-fA-F0-9]{2})$/;
  if (!regex.test(mac)) {
    throw new ValidationError("MAC-Adresse ist nicht korrekt");
  }

  // fetch corresponding user id from redis
  // -> see addDevice mutation
  const userID: number = await (
    await getRedisConnection()
  ).execute(["GET", `device#${mac}`]);

  // fetch user object
  const user = await getUser(userID, ["User.id"]);

  if (user == null) {
    throw new NotFoundError("Das Konto wurde nicht gefunden");
  }

  // create new device with default name
  const device: Device = new Device();
  device.user = user;
  device.mac = mac;
  device.name = "Neues Gerät";

  // try to add device to the db and fetch its generated id
  try {
    device.id = (await db.getRepository(Device).insert(device)).raw.insertId;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  } catch (e: any) {
    if (e.code === "ER_DUP_ENTRY") {
      throw new DuplicateError("Das Gerät existiert bereits.");
    }

    throw e;
  }

  // delete redis entry from addDevice mutation
  await (await getRedisConnection()).execute(["DEL", `device#${mac}`]);

  // generate valid JWT for authentication
  return sign({ sub: mac }, process.env.JWT_SECRET);
}

/**
 * GraphQL mutation for updating a device.
 *
 * ### GraphQL Parameter:
 * - `id` ID of the device (to identify the device, not to be updated!)
 * - `name` new name of the device
 *
 * @example
 * ```graphql
 * mutation UpdateDevice($id: Int!, $name: String) {
 *  updateDevice(id: $id, name: $name) {
 *    id
 *    mac
 *    name
 *  }
 * }
 * ```
 *
 * @throws `AuthenticationError` if user is not logged in.
 * @throws `NotFoundError` if no device with the given ID exists.
 *
 * @param _parent parent request - **not used**
 * @param args parameters from the request
 * @param context request context
 * @param info meta data from the request
 * @returns Updated instance of the device with requested parameters.
 */
export async function updateDevice(
  _parent: unknown,
  args: UpdateParams,
  context: AppContext,
  info: GraphQLResolveInfo
): Promise<Device> {
  const { userID } = context;
  const { id, name } = args;

  // check if user is logged in
  if (userID == undefined) {
    throw new AuthenticationError("Sie sind nicht angemeldet.");
  }

  // find device by id and user id
  // this query makes sure that the device is owned by the user,
  // because otherwise it would find no result
  const device = await db.getRepository(Device).findOneBy({
    id: id,
    user: {
      id: userID,
    },
  });

  if (device == null) {
    throw new NotFoundError("Der Roboter konnte nicht gefunden werden.");
  }

  // Update name, if given
  if (name != undefined) {
    device.name = name;
  }

  // send updated entity to the server
  await db.getRepository(Device).save(device);

  // call query resolver to fetch device
  return getDevice(undefined, { id }, context, info);
}

/**
 * GraphQL mutation for deleting a device.
 *
 * ### GraphQL Parameter:
 * - `id` ID of the device
 *
 * @example
 * ```graphql
 * mutation DeleteDevice($id: Int!) {
 *  deleteDevice(id: $id)
 * }
 * ```
 *
 * @throws `AuthenticationError` if user is not logged in.
 * @throws `NotFoundError` if no device with the given ID exists.
 *
 * @param _parent parent request - **not used**
 * @param args parameters from the request
 * @param context request context
 * @param _info meta data from the request - **not used**
 */
export async function deleteDevice(
  _parent: unknown,
  args: ByID,
  context: AppContext,
  _info: GraphQLResolveInfo
): Promise<void> {
  const { userID } = context;
  const { id } = args;

  // check if user is logged in
  if (userID == undefined) {
    throw new AuthenticationError("Sie sind nicht angemeldet.");
  }

  // find device by id and user id
  // this query makes sure that the device is owned by the user,
  // because otherwise it would find no result
  const device = await db.getRepository(Device).findOneBy({
    id: id,
    user: {
      id: userID,
    },
  });

  if (device == null) {
    throw new NotFoundError("Der Roboter konnte nicht gefunden werden.");
  }

  // send delete request to the db
  await db.getRepository(Device).delete(id);
}

/**
 * File-specific interface for the mutation params,
 * which includes the MAC Address as a string.
 */
export interface AddParams {
  mac: string;
}

/**
 * File-specific interface for mutation params,
 * which includes the ID and the optional name as a string.
 */
export interface UpdateParams {
  id: number;
  name?: string;
}
