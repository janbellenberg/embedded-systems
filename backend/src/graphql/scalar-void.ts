import { GraphQLScalarType } from "graphql";

/**
 * Representation of the `Void` type in the GraphQL type definitions.
 */
export const Void = new GraphQLScalarType({
  name: "Void",

  description: "Represents NULL values",

  serialize() {
    return null;
  },

  parseValue() {
    return null;
  },

  parseLiteral() {
    return null;
  },
});
