import { GraphQLResolveInfo } from "graphql";
import AppContext from "../context";

/**
 * GraphQL query for getting information about the application.
 *
 * @example
 * ```graphql
 * mutation Info {
 *  info {
 *    appVersion
 *  }
 * }
 * ```
 *
 * @param _parent parent request - **not used**
 * @param _args parameters from the request - **not used**
 * @param _context request context - **not used**
 * @param _info meta data from the request - **not used**
 * @returns information about the application
 */
export function info(
  _parent: unknown,
  _args: unknown,
  _context: AppContext,
  _info: GraphQLResolveInfo
): Info {
  return {
    appVersion: 0.9,
  };
}

export interface Info {
  appVersion: number;
}
