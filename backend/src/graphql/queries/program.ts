import ByID from "../arg-types/by-id";
import { GraphQLResolveInfo } from "graphql";
import AppContext from "../context";
import Program from "../../db/model/program";
import { getFieldsFromInfo } from "../fields";
import { getProgram, getProgramsOfUser } from "../../db/dao/program";
import AuthenticationError from "../errors/authentication";
import NotFoundError from "../errors/notfound";

/**
 * GraphQL Query for fetching information about a program.
 *
 * ### GraphQL Parameter:
 * - `id` ID of the program [Int]
 *
 * @example
 * ```graphql
 * query Program($id: Int!) {
 *  program(id: $id) {
 *    content
 *  }
 * }
 * ```
 *
 * @throws `AuthenticationError` if user is not logged in.
 * @throws `NotFoundError` if program was not found or is not owned by the user.
 *
 * @param _parent parent request - **not used**
 * @param args parameters for the request
 * @param context request context
 * @param info meta data from the request
 * @returns program with the `ID` with the fields requested in `info`
 */
export async function program(
  _parent: unknown,
  args: ByID,
  context: AppContext,
  info: GraphQLResolveInfo
): Promise<Program> {
  const { id } = args;
  const { userID } = context;

  // check if logged in
  if (userID == undefined) {
    throw new AuthenticationError("Sie sind nicht angemeldet!");
  }

  // fetch requested information about the device
  const fields = getFieldsFromInfo(Program, info);
  if (fields.length < 1) return new Program();

  const result = await getProgram(id, userID, fields);

  // throw exception if no robot was found
  if (result == null) {
    throw new NotFoundError("Das Program konnte nicht gefunden werden!");
  }

  return result;
}

/**
 * GraphQL Query for fetching all programs of the logged in user.
 *
 * @example
 * ```graphql
 * query User() {
 *  user {
 *    program {
 *      id
 *      name
 *    }
 *  }
 * }
 * ```
 *
 * @throws `AuthenticationError` if user is not logged in.
 *
 * @param _parent parent request - **not used**
 * @param _args parameters for the request - **not used**
 * @param context request context
 * @param info meta data from the request
 * @returns list of programs of the logged in user with the fields requested in `info`
 */
export async function programsOfUser(
  _parent: unknown,
  _args: unknown,
  context: AppContext,
  info: GraphQLResolveInfo
): Promise<Program[]> {
  const { userID } = context;

  // check if logged in
  if (userID == undefined) {
    throw new AuthenticationError("Sie sind nicht angemeldet!");
  }

  // fetch list of programs for the user
  // with the information specified in the `info` object
  const fields = getFieldsFromInfo(Program, info);
  if (fields.length < 1) return [];

  const result = await getProgramsOfUser(userID, fields);

  return result;
}
