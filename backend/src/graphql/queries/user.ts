import { GraphQLResolveInfo } from "graphql";
import AppContext from "../context";
import User from "../../db/model/user";
import { getUser } from "../../db/dao/user";
import { getFieldsFromInfo } from "../fields";
import AuthenticationError from "../errors/authentication";
import NotFoundError from "../errors/notfound";

/**
 * GraphQL Query for fetching information about the currently logged in user.
 *
 * @example
 * ```graphql
 * query User() {
 *  user {
 *    username
 *    darkmode
 *  }
 * }
 * ```
 *
 * @throws `AuthenticationError` if user is not logged in.
 * @throws `NotFoundError` if user was not found.
 *
 * @param _parent parent request - **not used**
 * @param _args parameters for the request - **not used**
 * @param context request context
 * @param info meta data from the request
 * @returns logged in user with the fields requested in `info`
 */
export async function user(
  _parent: unknown,
  _args: unknown,
  context: AppContext,
  info: GraphQLResolveInfo
): Promise<User> {
  const { userID } = context;

  if (userID == undefined) {
    throw new AuthenticationError("Sie sind nicht angemeldet.");
  }

  const fields = getFieldsFromInfo(User, info);
  if (fields.length < 1) return new User();

  const result = await getUser(userID, fields);

  if (result == null) {
    throw new NotFoundError("Der Benutzer konnte nicht gefunden werden.");
  }

  return result;
}
