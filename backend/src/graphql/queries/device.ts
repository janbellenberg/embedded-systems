import { GraphQLResolveInfo } from "graphql";
import AppContext from "../context";
import { getFieldsFromInfo } from "../fields";
import Device from "../../db/model/device";
import { deviceExists, getDevice, getDevicesOfUser } from "../../db/dao/device";
import ByID from "../arg-types/by-id";
import AuthenticationError from "../errors/authentication";
import NotFoundError from "../errors/notfound";
import { verify } from "jsonwebtoken";

/**
 * GraphQL Query for fetching information about a device.
 *
 * ### GraphQL Parameter:
 * - `id` ID of the device [Int]
 *
 * @example
 * ```graphql
 * query Device($id: Int!) {
 *  device(id: $id) {
 *    name
 *    mac
 *  }
 * }
 * ```
 *
 * @throws `AuthenticationError` if user is not logged in.
 * @throws `NotFoundError` if device was not found or is not owned by the user.
 *
 * @param _parent parent request - **not used**
 * @param args parameters for the request
 * @param context request context
 * @param info meta data from the request
 * @returns device with the `ID` with the fields requested in `info`
 */
export async function device(
  _parent: unknown,
  args: ByID,
  context: AppContext,
  info: GraphQLResolveInfo
): Promise<Device> {
  const { id } = args;
  const { userID } = context;

  // check if logged in
  if (userID == undefined) {
    throw new AuthenticationError("Sie sind nicht angemeldet");
  }

  // fetch requested information about the device
  const fields = getFieldsFromInfo(Device, info);
  if (fields.length < 1) return new Device();

  const result = await getDevice(id, userID, fields);

  // throw exception if no robot was found.
  if (result == null) {
    throw new NotFoundError("Der Roboter konnte nicht gefunden werden.");
  }

  return result;
}

/**
 * GraphQL Query for fetching all devices of the logged in user.
 *
 * @example
 * ```graphql
 * query User() {
 *  user {
 *    devices {
 *      id
 *      name
 *      mac
 *    }
 *  }
 * }
 * ```
 *
 * @throws `AuthenticationError` if user is not logged in.
 *
 * @param _parent parent request - **not used**
 * @param _args parameters for the request - **not used**
 * @param context request context
 * @param info meta data from the request
 * @returns list of devices of the logged in user with the fields requested in `info`
 */
export async function devicesOfUser(
  _parent: unknown,
  _args: unknown,
  context: AppContext,
  info: GraphQLResolveInfo
): Promise<Device[]> {
  const { userID } = context;

  // check if user is logged in
  if (userID == undefined) {
    throw new AuthenticationError("Sie sind nicht angemeldet");
  }

  // fetch list of devices for the user
  // with the information specified in the `info` object
  const fields = getFieldsFromInfo(Device, info);
  if (fields.length < 1) return [];
  const result = await getDevicesOfUser(userID, fields);
  return result;
}


/**
 * GraphQL Query for validating a JWT.
 *
 * ### GraphQL Parameter:
 * - `mac` MAC Address of the device [String]
 *
 * @example
 * ```graphql
 * query ValidateJWT($token: String!) {
 *  validateJWT(token: $token) 
 * }
 * ```
 *
 * @param _parent parent request - **not used**
 * @param args parameters for the request
 * @param _context request context - **not used**
 * @param _info meta data from the request - **not used**
 * @returns whether the JWT is valid and the device exists 
 */
export async function validateJWT(
  _parent: unknown,
  args: JwtArgs,
  _context: unknown,
  _info: GraphQLResolveInfo
): Promise<boolean> {
  try {
    const token = verify(args.token, process.env.JWT_SECRET ?? "");
    if(typeof(token) !== "object") return false;

    const exists = await deviceExists(token["sub"] ?? "");
    return exists;    
    
  } catch (e) {
    return false;
  }
}

interface JwtArgs {
  token: string;
}
