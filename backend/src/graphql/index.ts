import { ApolloServer } from "@apollo/server";
import { expressMiddleware } from "@apollo/server/express4";
import { Express } from "express-serve-static-core";
import cors from "cors";
import { json } from "body-parser";

import typeDefs from "./type-defs";
import AppContext from "./context";
import { httpStatusPlugin } from "./plugins/httpStatus";
import { resolvers } from "./resolver";
import { getUserIDBySession } from "../security/authentication";

/**
 * Starts a new instance of the apollo server and adds it as middleware to the express server instance.
 * @param app Instance of the express app
 */
export async function addApolloMiddleware(app: Express) {
  const graphql = new ApolloServer<AppContext>({
    typeDefs,
    resolvers,
    plugins: [httpStatusPlugin],
  });

  await graphql.start();

  app.use(
    "/api/graphql",
    cors<cors.CorsRequest>(),
    json(),
    expressMiddleware(graphql, {
      context: async ({req}) => {
        const token = req.headers["session"]?.toString();
        const userID = await getUserIDBySession(token ?? "");
        const context: AppContext = {
          userID,
          sessionID: token,
        };
  
        return context;
      },
    })
  );
}
