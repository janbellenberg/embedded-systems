/**
 * Interface for GraphQL resolver parameters, when only an ID is given.
 */
interface ByID {
  id: number;
}

export default ByID;
