import { IEntity } from "../db/IEntity";
import { FieldNode, GraphQLResolveInfo } from 'graphql';

/**
 * Extracts the queried fields from the request and filters forbidden fields.
 * @see IEntity
 * @param entity Class type of the entity
 * @param info `info` object from Apollo GraphQL resolver functions
 * @returns names of the attributes that are asked by the GraphQL request but not excluded by the entity
 */
export function getFieldsFromInfo<T extends IEntity>(
  entity: { new (): T },
  info: GraphQLResolveInfo
): string[] {

  const fields: string[] = [];
  
  for(const node of info.fieldNodes) {
    if(node.selectionSet === undefined) continue;
    for(const selection of node.selectionSet.selections) {
      fields.push((selection as FieldNode).name.value);
    }
  }

  const excludedFields = [
    ...entity.prototype.getExcludedFields(),
    "__typename",
  ];

  return fields
    .filter((field) => !excludedFields.includes(field))
    .map((field) => `${entity.name}.${field}`);
}
