/**
 * Interface implemented by database entities, so that they can used by GraphQL Resolvers.
 */
export interface IEntity {
  /**
   * Gives a list of attributes, which may not be queried by the user or at least not be queried in a normal query, like foreign keys.
   * @returns list of names of attribute, which should be excluded from sql queries.
   * @example 
   * ```ts
   * getExcludedFields(): string[] {
      return ["devices", "programs", "password"];
    }
   * ```
   */
  getExcludedFields(): string[];
}
