import { db } from "..";
import Program from "../model/program";

/**
 * Data-Access function that finds a program via its `id`.
 *
 * @param id id of the program
 * @param userID id of the user
 * @param fields list of attributes, that should be queried
 * @returns program object matching the *id* or null, if not found
 */
export function getProgram(
  id: number,
  userID: number,
  fields: string[]
): Promise<Program | null> {
  return db
    .getRepository(Program)
    .createQueryBuilder()
    .select(fields)
    .where("Program.id = :id", { id: id })
    .andWhere("Program.user = :uid", { uid: userID })
    .getOne();
}

/**
 * Data-Access function that finds all programs owned by a user.
 * If the user was not found, an empty list will be returned.
 *
 * @param userID id of the user
 * @param fields list of attributes, that should be queries
 * @returns list of program object owned by the user, may be empty if user not found
 */
export function getProgramsOfUser(
  userID: number,
  fields: string[]
): Promise<Program[]> {
  return db
    .getRepository(Program)
    .createQueryBuilder()
    .select(fields)
    .where("Program.user = :uid", { uid: userID })
    .getMany();
}
