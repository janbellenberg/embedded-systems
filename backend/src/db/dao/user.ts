import { db } from "..";
import User from "../model/user";

/**
 * Data-Access function that finds a user via its **id**.
 *
 * @param id id of the user
 * @param fields list of attributes, that should be queried
 * @returns user object matching the *id* or null, if not found
 */
export function getUser(id: number, fields: string[]): Promise<User | null> {
  return db
    .getRepository(User)
    .createQueryBuilder()
    .select(fields)
    .where("id = :id", { id })
    .getOne();
}
