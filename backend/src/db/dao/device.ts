import { db } from "..";
import Device from "../model/device";

/**
 * Data-Access function that finds a device via its `id`.
 *
 * @param id id of the device
 * @param userID id of the user
 * @param fields list of attributes, that should be queried
 * @returns device object matching the *id* or null, if not found
 */
export function getDevice(
  id: number,
  userID: number,
  fields: string[]
): Promise<Device | null> {
  return db
    .getRepository(Device)
    .createQueryBuilder()
    .select(fields)
    .where("Device.id = :id", { id: id })
    .andWhere("Device.user = :uid", { uid: userID })
    .getOne();
}

/**
 * Data-Access function that check, if a device exists by its `id` 
 *
 * @param id id of the device
 * @returns whether the device exists
 */
export async function deviceExists(
  mac: string,
): Promise<boolean> {
  return (await db
    .getRepository(Device)
    .createQueryBuilder()
    .where("Device.mac = :mac", { mac: mac })
    .getOne()) != null;
}

/**
 * Data-Access function that finds all devices owned by a user.
 * If the user was not found, an empty list will be returned.
 *
 * @param userID id of the user
 * @param fields list of attributes, that should be queried
 * @returns list of device object owned by the user, may be empty if user not found
 */
export async function getDevicesOfUser(
  userID: number,
  fields: string[]
): Promise<Device[]> {
  return db
    .getRepository(Device)
    .createQueryBuilder()
    .select(fields)
    .where("Device.user = :uid", { uid: userID })
    .getMany();
}
