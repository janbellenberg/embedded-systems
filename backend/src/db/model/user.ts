import { Column, Entity, PrimaryColumn, OneToMany } from "typeorm";
import { IEntity } from "../IEntity";
import Device from "./device";
import Program from "./program";

/**
 * Representation of the table `users`
 */
@Entity("users", { schema: "nobot" })
class User implements IEntity {
  /**
   * Auto-generated ID of the user.
   * Primary Key
   */
  @PrimaryColumn()
  id?: number;

  /**
   * Unique username, used during login.
   */
  @Column()
  username?: string;

  /**
   * Scrypt Hash of the user's password.
   */
  @Column()
  password?: string;

  /**
   * Salt value for the Scrypt password hashing.
   * 20 chars.
   */
  @Column()
  salt?: string;

  /**
   * Boolean, whether the user did enable the darkmode.
   */
  @Column()
  darkmode?: boolean;

  /**
   * List of devices owned by the user.
   */
  @OneToMany(() => Device, (device) => device.user)
  devices?: Device[];

  /**
   * List of programs owned by the user.
   */
  @OneToMany(() => Program, (program) => program.user)
  programs?: Program[];

  /**
   * @inheritDoc
   */
  getExcludedFields(): string[] {
    return ["devices", "programs"];
  }
}

export default User;
