import { IEntity } from "../IEntity";
import { Column, PrimaryColumn, ManyToOne, JoinColumn, Entity } from "typeorm";
import User from "./user";

/**
 * Representation of the table `devices`.
 */
@Entity("devices", { schema: "nobot" })
class Device implements IEntity {
  /**
   * Auto-generated ID of the device.
   * Primary Key
   */
  @PrimaryColumn()
  id?: number;

  /**
   * User-defined name of the device.
   * Max 50 chars
   */
  @Column()
  name?: string;

  /**
   * MAC-Address of the device.
   * 17 chars
   */
  @Column()
  mac?: string;

  /**
   * User, that owns this device.
   * Foreign Key via field `userID`.
   * Delete and Updated **CASCADE**
   */
  @ManyToOne(() => User, (user) => user.devices, {
    onDelete: "CASCADE",
    onUpdate: "CASCADE",
  })
  @JoinColumn({ name: "userID" })
  user?: User;

  /**
   * @inheritDoc
   */
  getExcludedFields(): string[] {
    return ["user"];
  }
}

export default Device;
