import { Column, Entity, PrimaryColumn, ManyToOne, JoinColumn } from "typeorm";
import { IEntity } from "../IEntity";
import User from "./user";

/**
 * Representation of the table `programs`.
 */
@Entity("programs", { schema: "nobot" })
class Program implements IEntity {
  /**
   * Auto-generated ID of a program.
   * Primary Key
   */
  @PrimaryColumn()
  id?: number;

  /**
   * User-defined name of the program.
   * Max 50 chars
   */
  @Column()
  name?: string;

  /**
   * Representation of the blocks in JSON format.
   */
  @Column()
  content?: string;

  /**
   * User, that owns this program.
   * Foreign Key via field `userID`
   * Delete and Updated **CASCADE**
   */
  @ManyToOne(() => User, (user) => user.programs, {
    onDelete: "CASCADE",
    onUpdate: "CASCADE",
  })
  @JoinColumn({ name: "userID" })
  user?: User;

  /**
   * @inheritDoc
   */
  getExcludedFields(): string[] {
    return ["device"];
  }
}

export default Program;
