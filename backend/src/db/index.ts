import { DataSource } from "typeorm";
import { RedisCache } from "../redis/cache";
import Device from "./model/device";
import Program from "./model/program";
import User from "./model/user";

/**
 * MariaDB SQL Datasource for `nobot` database.
 */
export const db = new DataSource({
  type: "mariadb",
  host: process.env.MYSQL_HOST ?? "localhost",
  port: parseInt(process.env.MYSQL_PORT ?? "3306"),
  username: process.env.MYSQL_USER,
  password: process.env.MYSQL_PASSWORD,
  database: "nobot",
  synchronize: false,
  logging: process.env["DEBUGGING"] !== undefined,
  entities: [Device, Program, User],
  migrations: [],
  subscribers: [],
  cache: {
    provider(connection) {
      return new RedisCache(connection);
    },
    duration: 5 * 60 * 1000, // 5 minutes default cache time
  },
});
