import { db } from "../db";
import User from "../db/model/user";
import { getRedisConnection } from "../redis";

/**
 * Validates a session id and looks the according user id up.
 *
 * @param session session id, given by the user
 * @returns ID of the logged-in user, or undefined if session id is invalid
 */
export async function getUserIDBySession(
  session: string
): Promise<number | undefined> {
  const result = await (await getRedisConnection()).get(`session#${session}`);
  if (result === null) {
    return undefined;
  }

  const dbResult = await db
    .getRepository(User)
    .createQueryBuilder()
    .select("id")
    .where("id = :id", { id: result })
    .cache(2 * 60 * 1000) // 2 Minutes
    .getRawOne();

  if (dbResult === undefined) {
    return undefined;
  }

  return dbResult.id;
}
