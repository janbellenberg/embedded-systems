import { randomBytes, scryptSync } from "crypto";

/**
 * Hashes the password with the salt using the `scrypt` algorithm.
 *
 * @param raw not-hashed password
 * @param salt salt-string from the database
 * @returns hashed password in hex format
 */
export function computePasswordHash(raw: string, salt: string): string {
  return scryptSync(raw, salt, 64).toString("hex");
}

/**
 * Generates a random 20-character string in hex format, that can be used as salt for password hashing.
 *
 * @returns salt value
 */
export function generateSalt(): string {
  return randomBytes(20).toString("hex");
}

/**
 * Generated a random 50-character string in hex format, that can be used as a session id.
 *
 * @returns session id
 */
export function generateSessionID(): string {
  return randomBytes(50).toString("hex");
}
