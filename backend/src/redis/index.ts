import { Client } from "redis-om";

let redis: Client | undefined;

/**
 * Creates and returns a singleton redis connection.
 * @returns the redis connection
 */
export async function getRedisConnection(): Promise<Client> {
  if (redis === undefined) {
    redis = new Client();
    const url = getRedisConnectionURL();
    await redis.open(url);
  }

  return redis;
}

/**
 * Builds a connection string for the redis connection.
 *
 * **Parameters from env:**
 * - REDIS_HOST
 * - REDIS_PORT
 * - REDIS_USER
 * - REDIS_PASSWORD
 *
 * @returns formatted redis connection string
 */
export function getRedisConnectionURL(): string {
  const host = process.env.REDIS_HOST ?? "localhost";
  const port = parseInt(process.env.REDIS_PORT ?? "6379");
  const user = process.env.REDIS_USER ?? "default";
  const password = process.env.REDIS_PASSWORD;

  return `redis://${user}:${password}@${host}:${port}`;
}
