import { Client } from "redis-om";
import { DataSource, QueryRunner } from "typeorm";
import { QueryResultCache } from "typeorm/cache/QueryResultCache";
import { QueryResultCacheOptions } from "typeorm/cache/QueryResultCacheOptions";
import { getRedisConnectionURL } from ".";

/**
 * TypeORM query result cache using redis.
 */
export class RedisCache implements QueryResultCache {
  constructor(private _dataSource: DataSource) {}

  private redis = new Client();
  private prefix = "#cache#";

  /**
   * Opens a connection to the redis server.
   */
  async connect(): Promise<void> {
    const url = getRedisConnectionURL();

    await this.redis
      .open(url)
      .then(() => console.log("> Cache connected"))
      .then(() => this.clear())
      .catch((error: unknown) => {
        console.log(error);
        process.exit(1);
      });
  }

  /**
   * Instruct redis to persist the data and then closes the connection.
   */
  async disconnect(): Promise<void> {
    await this.redis.execute(["save"]);
    await this.redis.close();
  }

  /**
   * Clears the entire cache for schema synchronization.
   */
  async synchronize(_queryRunner?: QueryRunner | undefined): Promise<void> {
    this.clear();
  }

  /**
   * @inheritDoc
   */
  async getFromCache(
    options: QueryResultCacheOptions,
    _queryRunner?: QueryRunner | undefined
  ): Promise<QueryResultCacheOptions | undefined> {
    let result: string | null = await this.redis.get(
      `${this.prefix}${options.query}`
    );

    if (result == null) {
      return undefined;
    }

    result = result.substring(1, result.length - 1);
    result = result.replaceAll('\\"', '"').replace("\\\\n", "\\n");
    return { ...options, result };
  }

  /**
   * Stores given query result in the cache for one minute.
   */
  async storeInCache(
    options: QueryResultCacheOptions,
    _savedCache: QueryResultCacheOptions | undefined,
    _queryRunner?: QueryRunner | undefined
  ): Promise<void> {
    // expire in 60 seconds
    await this.redis.execute([
      "SET",
      `${this.prefix}${options.query}`,
      JSON.stringify(options.result),
      "EX",
      "60",
    ]);
  }

  /**
   * Checks if the cache is expired.
   * @returns always false
   */
  isExpired(_savedCache: QueryResultCacheOptions): boolean {
    return false;
  }

  /**
   * Deletes every entry in redis, that belongs to the cache.
   */
  async clear(_queryRunner?: QueryRunner | undefined): Promise<void> {
    this.redis.execute<string[]>(["keys", `${this.prefix}*`]).then((keys) => {
      keys.forEach((key) => {
        this.redis.execute(["del", key]);
      });
    });
  }

  /**
   * Deletes cached results from the cache.
   * @param identifiers list of identifiers to delete from the cache
   */
  async remove(
    identifiers: string[],
    _queryRunner?: QueryRunner | undefined
  ): Promise<void> {
    identifiers.forEach((identifier) => {
      this.redis.execute(["del", identifier]);
    });
  }
}
