version: "3.8"
services:
  proxy:
    image: nobot-proxy:latest
    container_name: nobot-proxy
    mem_limit: 1gb
    mem_reservation: 100m
    cpu_count: 1
    cpu_percent: 50
    healthcheck:
      test: [ "CMD", "curl", "-f", "http://localhost/health" ]
      interval: 1m
      timeout: 10s
    depends_on:
      - frontend
      - backend
    volumes:
      - ~/nobot/log/proxy:/var/log/nginx
    ports:
      - "127.0.0.1:8080:80"
    networks:
      main:
        ipv4_address: 192.168.20.2
        ipv6_address: fc00:1::2

  frontend:
    image: nobot-frontend:latest
    container_name: nobot-frontend
    mem_limit: 500m
    mem_reservation: 50m
    cpu_count: 1
    cpu_percent: 50
    restart: always
    healthcheck:
      test: [ "CMD", "curl", "-f", "http://localhost/health" ]
      interval: 1m
      timeout: 10s
    volumes:
      - ~/nobot/log/frontend:/var/log/nginx
    depends_on:
      - backend
    networks:
      main:
        ipv4_address: 192.168.20.3
        ipv6_address: fc00:1::3

  backend:
    image: nobot-backend:latest
    container_name: nobot-backend
    depends_on:
      - db
      - redis
    healthcheck:
      test: [ "CMD", "curl", "-f", "http://localhost:8080/health" ]
      interval: 1m
      timeout: 10s
    ports:
      - "127.0.0.1:1883:1883"
      - "127.0.0.1:1884:1884"
    mem_limit: 300m
    mem_reservation: 100m
    cpu_count: 1
    cpu_percent: 50
    restart: always
    user: "node:node"
    networks:
      main:
        ipv4_address: 192.168.20.4
        ipv6_address: fc00:1::4

  db:
    image: nobot-db:latest
    container_name: nobot-db
    environment:
      - TZ=Europe/Berlin
    ports:
      - "127.0.0.1:3306:3306"
    healthcheck:
      test: "mysql --user=nobot --password=gXg33Ep4urGp6bF2 --execute 'USE nobot; SELECT 1 FROM dual;'"
      interval: 1m
      timeout: 10s
    mem_limit: 100m
    mem_reservation: 50m
    cpu_count: 2
    cpu_percent: 50
    volumes:
      - ~/nobot/db:/var/lib/mysql
    restart: always
    networks:
      main:
        ipv4_address: 192.168.20.5
        ipv6_address: fc00:1::5

  redis:
    image: redis:7.0.5-alpine
    command: /bin/sh -c "redis-server --requirepass $$REDIS_HOST_PASSWORD"
    container_name: nobot-redis
    ports:
      - "127.0.0.1:6379:6379"
    volumes:
      - ~/nobot/redis:/data
    env_file:
      - redis.env
    healthcheck:
      test: [ "CMD", "redis-cli", "ping" ]
      interval: 1m
      timeout: 10s
    mem_limit: 200m
    mem_reservation: 50m
    cpu_count: 1
    cpu_percent: 50
    networks:
      main:
        ipv4_address: 192.168.20.6
        ipv6_address: fc00:1::6

networks:
  main:
    enable_ipv6: true
    driver: bridge
    ipam:
      driver: default
      config:
        - subnet: 192.168.20.0/29
        - subnet: fc00:1::0/120
