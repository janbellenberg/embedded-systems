@echo off
cd ..
mkdir bin 2>NUL

:: build flutter app
cd frontend/nobot
CALL flutter build apk

copy build\app\outputs\apk\release\app-release.apk ..\..\bin\nobot.apk > NUL
cd ..\..\scripts

echo "-> bin/nobot.apk"
pause