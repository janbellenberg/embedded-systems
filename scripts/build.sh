#!/bin/sh

cd ./backend
docker build -t nobot-backend .

cd ../db
docker build -t nobot-db .

cd ../frontend/nobot
docker build -t nobot-frontend .

cd ../../proxy
docker build -t nobot-proxy:latest .
docker build -t nobot-proxy:latest-selfhosting -f - . < Dockerfile.selfhosting

cd ..