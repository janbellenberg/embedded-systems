@echo off
cd ..
mkdir bin 2>NUL
mkdir bin\windows\ 2>NUL

:: Build flutter app
cd frontend\nobot
CALL flutter build windows
xcopy build\windows\runner\Release ..\..\bin\windows /E

:: build portable zip
cd ..\..\bin\windows
powershell Compress-Archive * ..\nobot-portable-windows.zip -Force

:: collect uninstaller
cd ..\..
copy installer\windows\uninstaller\bin\Release\uninstaller.exe bin\windows\uninstaller.exe

:: build zip
cd bin\windows
powershell Compress-Archive * ..\windows.zip -Force
cd ..
rd /s /q "windows"

cd ..\scripts

echo "-> bin/windows.zip"
echo "-> bin/nobot-portable-windows.zip"
pause