#!/bin/sh

echo "Tagging images..."
docker tag nobot-frontend:latest container.janbellenberg.de:5000/nobot-frontend:latest
docker tag nobot-backend:latest container.janbellenberg.de:5000/nobot-backend:latest
docker tag nobot-db:latest container.janbellenberg.de:5000/nobot-db:latest
docker tag nobot-proxy:latest container.janbellenberg.de:5000/nobot-proxy:latest
docker tag nobot-proxy:latest-selfhosting container.janbellenberg.de:5000/nobot-proxy:latest-selfhosting

echo "Pushing images..."
docker push container.janbellenberg.de:5000/nobot-frontend:latest
docker push container.janbellenberg.de:5000/nobot-backend:latest
docker push container.janbellenberg.de:5000/nobot-db:latest
docker push container.janbellenberg.de:5000/nobot-proxy:latest
docker push container.janbellenberg.de:5000/nobot-proxy:latest-selfhosting
