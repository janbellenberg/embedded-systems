#!/bin/bash

checkExitCode()
{
  exitCode=$?
  if [ $exitCode -ne 0 ]; then
    echo "Es ist ein Fehler aufgetreten."
    exit 1
  fi
}

echo "Daten werden heruntergeladen ."
curl https://nobot.janbellenberg.de/download/docker-compose.self-hosted.yml -f -s -o docker-compose.self-hosted.yml
checkExitCode

echo "Daten werden heruntergeladen .."
docker pull -q container.janbellenberg.de:5000/nobot-frontend:latest > /dev/null
checkExitCode

echo "Daten werden heruntergeladen ..."
docker pull -q container.janbellenberg.de:5000/nobot-backend:latest > /dev/null
checkExitCode

echo "Daten werden heruntergeladen ...."
docker pull -q container.janbellenberg.de:5000/nobot-db:latest > /dev/null
checkExitCode

echo "Daten werden heruntergeladen ....."
docker pull -q container.janbellenberg.de:5000/nobot-proxy:latest > /dev/null
checkExitCode

echo "Daten werden heruntergeladen ......"
docker pull -q redis:7.0.5-alpine > /dev/null  # ! keep version in sync with docker compose file
checkExitCode


echo "Herunterladen abgeschlossen"
echo "Anwendung wird nun gestartet"
docker compose -f docker-compose.self-hosted.yml -p nobot up -d

echo "Installation abgeschlossen"