@echo off

title NoBot
echo "Installiere aktuelle Version von NoBot Server"

wsl --cd "~" -e rm -rf nobot
wsl --cd "~" -e mkdir nobot
wsl --cd "~/nobot" -e curl https://nobot.janbellenberg.de/download/nobot.sh -f -s -o nobot.sh

if not errorlevel 0 (
  echo "Es ist ein Fehler aufgetreten"
  pause
  exit
)

wsl --cd "~/nobot" -e sh nobot.sh