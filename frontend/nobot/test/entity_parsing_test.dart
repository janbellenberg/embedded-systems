import 'dart:convert';

import 'package:nobot/model/blocks/mqtt_command.dart';
import 'package:nobot/model/db/user.dart';
import 'package:test/test.dart';

void main() {
  late Map<String, dynamic> data;

  setUp(() {
    String rawJSON = """{
      "data": {
          "user": {
              "username": "janbellenberg",
              "darkmode": true,
              "devices": [
                  {
                      "id": 1,
                      "name": "Test",
                      "mac": "aa:bb:cc:dd:ee:ff"
                  },
                  {
                      "id": 2,
                      "name": "ABC",
                      "mac": "ab:bc:cd:de:ef:fa"
                  }
              ],
              "programs": [
                  {
                      "id": 1,
                      "name": "Test",
                      "content": "{ \\"blocks\\": [{ \\"type\\": \\"MQTT\\", \\"message\\": \\"set-to-zero\\" }, { \\"type\\": \\"WHILE\\", \\"condition\\": \\"\$backDistance < 5\\", \\"items\\": [ { \\"type\\": \\"MQTT\\", \\"message\\": \\"forward\\" }, { \\"type\\": \\"MQTT\\", \\"message\\": \\"backward\\" } ] },{ \\"type\\": \\"MQTT\\", \\"message\\": \\"stop\\" }, { \\"type\\": \\"IF\\", \\"condition\\": \\"\$backDistance == 6\\", \\"items\\": [ { \\"type\\": \\"MQTT\\", \\"message\\": \\"backward\\" }, { \\"type\\": \\"MQTT\\", \\"message\\": \\"forward\\" } ] } ] }"
                  },
                  {
                      "id": 2,
                      "name": "Test2",
                      "content": "{ \\"blocks\\": [] }"
                  }
              ]
          }
      }
  }""";
    data = jsonDecode(rawJSON)["data"]["user"];
  });

  group("Parsing", () {
    late User user;

    test("should not throw an exception", () {
      expect(() {
        user = User.fromAPI(data);
      }, returnsNormally);
    });

    test("user data", () {
      expect(user.username, equals("janbellenberg"));
      expect(user.darkmode, equals(true));
      expect(user.devices.length, equals(2));
      expect(user.programs.length, equals(2));
    });

    test("device data", () {
      expect(user.devices[0].id, equals(1));
      expect(user.devices[1].id, equals(2));
      expect(user.devices[0].name, equals("Test"));
      expect(user.devices[1].name, equals("ABC"));
      expect(user.devices[0].mac, equals("aa:bb:cc:dd:ee:ff"));
      expect(user.devices[1].mac, equals("ab:bc:cd:de:ef:fa"));
    });

    test("program data", () {
      expect(user.programs[0].id, equals(1));
      expect(user.programs[1].id, equals(2));
      expect(user.programs[0].name, equals("Test"));
      expect(user.programs[1].name, equals("Test2"));
      expect(
          user.programs[0].parsed.blocks[0].runtimeType, equals(MqttCommand));
      expect(user.programs[0].parsed.blocks.length, equals(4));
      expect(user.programs[1].parsed.blocks.length, equals(0));
    });
  });
}
