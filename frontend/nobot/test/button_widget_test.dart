import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:nobot/widgets/button_widget.dart';

void main() {
  group("Button Widget", () {
    late Widget widget1, widget2;
    bool buttonPressed;

    setUp(() {
      widget1 = MaterialApp(
        home: Button(
          onPressed: () {
            buttonPressed = true;
          },
          borderColor: Colors.pink,
          child: const Text("Demo text"),
        ),
      );

      widget2 = MaterialApp(
        theme: ThemeData(
          colorScheme: const ColorScheme.light(
            primary: Colors.yellow,
          ),
        ),
        home: Button(
          onPressed: () {},
          isPrimary: true,
          height: 100.0,
          child: const Icon(Icons.play_arrow),
        ),
      );
    });

    testWidgets("should have the text", (WidgetTester tester) async {
      await tester.pumpWidget(widget1);
      expect(find.text("Demo text"), findsOneWidget);
    });

    testWidgets("should have icon", (WidgetTester tester) async {
      await tester.pumpWidget(widget2);
      expect(find.byIcon(Icons.play_arrow), findsOneWidget);
    });

    testWidgets("causes action when button pressed",
        (WidgetTester tester) async {
      await tester.pumpWidget(widget1);

      buttonPressed = false;
      expect(buttonPressed, equals(false));
      await tester.tap(find.byType(Button));
      await tester.pump();
      expect(buttonPressed, equals(true));
    });

    testWidgets("should have specified border", (WidgetTester tester) async {
      await tester.pumpWidget(widget1);
      ElevatedButton button = find
          .byType(ElevatedButton)
          .last
          .evaluate()
          .single
          .widget as ElevatedButton;

      expect(
        button.style?.shape?.resolve({})?.side.color.value,
        equals(Colors.pink.value),
      );
    });

    testWidgets("should change color if primary", (WidgetTester tester) async {
      await tester.pumpWidget(widget2);
      ElevatedButton button = find
          .byType(ElevatedButton)
          .last
          .evaluate()
          .single
          .widget as ElevatedButton;
      expect(
        button.style?.backgroundColor?.resolve({})?.value,
        equals((widget2 as MaterialApp).theme?.colorScheme.primary.value),
      );
    });
  });
}
