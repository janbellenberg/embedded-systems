import 'package:nobot/model/db/device.dart';
import 'package:nobot/model/db/program.dart';
import 'package:nobot/model/db/user.dart';
import 'package:test/test.dart';

void main() {
  group("Constructor of User", () {
    late User user1, user2;

    setUp(() {
      user1 = User();
      user2 = User.fromData(id: 1, username: "ABC", darkmode: true);
    });

    test("should create empty lists", () {
      expect(user1.devices.length, 0);
      expect(user1.programs.length, 0);

      expect(user2.devices.length, 0);
      expect(user2.programs.length, 0);
    });

    test("should set values", () {
      expect(user2.id, 1);
      expect(user2.username, "ABC");
      expect(user2.darkmode, true);
    });
  });

  group("Constructor of Device", () {
    late Device device;

    setUp(() {
      device = Device.fromData(
        id: 1,
        name: "Test",
        batteryLevel: 50,
        mac: "aa:bb:cc:dd:ee:ff",
      );
    });

    test("should set values", () {
      expect(device.id, 1);
      expect(device.name, "Test");
      expect(device.batteryLevel, 50);
      expect(device.mac, "aa:bb:cc:dd:ee:ff");
    });
  });

  group("Constructor of Program", () {
    late Program program;

    setUp(() {
      program = Program.fromData(id: 1, name: "Test");
    });

    test("should create empty lists", () {
      expect(Program().parsed.blocks.isEmpty, true);
    });

    test("should set values", () {
      expect(program.id, 1);
      expect(program.name, "Test");
      expect(program.content, '{"blocks":[]}');
    });
  });
}
