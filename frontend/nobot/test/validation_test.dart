import 'package:nobot/model/db/device.dart';
import 'package:nobot/model/db/user.dart';
import 'package:test/test.dart';

void main() {
  group("Validation", () {
    test("of username", () {
      expect(User.validateUsername("abc"), true);
      expect(User.validateUsername(""), false);
      expect(User.validateUsername(" "), false);
    });

    test("of password", () {
      expect(User.validatePassword("abc"), true);
      expect(User.validatePassword(""), false);
      expect(User.validatePassword(" "), false);
    });

    test("of mac", () {
      expect(Device.validateMAC("aa:bb:cc:dd:ee:ff"), true);
      expect(Device.validateMAC("1a:5b:6c:0e:8f:2a"), true);
      expect(Device.validateMAC("12:34:56:78:90:11"), true);
      expect(Device.validateMAC("1a:5b:6cc:0e:8f:2a"), false);
      expect(Device.validateMAC("1a-5b-6c-0e-8f-2a"), false);
      expect(Device.validateMAC("hi:jj:ff:zz:qq:rr"), false);
      expect(Device.validateMAC(" "), false);
      expect(Device.validateMAC(""), false);
    });
  });
}
