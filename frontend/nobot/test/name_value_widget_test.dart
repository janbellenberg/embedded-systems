import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:nobot/widgets/name_value_widget.dart';

void main() {
  group("Name Value Widget", () {
    late Widget widget1, widget2;

    setUp(() {
      widget1 = const MaterialApp(
        home: NameValueWidget(
          title: "Key",
          value: "VAL",
          whiteSpaceAfterValue: false,
        ),
      );

      widget2 = const MaterialApp(
        home: NameValueWidget(
          title: "Key",
          value: "VAL",
          unit: "cm",
        ),
      );
    });

    testWidgets("should have the key", (WidgetTester tester) async {
      await tester.pumpWidget(widget1);

      expect(find.text("Key:"), findsOneWidget);
    });

    testWidgets("should have the value", (WidgetTester tester) async {
      await tester.pumpWidget(widget1);

      expect(find.text("VAL"), findsOneWidget);
    });

    testWidgets("should have the value with unit", (WidgetTester tester) async {
      await tester.pumpWidget(widget2);

      expect(find.text("VAL cm"), findsOneWidget);
    });
  });
}
