import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:nobot/widgets/text_button_widget.dart';

void main() {
  group("Text Button Widget", () {
    late Widget widget1, widget2;
    bool buttonPressed;

    setUp(() {
      widget1 = MaterialApp(
        home: TextButtonWidget(
          text: "Demo text",
          onPressed: () {
            buttonPressed = true;
          },
          leadingIcon: const Icon(Icons.play_arrow),
          borderColor: Colors.pink,
          fontSize: 10.0,
        ),
      );

      widget2 = MaterialApp(
        theme: ThemeData(
          colorScheme: const ColorScheme.light(
            primary: Colors.yellow,
          ),
        ),
        home: TextButtonWidget(
          text: "ABC",
          onPressed: () {},
          isPrimary: true,
          height: 100.0,
        ),
      );
    });

    testWidgets("should have the text in correct font size",
        (WidgetTester tester) async {
      await tester.pumpWidget(widget1);

      expect(find.text("Demo text"), findsOneWidget);

      Text label =
          find.text("Demo text").first.evaluate().single.widget as Text;
      expect(label.style?.fontSize, equals(10.0));
    });

    testWidgets("causes action when button pressed",
        (WidgetTester tester) async {
      await tester.pumpWidget(widget1);

      buttonPressed = false;
      expect(buttonPressed, equals(false));
      await tester.tap(find.byType(TextButtonWidget));
      await tester.pump();
      expect(buttonPressed, equals(true));
    });

    testWidgets("should have the icon", (WidgetTester tester) async {
      await tester.pumpWidget(widget1);
      expect(find.byIcon(Icons.play_arrow), findsOneWidget);
    });

    testWidgets("should have specified border", (WidgetTester tester) async {
      await tester.pumpWidget(widget1);
      ElevatedButton button = find
          .byType(ElevatedButton)
          .last
          .evaluate()
          .single
          .widget as ElevatedButton;

      expect(
        button.style?.shape?.resolve({})?.side.color.value,
        equals(Colors.pink.value),
      );
    });

    testWidgets("should change color if primary", (WidgetTester tester) async {
      await tester.pumpWidget(widget2);
      ElevatedButton button = find
          .byType(ElevatedButton)
          .last
          .evaluate()
          .single
          .widget as ElevatedButton;
      expect(
        button.style?.backgroundColor?.resolve({})?.value,
        equals((widget2 as MaterialApp).theme?.colorScheme.primary.value),
      );
    });
  });
}
