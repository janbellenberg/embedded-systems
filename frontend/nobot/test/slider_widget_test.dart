import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:nobot/widgets/slider_widget.dart';

void main() {
  group("Slider widget", () {
    late Widget widget;
    double value = 0.5;

    setUp(() {
      widget = MaterialApp(
        home: Scaffold(
          body: SliderWidget(
            value: value,
            onValueChanged: (val) {
              value = val;
            },
          ),
        ),
      );
    });

    testWidgets("should react on touch gesture", (WidgetTester tester) async {
      await tester.pumpWidget(widget);

      await tester.drag(find.byType(SliderWidget), const Offset(-500.0, 0.0));
      await tester.pump();
      expect(value, 0);

      await tester.drag(find.byType(SliderWidget), const Offset(500.0, 0.0));
      await tester.pump();
      expect(value, 1);
    });

    testWidgets("should react on scroll gesture", (WidgetTester tester) async {
      await tester.pumpWidget(widget);

      final Offset location = tester.getCenter(find.byType(SliderWidget));
      final TestPointer testPointer = TestPointer(1, PointerDeviceKind.mouse);
      testPointer.hover(location);

      await tester.sendEventToBinding(
        testPointer.scroll(const Offset(0.0, 1.0)),
      );
      await tester.pumpAndSettle();
      expect(value, 0.95);

      await tester.sendEventToBinding(
        testPointer.scroll(const Offset(0.0, -1.0)),
      );
      await tester.pumpAndSettle();
      expect(value, 1);
    });
  });
}
