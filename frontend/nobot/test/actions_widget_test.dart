import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:nobot/widgets/actions_widget.dart';

void main() {
  group("Actions widget", () {
    late Widget widget;

    setUp(() {
      widget = const MaterialApp(
        home: Scaffold(
          body: ActionMenu(children: [
            ListTile(
              title: Text("Test"),
            ),
            ListTile(
              title: Text("ABC"),
            ),
          ]),
        ),
      );
    });

    testWidgets("should contain the widgets", (WidgetTester tester) async {
      await tester.pumpWidget(widget);

      expect(find.text("Test"), findsOneWidget);
      expect(find.text("ABC"), findsOneWidget);
    });
  });
}
