import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';

import '../controller/device.dart';
import '../model/db/device.dart';
import '../model/redux/app_state.dart';
import '../widgets/content_wrapper_widget.dart';
import '../widgets/text_button_widget.dart';

/// Page for editing the name of a device or deleting the device.
///
/// The page can be displayed as a full-screen version, as well as a dialog.
class DeviceEditPage extends StatelessWidget {
  DeviceEditPage(this.device, {super.key}) {
    nameController = TextEditingController.fromValue(
      TextEditingValue(
        text: device.name ?? "",
      ),
    );
  }

  late final Device device;
  late final TextEditingController nameController;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: StoreConnector<AppState, AppState>(
        converter: (store) => store.state,
        builder: (context, state) {
          return ContentWrapper(
            canGoBack: true,
            title: '${device.name} bearbeiten',
            child: Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.symmetric(
                    horizontal: 32.0,
                    vertical: 32.0,
                  ),
                  child: TextField(
                    cursorColor: Theme.of(context).colorScheme.primary,
                    maxLength: 50,
                    controller: nameController,
                    style: const TextStyle(
                      fontSize: 25,
                    ),
                    decoration: const InputDecoration(
                      labelText: 'Name',
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(
                    top: 32.0,
                    left: 32.0,
                    right: 32.0,
                  ),
                  child: Wrap(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: TextButtonWidget(
                          text: "Speichern",
                          width: 200.0,
                          onPressed: () {
                            if (device.id == null) return;
                            updateDeviceName(
                              device.id!,
                              nameController.text,
                            );
                          },
                          leadingIcon: const Icon(
                            Icons.save,
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: TextButtonWidget(
                          text: "Löschen",
                          width: 200.0,
                          onPressed: () {
                            if (device.id == null) return;

                            deleteDevice(
                              device.id!,
                            ).then((value) {
                              Navigator.pop(context);
                            });
                          },
                          leadingIcon: const Icon(
                            Icons.delete,
                            color: Colors.red,
                          ),
                          isPrimary: false,
                          borderColor: Colors.red,
                        ),
                      )
                    ],
                  ),
                ),
              ],
            ),
          );
        },
      ),
    );
  }
}
