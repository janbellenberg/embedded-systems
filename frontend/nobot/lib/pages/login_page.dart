import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';

import '../core/page_wrapper.dart';
import '../model/db/user.dart';
import '../model/redux/app_state.dart';
import '../widgets/content_wrapper_widget.dart';
import '../controller/user.dart' as controller;
import '../widgets/text_button_widget.dart';
import '../widgets/web_bottom_bar.dart';
import 'create_account_page.dart';

/// Page for the login process with [TextField]s for username and password.
class LoginPage extends StatefulWidget {
  const LoginPage({super.key});

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  bool _validPassword = false;

  final TextEditingController _username = TextEditingController();
  final TextEditingController _password = TextEditingController();

  @override
  void dispose() {
    _username.dispose();
    _password.dispose();
    super.dispose();
  }

  /// Performs the login.
  /// See: [controller.login]
  void login() {
    controller.login(_username.text, _password.text);
  }

  /// Routes to the [AccountCreatePage]
  void createAccount() {
    PageWrapper.routeToPage(
      const AccountCreatePage(),
      context,
      showInDialog: false,
      overrideLogin: true,
    );
  }

  /// Validates if the username and the password meet the formal requirements.
  void validate() {
    setState(() {
      _validPassword = User.validateUsername(_username.text) &&
          User.validatePassword(_password.text);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: StoreConnector<AppState, AppState>(
        converter: (store) => store.state,
        builder: (context, state) {
          return ContentWrapper(
            title: "Login",
            child: Stack(
              children: <Widget>[
                Center(
                  child: ConstrainedBox(
                    constraints: const BoxConstraints(maxWidth: 400),
                    child: SingleChildScrollView(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.symmetric(
                              vertical: 10.0,
                              horizontal: 20.0,
                            ),
                            child: TextField(
                              controller: _username,
                              style: const TextStyle(
                                fontSize: 30.0,
                                height: 1.0,
                              ),
                              onChanged: (value) => validate(),
                              cursorColor:
                                  Theme.of(context).colorScheme.primary,
                              decoration: const InputDecoration(
                                labelText: "Benutzername",
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(
                              left: 20.0,
                              right: 20.0,
                              bottom: 10.0,
                            ),
                            child: TextField(
                              controller: _password,
                              style: const TextStyle(
                                fontSize: 30.0,
                                height: 1.0,
                              ),
                              cursorColor:
                                  Theme.of(context).colorScheme.primary,
                              obscureText: true,
                              decoration: const InputDecoration(
                                labelText: "Passwort",
                              ),
                              onChanged: (value) => validate(),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(
                              top: 20.0,
                              left: 85.0,
                              right: 85.0,
                            ),
                            child: TextButtonWidget(
                              text: "Anmelden",
                              onPressed: _validPassword ? login : null,
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(
                              top: 20.0,
                              left: 85.0,
                              right: 85.0,
                              bottom: 10.0,
                            ),
                            child: TextButtonWidget(
                              text: "Konto erstellen",
                              isPrimary: false,
                              borderColor:
                                  Theme.of(context).colorScheme.primary,
                              onPressed: createAccount,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                const WebBottomBar()
              ],
            ),
          );
        },
      ),
    );
  }
}
