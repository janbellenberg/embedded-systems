import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:collection/collection.dart';

import '../controller/user.dart';
import '../core/page_wrapper.dart';
import '../dialogs/add_device_dialog.dart';
import '../dialogs/info_dialog.dart';
import '../model/db/device.dart';
import '../model/redux/app_state.dart';
import '../model/redux/store.dart';
import '../model/redux/actions.dart' as redux;
import '../widgets/content_wrapper_widget.dart';
import '../widgets/device_button.dart';
import '../widgets/device_split_screen.dart';
import '../widgets/text_button_widget.dart';
import 'device_details_page.dart';
import 'device_edit_page.dart';
import 'user_page.dart';

/// Start Page for selecting the device, which should be controlled.
///
/// On small screens (<1000px) only a list of devices will be shown.
/// On larger screens the devices will be shown as a horizontal list and
/// the device information in a split screen.
class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int _selectedDevice = -1;

  void _addDevice() {
    showDialog(
      context: context,
      builder: ((context) => const AddDeviceDialog()),
    );
  }

  @override
  void initState() {
    super.initState();
    loadUserInfo() // load user data
        .then(
      (value) {
        store.dispatch(
          redux.Action(redux.ActionTypes.setupDone),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: StoreConnector<AppState, AppState>(
        converter: (store) => store.state,
        builder: (context, state) {
          Widget body;
          Device? selected;

          // Only show a list of widgets on small screens
          bool smallScreen = MediaQuery.of(context).size.width < 1000;
          if (smallScreen) {
            _selectedDevice = -1;
            body = SingleChildScrollView(
              physics: const AlwaysScrollableScrollPhysics(),
              child: SizedBox(
                width: MediaQuery.of(context).size.width,
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(
                        top: 30.0,
                        bottom: 40.0,
                      ),
                      child: TextButtonWidget(
                        text: "Gerät hinzufügen",
                        onPressed: _addDevice,
                      ),
                    ),
                    Wrap(
                      children: [
                        for (Device device in state.user?.devices ?? [])
                          DeviceButton(
                            device: device,
                            onPressed: () {
                              PageWrapper.routeToPage(
                                DeviceDetailsPage(device),
                                context,
                                showInDialog: false,
                              );
                            },
                          ),
                      ],
                    )
                  ],
                ),
              ),
            );
          } else {
            // show a horizontal list of devices and the split screen on larger screens.
            selected = state.user?.devices
                .firstWhereOrNull((element) => element.id == _selectedDevice);

            body = LayoutBuilder(
              builder: (context, constraints) {
                return SizedBox(
                  width: constraints.maxWidth,
                  child: Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.symmetric(
                          horizontal: 20.0,
                          vertical: 20.0,
                        ),
                        child: SingleChildScrollView(
                          scrollDirection: Axis.horizontal,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(
                                  right: 40.0,
                                ),
                                child: TextButtonWidget(
                                  text: "Gerät hinzufügen",
                                  onPressed: _addDevice,
                                ),
                              ),
                              for (Device device in state.user?.devices ?? [])
                                Padding(
                                  padding: const EdgeInsets.symmetric(
                                    horizontal: 10.0,
                                  ),
                                  child: TextButtonWidget(
                                    text: device.name ?? "Unbekanntes Gerät",
                                    onPressed: () => setState(
                                      () => _selectedDevice = device.id ?? -1,
                                    ),
                                    borderColor: device.id == _selectedDevice
                                        ? Theme.of(context).colorScheme.primary
                                        : null,
                                    isPrimary: false,
                                  ),
                                )
                            ],
                          ),
                        ),
                      ),
                      if (selected != null)
                        DeviceSplitScreen(
                          device: selected,
                          height: constraints.maxHeight,
                        ),
                      if (selected == null)
                        const Expanded(
                          child: Center(
                            child: Text(
                              "Bitte wählen Sie ein Gerät aus",
                              style: TextStyle(
                                fontSize: 25.0,
                              ),
                            ),
                          ),
                        ),
                    ],
                  ),
                );
              },
            );
          }

          return ContentWrapper(
            onRefresh: () async {
              await loadUserInfo();
              _selectedDevice = -1;
            },
            actions: [
              // Menu options:
              ListTile(
                leading: Icon(
                  Icons.person_outline,
                  color: Theme.of(context).colorScheme.primary,
                ),
                title: const Text("Mein Account"),
                onTap: () {
                  Navigator.pop(context);
                  PageWrapper.routeToPage(
                    const UserPage(),
                    context,
                  );
                },
              ),
              StoreConnector<AppState, AppState>(
                  converter: (store) => store.state,
                  builder: (context, state) {
                    return ListTile(
                      leading: Icon(
                        Icons.lightbulb_outline,
                        color: Theme.of(context).colorScheme.primary,
                      ),
                      title: (state.user?.darkmode ?? false)
                          ? const Text("Dunkles Design")
                          : const Text("Helles Design"),
                      onTap: () {
                        updateDesign(!(state.user!.darkmode ?? true));
                      },
                    );
                  }),
              ListTile(
                leading: Icon(
                  Icons.info_outline,
                  color: Theme.of(context).colorScheme.primary,
                ),
                title: const Text("Info"),
                onTap: () {
                  Navigator.pop(context);
                  showDialog(
                    context: context,
                    builder: (context) => const InfoDialog(),
                    barrierColor: Colors.transparent,
                  );
                },
              ),
              ListTile(
                leading: Icon(
                  Icons.logout,
                  color: Theme.of(context).colorScheme.primary,
                ),
                title: const Text("Abmelden"),
                onTap: () {
                  logout().then((_) {
                    Navigator.pop(context);
                  });
                },
              ),
              if (selected != null &&
                  MediaQuery.of(context).size.width > 1000) ...[
                Divider(color: Theme.of(context).cardColor),
                ListTile(
                  leading: Icon(
                    Icons.edit,
                    color: Theme.of(context).colorScheme.primary,
                  ),
                  title: const Text("Gerät bearbeiten"),
                  onTap: () {
                    Navigator.pop(context);
                    PageWrapper.routeToPage(
                      DeviceEditPage(selected!),
                      context,
                    );
                  },
                ),
              ]
            ],
            child: body,
          );
        },
      ),
    );
  }
}
