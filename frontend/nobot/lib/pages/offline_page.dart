import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import '../model/redux/store.dart';
import '../model/redux/actions.dart' as redux;

import '../services/shared_preferences/server.dart'
    if (dart.library.html) '../services/shared_preferences/server_web.dart';
import '../widgets/text_button_widget.dart';

/// Page for displaying the offline icon.
class OfflinePage extends StatelessWidget {
  const OfflinePage({Key? key}) : super(key: key);

  void _resetApp() {
    store.dispatch(
      redux.Action(
        redux.ActionTypes.reset,
      ),
    );

    store.dispatch(
      redux.Action(
        redux.ActionTypes.setupDone,
      ),
    );

    readServerAddress().then((value) {
      store.dispatch(
        redux.Action(
          redux.ActionTypes.setHostname,
          payload: value,
        ),
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 50.0),
              child: Icon(
                Icons.signal_wifi_off_outlined,
                color: Theme.of(context).colorScheme.primary,
                size: 170.0,
              ),
            ),
            const Padding(
              padding: EdgeInsets.symmetric(horizontal: 25.0),
              child: Text(
                "Keine Verbindung",
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 30.0,
                ),
              ),
            ),
            if (!kIsWeb)
              FutureBuilder(
                future: usesDefaultServer(),
                builder: (context, usingDefault) {
                  if (!usingDefault.hasData || usingDefault.data != false) {
                    return Container();
                  }

                  return TextButtonWidget(
                    onPressed: () {
                      updateServerAddress(null);
                      _resetApp();
                    },
                    text: "Server zurücksetzen",
                    isPrimary: false,
                  );
                },
              ),
          ],
        ),
      ),
    );
  }
}
