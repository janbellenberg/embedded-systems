import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';

import '../model/db/user.dart';
import '../controller/user.dart';
import '../model/redux/app_state.dart';
import '../widgets/content_wrapper_widget.dart';
import '../widgets/text_button_widget.dart';

/// Page for creating a new user account by username and password.
class AccountCreatePage extends StatefulWidget {
  const AccountCreatePage({super.key});

  @override
  State<AccountCreatePage> createState() => _AccountCreatePageState();
}

class _AccountCreatePageState extends State<AccountCreatePage> {
  bool _isValid = false;
  bool _nameValid = false;

  final TextEditingController _username = TextEditingController();
  final TextEditingController _password = TextEditingController();
  final TextEditingController _passwordRepeat = TextEditingController();

  @override
  void dispose() {
    _username.dispose();
    _password.dispose();
    _passwordRepeat.dispose();

    super.dispose();
  }

  /// Validate if the passwords are equal and meet the password requirements.
  void _validatePassword() {
    String password1 = _password.text;
    String password2 = _passwordRepeat.text;
    setState(() {
      _isValid = password1 == password2 && User.validatePassword(password1);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: StoreConnector<AppState, AppState>(
        converter: (store) => store.state,
        builder: (context, state) {
          return ContentWrapper(
            canGoBack: true,
            title: "Account erstellen",
            child: Center(
              child: ConstrainedBox(
                constraints: const BoxConstraints(maxWidth: 400),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    // Username
                    Padding(
                      padding: const EdgeInsets.only(
                          left: 20.0, right: 20.0, bottom: 10.0),
                      child: TextField(
                        controller: _username,
                        style: const TextStyle(
                          fontSize: 30.0,
                          height: 1.0,
                        ),
                        cursorColor: Theme.of(context).colorScheme.primary,
                        decoration: InputDecoration(
                          errorText: _nameValid
                              ? null
                              : "Benutzername darf nicht leer sein",
                          labelText: "Benutzername",
                        ),
                        onChanged: (value) {
                          setState(() {
                            _nameValid = User.validateUsername(value);
                          });
                        },
                      ),
                    ),
                    // Password
                    Padding(
                      padding: const EdgeInsets.only(
                          left: 20.0, right: 20.0, bottom: 10.0),
                      child: TextField(
                        controller: _password,
                        style: const TextStyle(
                          fontSize: 30.0,
                          height: 1.0,
                        ),
                        cursorColor: Theme.of(context).colorScheme.primary,
                        obscureText: true,
                        decoration: const InputDecoration(
                          labelText: "Passwort",
                        ),
                        onChanged: (value) => _validatePassword(),
                      ),
                    ),
                    // Repeat password
                    Padding(
                      padding: const EdgeInsets.only(
                          left: 20.0, right: 20.0, bottom: 10.0),
                      child: TextField(
                        controller: _passwordRepeat,
                        style: const TextStyle(
                          fontSize: 30.0,
                          height: 1.0,
                        ),
                        cursorColor: Theme.of(context).colorScheme.primary,
                        obscureText: true,
                        decoration: const InputDecoration(
                          labelText: "Passwort bestätigen",
                        ),
                        onChanged: (value) => _validatePassword(),
                      ),
                    ),
                    // Submit button
                    if (_isValid && _nameValid)
                      Padding(
                        padding: const EdgeInsets.only(
                          top: 20.0,
                          left: 85.0,
                          right: 85.0,
                        ),
                        child: Container(
                          decoration: BoxDecoration(
                            color: Theme.of(context).colorScheme.primary,
                            borderRadius:
                                const BorderRadius.all(Radius.circular(50)),
                          ),
                          child: TextButtonWidget(
                            text: "Account erstellen",
                            onPressed: () {
                              if (!_isValid) return;

                              signup(_username.text, _password.text)
                                  .then((value) => Navigator.of(context).pop());
                            },
                          ),
                        ),
                      ),
                    if (!_isValid)
                      const Padding(
                        padding: EdgeInsets.only(top: 45.0),
                        child: Text(
                          "Passwort inkorrekt",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontSize: 20,
                            color: Colors.red,
                          ),
                        ),
                      )
                  ],
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}
