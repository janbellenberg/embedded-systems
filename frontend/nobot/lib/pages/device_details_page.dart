import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';

import '../core/page_wrapper.dart';
import '../model/db/device.dart';
import '../model/redux/app_state.dart';
import '../widgets/content_wrapper_widget.dart';
import '../widgets/device_control_widget.dart';
import 'device_edit_page.dart';

/// Page for controlling the device with the [device] and accessing information about it.
///
/// Only for small screens, it will automatically close, if the app is resized to bigger screens.
class DeviceDetailsPage extends StatefulWidget {
  const DeviceDetailsPage(this.device, {super.key});
  final Device device;

  @override
  State<DeviceDetailsPage> createState() => _DeviceDetailsPageState();
}

class _DeviceDetailsPageState extends State<DeviceDetailsPage> {
  @override
  Widget build(BuildContext context) {
    if (MediaQuery.of(context).size.width > 1000) {
      Timer.run(() => Navigator.pop(context));
      return Container();
    }

    return StoreConnector<AppState, AppState>(
      converter: ((store) => store.state),
      builder: (context, state) {
        return Scaffold(
          body: ContentWrapper(
            canGoBack: true,
            actions: [
              ListTile(
                leading: Icon(
                  Icons.edit,
                  color: Theme.of(context).colorScheme.primary,
                ),
                title: const Text("Gerät bearbeiten"),
                onTap: () {
                  Navigator.pop(context);
                  PageWrapper.routeToPage(
                    DeviceEditPage(widget.device),
                    context,
                  );
                },
              ),
            ],
            child: Padding(
              padding: const EdgeInsets.only(top: 40.0),
              child: SingleChildScrollView(
                child: DeviceControlWidget(widget.device),
              ),
            ),
          ),
        );
      },
    );
  }
}
