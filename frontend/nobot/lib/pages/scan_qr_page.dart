import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fast_barcode_scanner/fast_barcode_scanner.dart';

import '../widgets/content_wrapper_widget.dart';

/// Page with a full screen view of a QR scanner.
///
/// If a code was scanned, the [onSuccess] callback will be executed.
///
/// During the page is in view, the orientation of the app will be [DeviceOrientation.portraitUp].
/// After the app closes, the orientation will be set back to the [defaultOrientations].
class ScanQrPage extends StatefulWidget {
  const ScanQrPage({
    required this.onSuccess,
    this.defaultOrientations = const [
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
      DeviceOrientation.landscapeLeft,
      DeviceOrientation.landscapeRight,
    ],
    super.key,
  });

  final void Function(String) onSuccess;
  final List<DeviceOrientation> defaultOrientations;

  @override
  State<ScanQrPage> createState() => _ScanQrPageState();
}

class _ScanQrPageState extends State<ScanQrPage> {
  @override
  void initState() {
    super.initState();

    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);
  }

  @override
  void dispose() {
    SystemChrome.setPreferredOrientations(widget.defaultOrientations);

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ContentWrapper(
        child: BarcodeCamera(
          types: const [BarcodeType.qr],
          resolution: Resolution.hd720,
          framerate: Framerate.fps30,
          mode: DetectionMode.continuous,
          onScan: (code) => widget.onSuccess(code.value),
        ),
      ),
    );
  }
}
