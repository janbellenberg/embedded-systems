import 'package:flutter/material.dart';

/// Error page, which replaces the red and yellow error page from flutter.
class FatalPage extends StatelessWidget {
  const FatalPage({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'NoBot',
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: const [
              Padding(
                padding: EdgeInsets.only(top: 50.0),
                child: Icon(
                  Icons.error_outline,
                  color: Colors.red,
                  size: 170.0,
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 25.0),
                child: Text(
                  "Ein Fehler ist aufgetreten",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 30.0,
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
