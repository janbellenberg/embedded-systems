import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';

import '../model/redux/app_state.dart';
import '../model/redux/store.dart';
import '../model/redux/actions.dart' as redux;

import '../services/shared_preferences/server.dart'
    if (dart.library.html) '../services/shared_preferences/server_web.dart';
import '../widgets/content_wrapper_widget.dart';
import '../widgets/text_button_widget.dart';

/// Page for all app-related settings.
///
/// Currently only for setting the server address.
class SettingsPage extends StatefulWidget {
  const SettingsPage({super.key});

  @override
  State<SettingsPage> createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {
  TextEditingController _serverController = TextEditingController();

  @override
  void initState() {
    super.initState();
    usesDefaultServer().then((defaultServer) {
      if (!defaultServer) {
        readServerAddress().then((address) {
          setState(() {
            _serverController = TextEditingController.fromValue(
              TextEditingValue(
                text: address,
              ),
            );
          });
        });
      }
    });
  }

  @override
  void dispose() {
    _serverController.dispose();
    super.dispose();
  }

  void _updateServer() {
    if (_serverController.text.isNotEmpty) {
      updateServerAddress(_serverController.text);
      _resetApp();
    }
  }

  void _useDefaultServer() {
    updateServerAddress(null);
    _resetApp();
  }

  void _resetApp() {
    store.dispatch(
      redux.Action(
        redux.ActionTypes.reset,
      ),
    );

    store.dispatch(
      redux.Action(
        redux.ActionTypes.setupDone,
      ),
    );

    readServerAddress().then((value) {
      store.dispatch(
        redux.Action(
          redux.ActionTypes.setHostname,
          payload: value,
        ),
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: StoreConnector<AppState, AppState>(
        converter: (store) => store.state,
        builder: (context, state) {
          return ContentWrapper(
            canGoBack: true,
            title: "Einstellungen",
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  SizedBox(
                    width: 300,
                    child: TextField(
                      controller: _serverController,
                      style: const TextStyle(
                        fontSize: 25.0,
                        height: 1.0,
                      ),
                      cursorColor: Theme.of(context).primaryColor,
                      maxLength: 50,
                      decoration: const InputDecoration(
                        labelText: "Server",
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 32.0),
                    child: TextButtonWidget(
                      text: "Server speichern",
                      onPressed: () {
                        _updateServer();
                        Navigator.pop(context);
                      },
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 10.0),
                    child: TextButtonWidget(
                      text: "Standardserver verwenden",
                      isPrimary: false,
                      onPressed: () {
                        _useDefaultServer();
                        Navigator.pop(context);
                      },
                    ),
                  ),
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}
