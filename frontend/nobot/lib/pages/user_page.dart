import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';

import '../model/db/user.dart';
import '../controller/user.dart';
import '../dialogs/dialog_wrapper.dart';
import '../model/redux/app_state.dart';
import '../widgets/content_wrapper_widget.dart';
import '../widgets/text_button_widget.dart';

/// Page for all user-related settings.
///
/// - Updating password
/// - Deleting account
class UserPage extends StatefulWidget {
  const UserPage({super.key});

  @override
  State<UserPage> createState() => _UserPageState();
}

class _UserPageState extends State<UserPage> {
  bool _newValidPassword = false;

  final TextEditingController _newPassword = TextEditingController();
  final TextEditingController _newPasswordRepeat = TextEditingController();

  void validate() {
    String password1 = _newPassword.text;
    String password2 = _newPasswordRepeat.text;

    setState(() {
      _newValidPassword =
          User.validatePassword(password1) && password1 == password2;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: StoreConnector<AppState, AppState>(
        converter: (store) => store.state,
        builder: (context, state) {
          return ContentWrapper(
            canGoBack: true,
            title: "Mein Account",
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  SizedBox(
                    width: 300,
                    height: 80,
                    child: TextField(
                      controller: _newPassword,
                      obscureText: true,
                      style: const TextStyle(
                        fontSize: 25.0,
                        height: 1.0,
                      ),
                      cursorColor: Theme.of(context).colorScheme.primary,
                      maxLength: 50,
                      decoration: const InputDecoration(
                        labelText: "Neues Passwort",
                      ),
                      onChanged: (value) => validate(),
                    ),
                  ),
                  SizedBox(
                    width: 300,
                    height: 80,
                    child: TextField(
                      controller: _newPasswordRepeat,
                      obscureText: true,
                      style: const TextStyle(
                        fontSize: 25.0,
                        height: 1.0,
                      ),
                      cursorColor: Theme.of(context).colorScheme.primary,
                      maxLength: 50,
                      decoration: const InputDecoration(
                        labelText: "Passwort wiederholen",
                      ),
                      onChanged: (value) => validate(),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 32.0),
                    child: TextButtonWidget(
                      text: "Passwort ändern",
                      onPressed: _newValidPassword
                          ? () => _changePasswordDialog(context)
                          : null,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 20.0,
                      vertical: 10.0,
                    ),
                    child: Divider(
                      color: Theme.of(context).cardColor,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 32.0),
                    child: TextButtonWidget(
                      text: "Account löschen",
                      isPrimary: false,
                      borderColor: Colors.red,
                      onPressed: () => _deleteAccountDialog(context),
                    ),
                  ),
                ],
              ),
            ),
          );
        },
      ),
    );
  }

  void _changePasswordDialog(BuildContext context) {
    showDialog(
      context: context,
      builder: (context) => DialogWrapper(
        title: "Passwort ändern",
        children: <Widget>[
          const Padding(
            padding: EdgeInsets.only(bottom: 20.0),
            child: Text("Neues Passwort verwenden ?"),
          ),
          Row(
            children: <Widget>[
              TextButtonWidget(
                text: "Zurück",
                onPressed: () => Navigator.pop(context),
              ),
              const Spacer(),
              TextButtonWidget(
                text: "Bestätigen",
                onPressed: () {
                  updatePassword(_newPasswordRepeat.text)
                      .then((value) => Navigator.pop(context));
                },
              ),
            ],
          ),
        ],
      ),
    );
  }

  void _deleteAccountDialog(BuildContext context) {
    showDialog(
      context: context,
      builder: (context) => DialogWrapper(
        title: "Account löschen",
        children: <Widget>[
          const Padding(
            padding: EdgeInsets.only(bottom: 20.0),
            child: Text(
                "Sind Sie sicher?\nAlle Roboter und Einstellungen gehen verloren."),
          ),
          Row(
            children: <Widget>[
              TextButtonWidget(
                text: "Zurück",
                onPressed: () => Navigator.pop(context),
              ),
              const Spacer(),
              const TextButtonWidget(
                text: "Löschen",
                isPrimary: false,
                borderColor: Colors.red,
                onPressed: deleteUser,
              ),
            ],
          ),
        ],
      ),
    );
  }
}
