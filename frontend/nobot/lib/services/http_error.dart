enum HTTPError {
  CLIENT_ERROR,
  SERVER_ERROR,
  PROXY_ERROR,
  NOT_AUTHORIZED,
  NOT_FOUND,
  FORBIDDEN,
  DEPRECATED,
  CONNECTION_ERROR,
  UNKNOWN
}

Map<HTTPError, String> errorDescription = {
  HTTPError.CLIENT_ERROR: "Die Daten sind falsch oder die App ist veraltet.",
  HTTPError.SERVER_ERROR: "Auf dem Server ist ein Fehler aufgetreten.",
  HTTPError.PROXY_ERROR: "Der Server konnte nicht erreicht werden.",
  HTTPError.NOT_AUTHORIZED: "Bitte melden Sie sich an.",
  HTTPError.NOT_FOUND: "Es konnte keine Daten gefunden werden.",
  HTTPError.FORBIDDEN: "Sie sind für diesen Vorgang nicht berechtigt.",
  HTTPError.DEPRECATED: "Die Schnittstelle scheint veraltet zu sein.",
};
