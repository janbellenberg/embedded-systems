import '../http_error.dart';

/// A exception class for error-related http status codes.
///
/// The exception includes the type of [error], related to the status code and
/// a [message], that should be shown to the user.
class HttpException implements Exception {
  final HTTPError error;
  late final String message;

  /// Default message for unknown errors.
  static const String unknownMessage =
      "Es ist ein unbekannter Fehler aufgetreten";

  /// Creates a new [HttpException] with an [error] type and a [message].
  HttpException(this.error, this.message);

  /// Creates a new [HttpException] with an [error] type and
  /// the default message for this error type, specified in [errorDescriptions].
  /// This constructor should only be used, if the server didn't send a custom error message.
  HttpException.defaultMessage(this.error) {
    message = errorDescription[error] ?? unknownMessage;
  }
}
