import 'package:flutter/cupertino.dart';
import 'package:oktoast/oktoast.dart';

import '../../model/redux/actions.dart' as redux;
import '../../model/redux/store.dart';
import '../../themes/dark.dart';
import '../../themes/light.dart';

/// Display the error [message] as a toast and switch to the offline screen if [showOffline] is true.
void showError(String message, {bool showOffline = false}) {
  showMessage(message);

  if (showOffline) {
    store.dispatch(
      redux.Action(redux.ActionTypes.setOffline),
    );
  }
}

/// Display the [message] as a toast.
void showMessage(String message) {
  showToast(
    message,
    textPadding: const EdgeInsets.symmetric(
      vertical: 10.0,
      horizontal: 20.0,
    ),
    position: ToastPosition.bottom,
    backgroundColor: (store.state.user?.darkmode ?? false)
        ? lightTheme.colorScheme.primary
        : darkTheme.colorScheme.primary,
  );
}
