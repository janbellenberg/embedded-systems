import 'package:mqtt_client/mqtt_browser_client.dart';
import 'package:mqtt_client/mqtt_client.dart';
import 'package:uuid/uuid.dart';

import '../../model/redux/store.dart';

import '../../services/shared_preferences/server.dart'
    if (dart.library.html) '../../services/shared_preferences/server_web.dart';

/// Creates a new MQTT client using the WebSocket protocol,
/// because no raw TCP connections can be established in the browser.
Future<MqttClient> getClient() async {
  final bool useProdServer = await usesDefaultServer();
  return MqttBrowserClient.withPort(
    '${useProdServer ? "wss" : "ws"}://${store.state.hostname}',
    'user#${store.state.user?.id}#${const Uuid().v4()}',
    1884,
  );
}
