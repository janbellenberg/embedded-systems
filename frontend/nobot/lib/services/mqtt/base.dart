import 'dart:async';
import 'dart:convert';
import 'dart:developer';

import 'package:flutter/foundation.dart';
import 'package:mqtt_client/mqtt_client.dart';
import 'package:mutex/mutex.dart';
import 'package:oktoast/oktoast.dart';
import 'package:uuid/uuid.dart';

import '../../model/blocks/command_list.dart';
import '../../model/redux/store.dart';
import '../../model/redux/actions.dart' as redux;
import '../../themes/dark.dart';
import '../../themes/light.dart';
import 'default_connection.dart'
    if (dart.library.html) 'websocket_connection.dart' as connection;

/// MQTT connection for publishing and subscribing.
class MQTT {
  /// Whether the app is currently connecting.
  static bool connecting = false;

  /// The client object from `mqtt_client`.
  static MqttClient? _client;

  /// Mapping of the subscribed topics and their callbacks.
  static late Map<String, Function(String)> _subscriptions;
  static final subscriptionMutex = ReadWriteMutex();

  /// Timer for sending periodic ping messages.
  static Timer? _timer;

  /// Whether the connection state of the [_client] is connected.
  static bool get connected =>
      _client?.connectionStatus?.state == MqttConnectionState.connected;

  /// Configure and open a new connection to the MQTT Broker.
  static Future<void> connect() async {
    if (connecting) return;
    connecting = true;

    if (connected) return;
    if (store.state.sessionID == null) return;
    if (store.state.user == null) return;

    _client = await connection.getClient();
    if (_client == null) return;

    _client!.logging(on: false);
    _client!.setProtocolV311();
    _client!.autoReconnect = true;

    _subscriptions = {};

    _client!.connectionMessage = MqttConnectMessage().authenticateAs(
      "",
      store.state.sessionID,
    );

    try {
      await _client!.connect();
      if (kDebugMode) log("mqtt connected");
    } on Exception catch (e) {
      if (kDebugMode) log(e.toString());
      store.dispatch(redux.Action(redux.ActionTypes.setOffline));
      connecting = false;
      return;
    }

    // Check we are connected
    if (!connected) {
      store.dispatch(redux.Action(redux.ActionTypes.setOffline));
      _client = null;
      return;
    }

    _timer = Timer.periodic(const Duration(seconds: 60), (timer) {
      publish("ping", "ping");
    });

    _client!.updates!
        .listen((List<MqttReceivedMessage<MqttMessage?>>? c) async {
      // handle incoming message

      final MqttPublishMessage recMess = c![0].payload as MqttPublishMessage;
      final String message = MqttPublishPayload.bytesToStringAsString(
        recMess.payload.message,
      );

      try {
        await subscriptionMutex.acquireRead();
        for (String topic in _subscriptions.keys) {
          if (topic == c[0].topic) {
            _subscriptions[topic]!(message);
          }
        }
      } catch (e) {
        if (kDebugMode) log(e.toString());
      } finally {
        subscriptionMutex.release();
      }
    });
  }

  /// Subscribe to a [topic] and call the [onMessage] callback if a message was published on the [topic].
  static Future<void> subscribe(
      String topic, Function(String) onMessage) async {
    if (!connected) await connect();
    if (!connected) return;

    Subscription? result = _client!.subscribe(topic, MqttQos.exactlyOnce);

    if (result == null) {
      showToast(
        "Bei der Verbindung mit dem Roboter ist ein Problem aufgetreten.",
        position: ToastPosition.bottom,
        backgroundColor: (store.state.user?.darkmode ?? false)
            ? lightTheme.colorScheme.primary
            : darkTheme.colorScheme.primary,
      );
    }

    await subscriptionMutex.acquireWrite();
    _subscriptions.addAll({topic: onMessage});
    subscriptionMutex.release();
  }

  /// Unsubscribe from the [topic]
  static void unsubscribe(String topic) async {
    if (!connected) return;

    _client!.unsubscribe(topic);

    await subscriptionMutex.acquireWrite();
    _subscriptions.remove(topic);
    subscriptionMutex.release();
  }

  /// Publish [data] on the [topic].
  static void publish(String data, String topic) async {
    if (!connected) await connect();
    if (!connected) return;

    if (kDebugMode) {
      log("publish $data on $topic");
    }

    final builder = MqttClientPayloadBuilder();
    builder.addString(data);
    _client!.publishMessage(topic, MqttQos.exactlyOnce, builder.payload!);
  }

  /// Send a [command] to the device with the [mac] using the [accessKey].
  ///
  /// If the command takes longer, the method can [waitForFinished] signal,
  /// otherwise it waits just for a ACK with a certain [timeout] in ms.
  ///
  /// For identifying the ACK and FINISHED signals, a random ID gets generated for every command
  /// and used as part of the topic.
  static Future<void> sendCommand(
    String mac,
    String accessKey,
    Map<String, dynamic> command, {
    bool waitForFinished = false,
    int? timeout = 2000,
  }) async {
    if (!connected) await connect();
    if (!connected) return;

    String randomID = const Uuid().v4();
    command["randomID"] = randomID;

    Completer completer = Completer();

    finish() {
      if (!completer.isCompleted) {
        CommandList.programShouldBeCanceled.removeListener(finish);
        unsubscribe("$mac/$randomID/confirm");
        unsubscribe("$mac/connected");
        completer.complete();
      }
    }

    CommandList.programShouldBeCanceled.addListener(finish);

    // mark command as done, if the robot reconnected.
    await subscribe("$mac/connected", (_) {
      finish();
    });

    // wait async for confirmation
    await subscribe("$mac/$randomID/confirm", (data) {
      if (waitForFinished && data == "FINISHED" ||
          !waitForFinished && data == "ACK") {
        finish();
      }
    });

    // abort when no confirmation in timeout
    if (timeout != null && timeout > 0) {
      Future.delayed(Duration(milliseconds: timeout), () {
        finish();
      });
    }

    publish(jsonEncode(command), "$mac/$accessKey/command");

    Timer(const Duration(milliseconds: 500),
        () => publish(jsonEncode(command), "$mac/$accessKey/command"));
    return completer.future;
  }

  /// Closes the connection to the MQTT broker and cancels the ping timer.
  static void disconnect() async {
    if (_client!.connectionStatus!.state == MqttConnectionState.connected) {
      _client!.disconnect();
    }

    if (_timer != null) {
      _timer!.cancel();
    }

    _client = null;
  }
}
