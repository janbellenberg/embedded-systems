import 'package:mqtt_client/mqtt_client.dart';
import 'package:mqtt_client/mqtt_server_client.dart';
import 'package:uuid/uuid.dart';

import '../../model/redux/store.dart';

/// Creates a new MQTT Client using the default TCP-based protocol.
Future<MqttClient> getClient() async {
  // TODO: add TLS

  return MqttServerClient(
    store.state.hostname,
    'user#${store.state.user?.id}#${const Uuid().v4()}',
  ) /*..secure = true*/;
}
