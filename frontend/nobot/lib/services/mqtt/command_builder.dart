/// Collection of static command builder functions.
class CommandBuilder {
  /// Returns the command data for resetting the state of the robot.
  static Map<String, dynamic> setToZero() {
    return {
      "command": "set-zero",
    };
  }

  /// Returns the command data for setting the motor [speed] of the robot.
  static Map<String, dynamic> setSpeed(int speed) {
    return {
      "command": "set-speed",
      "speed": speed,
    };
  }

  /// Returns the command data for stopping the robot.
  static Map<String, dynamic> stop() {
    return {
      "command": "stop",
    };
  }

  /// Returns the command data for driving forward with no stop condition.
  static Map<String, dynamic> forward() {
    return {
      "command": "forward",
    };
  }

  /// Returns the command data for driving forward for a certain [distance] in cm.
  static Map<String, dynamic> forwardPosition(int distance) {
    return {
      "command": "forward",
      "distance": distance,
    };
  }

  /// Returns the command data for driving backward with no stop condition.
  static Map<String, dynamic> backward() {
    return {
      "command": "backward",
    };
  }

  /// Returns the command data for driving backward for certain [distance] in cm.
  static Map<String, dynamic> backwardPosition(int distance) {
    return {
      "command": "backward",
      "distance": distance,
    };
  }

  /// Returns the command data for turning left with no stop condition.
  static Map<String, dynamic> turnLeft() {
    return {
      "command": "left",
    };
  }

  /// Returns the command data for turning left for a certain [angle].
  static Map<String, dynamic> turnLeftAngle(int angle) {
    return {
      "command": "left",
      "angle": angle,
    };
  }

  /// Returns the command data for turning right with no stop condition.
  static Map<String, dynamic> turnRight() {
    return {
      "command": "right",
    };
  }

  /// Returns the command data for turning right for a certain [angle].
  static Map<String, dynamic> turnRightAngle(int angle) {
    return {
      "command": "right",
      "angle": angle,
    };
  }
}
