// !!!!! ONLY IMPORT WHEN COMPILING FOR THE WEB !!!!!

// ignore: avoid_web_libraries_in_flutter
import 'dart:html';

/// Checks, if the web-version of the app is running on the production server.
Future<bool> usesDefaultServer() async {
  return window.location.host == "nobot.janbellenberg.de";
}

/// Reads the current hostname of the web-version.
Future<String> readServerAddress() async {
  return window.location.hostname ?? "nobot.janbellenberg.de";
}

/// Updating the server on the web does not work!
Future<void> updateServerAddress(String? server) async {}
