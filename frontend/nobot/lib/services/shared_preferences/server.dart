import 'package:shared_preferences/shared_preferences.dart';

/// Checks, if a custom server was set in the settings or not.
Future<bool> usesDefaultServer() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  return prefs.getString("server") == null;
}

/// Reads the server address, stored in the settings.
///
/// If no server was specified, it will return the default production server.
Future<String> readServerAddress() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  return prefs.getString("server") ?? "nobot.janbellenberg.de";
}

/// Stores the [server] in the settings or removes it, if `null` was passed to the function.
Future<void> updateServerAddress(String? server) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  if (server == null) {
    prefs.remove("server");
  } else {
    prefs.setString("server", server);
  }
}
