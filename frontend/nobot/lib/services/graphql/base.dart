import 'dart:developer';

import 'package:flutter/foundation.dart';
import 'package:graphql/client.dart';

import '../../model/redux/store.dart';
import '../error/error_wrapper.dart';
import '../http_error.dart';
import '../../services/shared_preferences/server.dart'
    if (dart.library.html) '../../services/shared_preferences/server_web.dart';

/// Implementations for GraphQL API queries & mutations.
class GraphQL {
  /// Builds a new [GraphQLClient], based on the session id and the settings about the server.
  static Future<GraphQLClient> _getClient() async {
    final bool useProdServer = await usesDefaultServer();
    final httpLink = HttpLink(
      '${useProdServer ? "https" : "http"}://${store.state.hostname}:${useProdServer ? "443" : "80"}/api/graphql',
      defaultHeaders: {
        "session": store.state.sessionID ?? "",
      },
    );

    return GraphQLClient(
      cache: GraphQLCache(),
      link: httpLink,
    );
  }

  /// Performs the GraphQL query, specified in [options].
  ///
  /// If the query was successful and the client received data, it will be returned as a [Map],
  /// otherwise null will be returned.
  /// If an error occurred during the request, a [HttpException] for a unknown error will be thrown.
  /// If the result from the server indicates with a HTTP status code, that an
  /// error occurred, a [HttpException] with the code-specific error message will be thrown.
  ///
  /// See also:
  /// - [_getClient] which builds the client object.
  /// - [_getExceptionFromResult] which parses an error-response into a HttpException.
  static Future<Map<String, dynamic>?> query(QueryOptions options) async {
    QueryResult result;

    try {
      result = await (await _getClient()).query(options);
    } catch (e) {
      if (kDebugMode) log(e.toString());

      throw HttpException.defaultMessage(HTTPError.UNKNOWN);
    }

    if (result.hasException) {
      throw _getExceptionFromResult(result);
    }

    return result.data;
  }

  /// Performs the GraphQL mutation, specified in [options].
  ///
  /// If the mutation was successful and the client received data, it will be returned as a [Map],
  /// otherwise null will be returned.
  /// If an error occurred during the request, a [HttpException] for a unknown error will be thrown.
  /// If the result from the server indicates with a HTTP status code, that an
  /// error occurred, a [HttpException] with the code-specific error message will be thrown.
  ///
  /// See also:
  /// - [_getClient] which builds the client object.
  /// - [_getExceptionFromResult] which parses an error-response into a HttpException.
  static Future<Map<String, dynamic>?> mutate(MutationOptions options) async {
    QueryResult result;
    try {
      result = await (await _getClient()).mutate(options);
    } catch (e) {
      if (kDebugMode) log(e.toString());

      throw HttpException.defaultMessage(HTTPError.UNKNOWN);
    }

    if (result.hasException) {
      throw _getExceptionFromResult(result);
    }

    return result.data;
  }

  /// Builds an [HttpException], based on the error in the [result] of a GraphQL request.
  ///
  /// Returns a [HttpException] with [HTTPError.CONNECTION_ERROR] if the exception
  /// is a [NetworkException] or a [ServerException].
  ///
  /// If the exception is a [HttpLinkServerException], type of the resulting [HttpException]
  /// is based on the http status code.
  /// -  below 100 -> no real status code: [HTTPError.CONNECTION_ERROR]
  /// - 401 -> [HTTPError.NOT_AUTHORIZED]
  /// - 403 -> [HTTPError.FORBIDDEN]
  /// - 404 -> [HTTPError.NOT_FOUND]
  /// - 502 / 504 -> [HTTPError.PROXY_ERROR]
  /// - 500+ -> [HTTPError.SERVER_ERROR]
  /// - 400+ -> [HTTPError.CLIENT_ERROR]
  ///
  /// If the exceptions fits in none of those categories, [HTTPError.UNKNOWN] will be returned.
  static HttpException _getExceptionFromResult(QueryResult result) {
    LinkException? exception = result.exception!.linkException;

    int statusCode = 0;
    String? customMessage;

    // extract the statusCode and the message, if specified.
    if (exception is HttpLinkServerException) {
      statusCode = exception.response.statusCode;

      if ((exception.parsedResponse?.errors?.length ?? 0) > 0) {
        customMessage = exception.parsedResponse?.errors?[0].message;
      }
    }

    if (kDebugMode) log(customMessage ?? "no msg");

    if (statusCode < 100) {
      return HttpException.defaultMessage(HTTPError.CONNECTION_ERROR);
    } else if (statusCode == 401) {
      return customMessage == null
          ? HttpException.defaultMessage(HTTPError.NOT_AUTHORIZED)
          : HttpException(HTTPError.NOT_AUTHORIZED, customMessage);
    } else if (statusCode == 403) {
      return customMessage == null
          ? HttpException.defaultMessage(HTTPError.FORBIDDEN)
          : HttpException(HTTPError.FORBIDDEN, customMessage);
    } else if (statusCode == 404) {
      return customMessage == null
          ? HttpException.defaultMessage(HTTPError.NOT_FOUND)
          : HttpException(HTTPError.NOT_FOUND, customMessage);
    } else if (statusCode == 502 || statusCode == 504) {
      return HttpException.defaultMessage(HTTPError.PROXY_ERROR);
    } else if (statusCode >= 500) {
      return HttpException.defaultMessage(HTTPError.SERVER_ERROR);
    } else if (statusCode >= 400) {
      return HttpException.defaultMessage(HTTPError.CLIENT_ERROR);
    }

    if (exception is NetworkException || exception is ServerException) {
      return HttpException.defaultMessage(HTTPError.CONNECTION_ERROR);
    }

    return HttpException.defaultMessage(HTTPError.UNKNOWN);
  }
}
