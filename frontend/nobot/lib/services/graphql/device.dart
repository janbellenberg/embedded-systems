import 'package:gql/ast.dart';
import 'package:graphql/client.dart';

import 'base.dart';

/// Invokes the GraphQL mutation [_addDevice] for adding a new device by its [mac] address.
///
/// Returns always null.
/// Throws a [HttpException] if the request failed or caused an error.
///
/// See: [GraphQL.mutate]
Future<Map<String, dynamic>?> addDevice(String mac) async {
  GraphQL.mutate(
    MutationOptions(
      document: _addDevice,
      variables: <String, dynamic>{
        "mac": mac,
      },
    ),
  );

  return null;
}

/// Invokes the GraphQL mutation [_updateDeviceName] for updating the
/// [name] of the device with the [id].
///
/// Returns the parsed response data as a [Map] or null, if no data was fetched.
/// Throws a [HttpException] if the request failed or caused an error.
///
/// See: [GraphQL.mutate]
Future<Map<String, dynamic>?> updateDeviceName(int id, String name) async {
  return GraphQL.mutate(
    MutationOptions(
      document: _updateDeviceName,
      variables: <String, dynamic>{"id": id, "name": name},
    ),
  );
}

/// Invokes the GraphQL mutation [_deleteDevice] for remove a device by its [id].
///
/// Returns the parsed response data as a [Map] or null, if no data was fetched.
/// Throws a [HttpException] if the request failed or caused an error.
///
/// See: [GraphQL.mutate]
Future<Map<String, dynamic>?> deleteDevice(int id) async {
  GraphQL.mutate(
    MutationOptions(
      document: _deleteDevice,
      variables: <String, dynamic>{
        "id": id,
      },
    ),
  );

  return null;
}

DocumentNode _addDevice = gql("""
  mutation AddDevice(\$mac: String!) {
    addDevice(mac: \$mac)
  }
""");

DocumentNode _updateDeviceName = gql("""
  mutation UpdateDevice(\$id: ID!, \$name: String!) {
    updateDevice(id: \$id, name: \$name) {
      id
      name
      mac
    }
  }
""");

DocumentNode _deleteDevice = gql("""
  mutation DeleteDevice(\$id: ID!) {
    deleteDevice(id: \$id)
  }
""");
