import 'package:gql/ast.dart';
import 'package:graphql/client.dart';

import 'base.dart';

/// Invokes the GraphQL query [_getProgram] for fetching a program by its [id].
///
/// Returns the parsed response data as a [Map] or null, if no data was fetched.
/// Throws a [HttpException] if the request failed or caused an error.
///
/// See: [GraphQL.query]
Future<Map<String, dynamic>?> getProgramById(int id) async {
  return GraphQL.query(
    QueryOptions(
      document: _getProgram,
      variables: <String, dynamic>{
        "id": id,
      },
    ),
  );
}

/// Invokes the GraphQL mutation [_addProgram] for adding a new program with a [name].
///
/// Returns the parsed response data as a [Map] or null, if no data was fetched.
/// Throws a [HttpException] if the request failed or caused an error.
///
/// See: [GraphQL.mutate]
Future<Map<String, dynamic>?> addProgram(String name) async {
  return GraphQL.mutate(
    MutationOptions(
      document: _addProgram,
      variables: <String, dynamic>{
        "name": name,
      },
    ),
  );
}

/// Invokes the GraphQL mutation [_updateProgram] for updating the [content]
/// of a program with the [id].
///
/// Returns the parsed response data as a [Map] or null, if no data was fetched.
/// Throws a [HttpException] if the request failed or caused an error.
///
/// See: [GraphQL.mutate]
Future<Map<String, dynamic>?> updateProgram(int id, String content) async {
  return GraphQL.mutate(
    MutationOptions(
      document: _updateProgram,
      variables: <String, dynamic>{
        "id": id,
        "content": content,
      },
    ),
  );
}

/// Invokes the GraphQL mutation [_deleteProgram] for removing the program with the [id].
///
/// Returns always null.
/// Throws a [HttpException] if the request failed or caused an error.
///
/// See: [GraphQL.mutate]
Future<Map<String, dynamic>?> deleteProgram(int id) async {
  await GraphQL.mutate(
    MutationOptions(
      document: _deleteProgram,
      variables: <String, dynamic>{
        "id": id,
      },
    ),
  );

  return null;
}

DocumentNode _getProgram = gql("""
  query GetProgram(\$id: Int!) {
   program(id: \$id) {
    id
    name
    content
    }
  }
""");

DocumentNode _addProgram = gql("""
  mutation AddProgram(\$name: String!) {
    addProgram(name: \$name) {
      id
      name
      content
    }
  }
""");

DocumentNode _updateProgram = gql("""
  mutation UpdateProgram(\$id: ID!, \$content: String!) {
    updateProgram(id: \$id, content: \$content) {
      id
    }
  }
""");

DocumentNode _deleteProgram = gql("""
  mutation DeleteProgram(\$id: ID!) {
    deleteProgram(id: \$id)
  }
""");
