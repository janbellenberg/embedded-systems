import 'package:graphql/client.dart';
import 'package:gql/ast.dart';

import 'base.dart';

/// Invokes the GraphQL query [_dashboardQuery] for fetching user information
/// and the list of devices of the logged in user.
///
/// Returns the parsed response data as a [Map] or null, if no data was fetched.
/// Throws a [HttpException] if the request failed or caused an error.
///
/// See: [GraphQL.query]
Future<Map<String, dynamic>?> loadUserInfo() async {
  return GraphQL.query(
    QueryOptions(document: _dashboardQuery),
  );
}

/// Invokes the GraphQL query [loadProgramOfUser] for fetching the list of programs
/// of the logged in user.
///
/// Returns the parsed response data as a [Map] or null, if no data was fetched.
/// Throws a [HttpException] if the request failed or caused an error.
///
/// See: [GraphQL.query]
Future<Map<String, dynamic>?> loadProgramsOfUser() async {
  return GraphQL.query(
    QueryOptions(document: _programsOfUser),
  );
}

/// Invokes the GraphQL mutation [_updatePassword] for updating the [password]
/// of the user, which is currently logged in.
///
/// Returns always null.
/// Throws a [HttpException] if the request failed or caused an error.
///
/// See: [GraphQL.mutate]
Future<Map<String, dynamic>?> updatePassword(String password) async {
  await GraphQL.mutate(
    MutationOptions(
      document: _updatePassword,
      variables: <String, dynamic>{
        "password": password,
      },
    ),
  );
  return null;
}

/// Invokes the GraphQL mutation [_updateDesign] for updating whether
/// the [darkmode] is enabled or not.
///
/// Returns always null.
/// Throws a [HttpException] if the request failed or caused an error.
///
/// See: [GraphQL.mutate]
Future<Map<String, dynamic>?> updateDesign(bool darkmode) async {
  await GraphQL.mutate(
    MutationOptions(
      document: _updateDesign,
      variables: <String, dynamic>{
        "darkmode": darkmode,
      },
    ),
  );
  return null;
}

/// Invokes the GraphQL mutation [_signUp] for creating a new user
/// with a [username] and [password].
///
/// Returns the parsed response data as a [Map] or null, if no data was fetched.
/// This map contains the session id of the new session.
/// Throws a [HttpException] if the request failed or caused an error.
///
/// See: [GraphQL.mutate]
Future<Map<String, dynamic>?> signup(String username, String password) {
  return GraphQL.mutate(
    MutationOptions(
      document: _signUp,
      variables: <String, dynamic>{
        "username": username,
        "password": password,
      },
    ),
  );
}

/// Invokes the GraphQL mutation [_login] for creating a new session for the user
/// by authenticating with a [username] and a [password].
///
/// Returns the parsed response data as a [Map] or null, if no data was fetched.
/// This map contains the session id of the new session.
/// Throws a [HttpException] if the request failed or caused an error.
///
/// See: [GraphQL.mutate]
Future<Map<String, dynamic>?> login(String username, String password) {
  return GraphQL.mutate(
    MutationOptions(
      document: _login,
      variables: <String, dynamic>{
        "username": username,
        "password": password,
      },
    ),
  );
}

/// Invokes the GraphQL mutation [_deleteUser] for deleting the logged in user account
/// with all devices and programs.
///
/// Returns always null.
/// Throws a [HttpException] if the request failed or caused an error.
///
/// See: [GraphQL.mutate]
Future<Map<String, dynamic>?> deleteUser() async {
  await GraphQL.mutate(
    MutationOptions(
      document: _deleteUser,
    ),
  );
  return null;
}

/// Invokes the GraphQL mutation [_deleteUser] for terminating the current session.
///
/// Returns always null.
/// Throws a [HttpException] if the request failed or caused an error.
///
/// See: [GraphQL.mutate]
Future<Map<String, dynamic>?> logout() async {
  await GraphQL.mutate(
    MutationOptions(
      document: _logout,
    ),
  );
  return null;
}

DocumentNode _signUp = gql("""
  mutation AddUser(\$username: String!, \$password: String!) {
      addUser(username: \$username, password: \$password) {
        sessionID
    }
  }
""");

DocumentNode _deleteUser = gql("""
  mutation deleteUser() {
    deleteUser
  }
""");

DocumentNode _dashboardQuery = gql("""
  query GetUser() {
    user {
        id
        username
        darkmode
        devices {
            id
            name
            mac
        }
    }
  }
""");

DocumentNode _programsOfUser = gql("""
  query GetProgramsOfUser() {
    user {
        programs{
          name
          id
        }
    }
  }
""");

DocumentNode _updatePassword = gql("""
  mutation updatePassword(\$password: String) {
    updateUser(password: \$password) {
    
    }
  }
""");

DocumentNode _updateDesign = gql("""
  mutation updateDesign(\$darkmode: Boolean) {
        updateUser(darkmode: \$darkmode) {
        darkmode
    }
  }
""");

DocumentNode _login = gql("""
  mutation login(\$username: String!, \$password: String!) {
    login(username: \$username, password: \$password) {
      sessionID
    }
  }
""");

DocumentNode _logout = gql("""
  mutation logout() {
    logout
  }
""");
