import 'package:gql/ast.dart';
import 'package:graphql/client.dart';

import 'base.dart';

/// Invokes the GraphQL query [_versionQuery] for fetching the latest version of the app.
///
/// Returns the parsed response data as a [Map] or null, if no data was fetched.
/// Throws a [HttpException] if the request failed or caused an error.
///
/// See: [GraphQL.query]
Future<Map<String, dynamic>?> getVersion() async {
  return GraphQL.query(
    QueryOptions(
      document: _versionQuery,
    ),
  );
}

DocumentNode _versionQuery = gql("""
  {
    info {
      appVersion
    }
  }
""");
