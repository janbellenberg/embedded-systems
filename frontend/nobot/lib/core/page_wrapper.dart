import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:oktoast/oktoast.dart';

import '../dialogs/dialog_wrapper.dart';
import '../model/redux/app_state.dart';
import '../pages/login_page.dart';
import '../pages/offline_page.dart';
import '../pages/waiting_page.dart';

/// Wrapper widget which provides the [SafeArea] and [OKToast] widgets in the widget tree
/// and overloading the page with the [LoginPage], [OfflinePage] or [WaitingPage] is necessary.
class PageWrapper extends StatelessWidget {
  const PageWrapper(this.page, {Key? key, this.overrideLogin = false})
      : super(key: key);

  /// Showing a new [page] in the [context] as dialog if the Device is big enough or
  /// routing to the [page] if the screen is small or it is explicitly requested by setting [showInDialog] to false.
  ///
  /// LoginPage can be overridden by setting [overrideLogin] to true.
  static void routeToPage(
    Widget page,
    BuildContext context, {
    bool overrideLogin = false,
    bool showInDialog = true,
  }) {
    if (((MediaQuery.of(context).size.width < 500) &&
            MediaQuery.of(context).orientation == Orientation.portrait) ||
        !showInDialog) {
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => PageWrapper(page, overrideLogin: overrideLogin),
        ),
      );
    } else {
      showDialog(
        context: context,
        builder: (context) => DialogWrapper(
          isSubPage: true,
          children: [
            Container(
              constraints: BoxConstraints(
                maxHeight: MediaQuery.of(context).size.height * 0.9,
                maxWidth: MediaQuery.of(context).size.width * 0.9,
                minWidth: MediaQuery.of(context).size.width * 0.45,
              ),
              clipBehavior: Clip.antiAlias,
              decoration: const BoxDecoration(
                  borderRadius: BorderRadius.all(
                Radius.circular(15.0),
              )),
              child: page,
            )
          ],
        ),
      );
    }
  }

  final Widget page;
  final bool overrideLogin;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: OKToast(
        child: StoreConnector<AppState, AppState>(
          converter: (store) => store.state,
          builder: (context, state) {
            return Stack(
              children: [
                state.sessionID != null || overrideLogin
                    ? page
                    : const LoginPage(),
                if (state.runningTasks > 0 || !state.setupDone)
                  const WaitingPage(),
                if (!state.serverAvailable) const OfflinePage(),
              ],
            );
          },
        ),
      ),
    );
  }
}
