import 'dart:async';
import 'dart:convert';

import '../../controller/program.dart';
import '../blocks/command_list.dart';
import '../redux/actions.dart';
import '../redux/store.dart';

/// Representation of a block program.
class Program {
  /// Primary key of the program.
  int? id;

  /// User-defined name of the program.
  String? name;

  /// Gets the content of the program as a JSON string.
  String get content => jsonEncode({"blocks": parsed.toJSON()});

  /// Parses the program content from the JSON string [data].
  set content(String? data) {
    if (data == null) {
      parsed = CommandList(() {});
    } else {
      parsed = CommandList.fromJSON(
        jsonDecode(data)["blocks"],
        updateData,
      );
    }
  }

  /// Representation of the program content.
  late CommandList parsed;

  /// Timer for delaying updates at [updateData].
  Timer? _updateTimer;

  /// Whether the program content is in sync with the db or not.
  bool get isSynchronized => _isSynchronized;
  bool _isSynchronized = true;

  /// Creates an empty program.
  Program() {
    parsed = CommandList(updateData);
  }

  /// Creates an program with [id], [name] and the parsed [content].
  Program.fromData({this.id, this.name, String? content}) {
    if (content != null) {
      this.content = content;
    } else {
      // if no content submitted, create empty program.
      parsed = CommandList(updateData);
    }
  }

  /// Creates a new program and parses the [data] from a key/value map.
  /// Used for parsing an API response.
  Program.fromAPI(Map<String, dynamic> data) {
    id = data["id"];
    name = data["name"];
    content = data["content"];
  }

  /// Schedules the uploading of the program content if a change occurred.
  void updateData() {
    _isSynchronized = false;

    if (_updateTimer == null || !_updateTimer!.isActive) {
      _updateTimer = Timer(const Duration(seconds: 10), () {
        updateProgram(id ?? -1, content).then((value) {
          store.dispatch(
            Action(
              ActionTypes.updateProgram,
              payload: this.._isSynchronized = true,
            ),
          );
        });
      });
    }
    store.dispatch(Action(ActionTypes.updateProgram, payload: this));
  }
}
