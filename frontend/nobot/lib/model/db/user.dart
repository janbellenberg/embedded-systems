import 'device.dart';
import 'program.dart';

/// Representation of a user.
class User {
  /// Primary key of the user;
  int? id;

  /// username from the login.
  String? username;

  /// Whether the darkmode should be enabled.
  bool? darkmode;

  /// List of devices, owned by the user.
  late List<Device> devices;

  /// List of program, created by the user.
  late List<Program> programs;

  /// Creates an empty user.
  User() {
    devices = List.empty(growable: true);
    programs = List.empty(growable: true);
  }

  /// Creates a user with [id], [username] and [darkmode].
  User.fromData({this.id, this.username, this.darkmode}) {
    devices = List.empty(growable: true);
    programs = List.empty(growable: true);
  }

  /// Creates a new user instance and parses the [data] from a key/value map.
  /// It also parses the list of devices and programs.
  /// Used for parsing an API response.
  User.fromAPI(Map<String, dynamic> data) {
    id = data["id"];
    username = data["username"];
    darkmode = data["darkmode"];

    devices = List.empty(growable: true);
    programs = List.empty(growable: true);

    List<dynamic>? deviceList = data["devices"];

    if (deviceList != null) {
      for (Map<String, dynamic> item in deviceList) {
        devices.add(Device.fromAPI(item));
      }
    }

    List<dynamic>? programList = data["programs"];

    if (programList != null) {
      for (Map<String, dynamic> item in programList) {
        programs.add(Program.fromAPI(item));
      }
    }
  }

  /// Validates a given [username] and returns true if it is in a valid format.
  static bool validateUsername(String username) {
    return username.trim().isNotEmpty;
  }

  /// Validates a given [password] and returns true if it is in a valid format.
  static bool validatePassword(String password) {
    return password.trim().isNotEmpty;
  }
}
