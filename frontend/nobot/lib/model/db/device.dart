import 'dart:convert';

import 'package:flutter/material.dart';

import '../blocks/device_context.dart';
import '../redux/actions.dart' as redux;
import '../redux/store.dart';

/// Representation of a device.
class Device {
  /// Primary key of the device.
  int? id;

  /// User-defined name of the device.
  String? name;

  /// Unique MAC address of the device.
  String? mac;

  /// Hardware information about the current state of the robot.
  ValueNotifier<DeviceContext> context = ValueNotifier(DeviceContext());

  /// Current percentage of the battery level.
  int? batteryLevel;

  /// Random access key, used to authorize commands.
  String accessKey = "";

  /// Creates a device with an [id], a [name], a [mac] address and a [batteryLevel].
  Device.fromData({this.id, this.name, this.mac, this.batteryLevel});

  /// Creates a new device and parses the [data]  from a key/value map.
  /// Used for parsing an API response.
  Device.fromAPI(Map<String, dynamic> data) {
    id = data["id"];
    name = data["name"];
    mac = data["mac"];

    if (data.containsKey("key")) {
      accessKey = data["key"];
    }
  }

  /// Validates a given [mac] address with a RegExp and returns true, if valid.
  static bool validateMAC(String mac) {
    return RegExp(r"^(([a-fA-F0-9]{2}:){5}[a-fA-F0-9]{2})$").hasMatch(mac);
  }

  /// Parses the [data] string containing the x and y position and the angle,
  /// separated by a colon and updates the device.
  ///
  /// String format:
  /// `X:Y:Angle`
  set position(String data) {
    List<double> values = data.split(":").map((e) => double.parse(e)).toList();
    DeviceContext ctx = DeviceContext();

    ctx.distanceFront = context.value.distanceFront;
    ctx.distanceBack = context.value.distanceBack;
    ctx.positionX = values[0];
    ctx.positionY = values[1];
    ctx.angle = values[2];

    context.value = ctx;

    store.dispatch(redux.Action(redux.ActionTypes.setDevice, payload: this));
  }

  /// Parses the [data] string containing the front and back distance,
  /// separated by a colon and updates the device.
  ///
  /// String format:
  /// `front:back`
  set distance(String data) {
    List<double> values = data.split(":").map((e) => double.parse(e)).toList();
    DeviceContext ctx = DeviceContext();
    ctx.distanceFront = values[0];
    ctx.distanceBack = values[1];

    ctx.positionX = context.value.positionX;
    ctx.positionY = context.value.positionY;
    ctx.angle = context.value.angle;

    if (ctx.distanceFront > 0 && ctx.distanceBack > 0) {
      context.value = ctx;
      store.dispatch(redux.Action(redux.ActionTypes.setDevice, payload: this));
    }
  }

  /// Parses the [data] JSON string with status information from a robot and updates the device.
  set status(String data) {
    Map<String, dynamic> values = jsonDecode(data);
    batteryLevel = values["battery"];

    store.dispatch(redux.Action(redux.ActionTypes.setDevice, payload: this));
  }
}
