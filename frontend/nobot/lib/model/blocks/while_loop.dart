import 'dart:async';

import 'package:flutter/material.dart';
import 'package:uuid/uuid.dart';

import '../../widgets/blocks/while_loop_widget.dart';
import '../db/device.dart';
import 'command.dart';
import 'command_list.dart';
import 'device_context.dart';

/// Runs the commands in the [blockList] while the [condition] is true.
class WhileLoop implements AbstractCommand {
  /// Type of the block.
  static String get type => "WHILE";

  late final CommandList blockList;
  late final String condition;

  /// Whether the current block is currently running.
  @override
  bool get isRunning => _isRunning;
  bool _isRunning = false;

  /// Corresponding widget for the block.
  @override
  Widget get widget => _widget;

  @override
  set widget(Widget widget) {
    if (widget is WhileLoopWidget) {
      _widget = widget;
    }
  }

  late final WhileLoopWidget _widget;

  /// [onUpdate] will be called, if the program content changes.
  @override
  final void Function() onUpdate;

  /// [onDelete] will be called, if the block should be deleted.
  @override
  final void Function(AbstractCommand item) onDelete;

  /// Create a new while loop widget.
  WhileLoop({
    required this.onUpdate,
    required this.onDelete,
  }) {
    blockList = CommandList(onUpdate);
    condition = "true";
    widget = WhileLoopWidget(key: const Uuid().v4(), data: this);
  }

  /// Parses a while loop block from a API response.
  WhileLoop.fromJSON({
    required Map<String, dynamic> data,
    required this.onUpdate,
    required this.onDelete,
  }) {
    blockList = CommandList.fromJSON(data["items"], onUpdate);
    condition = data["condition"].toString();
    widget = WhileLoopWidget(key: const Uuid().v4(), data: this);
  }

  /// Validates recursively, if the current block contains errors.
  @override
  bool get hasErrors {
    if (blockList.blocks.isEmpty) return true;
    return blockList.blocks.map((e) => e.hasErrors).reduce(
              (value, element) => value |= element,
            ) ||
        !DeviceContext.checkExpressionSyntax(_widget.condition);
  }

  /// Executes the commands in the [blockList] while the condition is true.
  @override
  Future<void> run(Device device) async {
    if (CommandList.programShouldBeCanceled.value) return;

    _isRunning = true;
    onUpdate();

    Completer c = Completer();

    runBlocks() {
      if (device.context.value.evaluateExpression(_widget.condition)) {
        Timer.run(() async {
          for (AbstractCommand command in blockList.blocks) {
            if (CommandList.programShouldBeCanceled.value) {
              c.complete();
              return;
            }

            await command.run(device);
          }

          runBlocks();
        });
      } else {
        c.complete();
      }
    }

    runBlocks();

    await c.future;

    _isRunning = false;
    onUpdate();
  }

  /// Serializing the block into a [Map], which can be serialized into a string.
  @override
  Map<String, dynamic> toJSON() {
    return {
      "type": type,
      "condition": _widget.condition,
      "items": blockList.toJSON(),
    };
  }
}
