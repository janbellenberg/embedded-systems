import 'package:flutter/material.dart';
import 'package:uuid/uuid.dart';

import '../../widgets/blocks/empty_widget.dart';
import '../db/device.dart';
import 'command.dart';

/// Empty block for filling empty loops.
class EmptyCommand implements AbstractCommand {
  /// Type of the block.
  static String get type => "EMPTY";

  /// Whether the current block is currently running.
  @override
  bool get isRunning => false;

  /// Corresponding widget for the block.
  @override
  Widget get widget => _widget;

  @override
  set widget(Widget widget) {
    if (widget is EmptyCommandWidget) {
      _widget = widget;
    }
  }

  late final EmptyCommandWidget _widget;

  /// [onUpdate] will be called, if the program content changes.
  @override
  final void Function() onUpdate;

  /// [onDelete] will be called, if the block should be deleted.
  @override
  final void Function(AbstractCommand item) onDelete;

  /// Empty block has never errors.
  @override
  bool get hasErrors => false;

  /// Create a new empty command.
  EmptyCommand({
    required this.onUpdate,
    required this.onDelete,
  }) {
    widget = EmptyCommandWidget(key: const Uuid().v4(), data: this);
  }

  /// Create a new empty command.
  EmptyCommand.fromJSON({
    required Map<String, dynamic> data,
    required this.onUpdate,
    required this.onDelete,
  }) {
    widget = EmptyCommandWidget(key: const Uuid().v4(), data: this);
  }

  /// does nothing
  @override
  Future<void> run(Device device) async {}

  /// Serializing the block into a [Map], which can be serialized into a string.
  @override
  Map<String, dynamic> toJSON() {
    return {
      "type": type,
    };
  }
}
