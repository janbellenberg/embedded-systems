import 'package:flutter/material.dart';
import 'package:uuid/uuid.dart';

import '../../widgets/blocks/if_condition_widget.dart';
import '../db/device.dart';
import 'command.dart';
import 'command_list.dart';
import 'device_context.dart';

/// Running the [blockList] if the [condition] is true.
class IfCondition implements AbstractCommand {
  /// Type of the block.
  static String get type => "IF";

  late final CommandList blockList;
  late final String condition;

  /// Whether the current block is currently running.
  @override
  bool get isRunning => _isRunning;
  bool _isRunning = false;

  /// Corresponding widget for the block.
  @override
  Widget get widget => _widget;

  @override
  set widget(Widget widget) {
    if (widget is IfConditionWidget) {
      _widget = widget;
    }
  }

  late final IfConditionWidget _widget;

  /// [onUpdate] will be called, if the program content changes.
  @override
  final void Function() onUpdate;

  /// [onDelete] will be called, if the block should be deleted.
  @override
  final void Function(AbstractCommand item) onDelete;

  /// Create a new if condition block.
  IfCondition({
    required this.onUpdate,
    required this.onDelete,
  }) {
    blockList = CommandList(onUpdate);
    condition = "true";
    widget = IfConditionWidget(key: const Uuid().v4(), data: this);
  }

  /// Parses a if condition block from a API response.
  IfCondition.fromJSON({
    required Map<String, dynamic> data,
    required this.onUpdate,
    required this.onDelete,
  }) {
    blockList = CommandList.fromJSON(data["items"], onUpdate);
    condition = data["condition"].toString();
    widget = IfConditionWidget(key: const Uuid().v4(), data: this);
  }

  /// Validates recursively, if the current block contains errors.
  @override
  bool get hasErrors {
    if (blockList.blocks.isEmpty) return true;
    return blockList.blocks.map((e) => e.hasErrors).reduce(
              (value, element) => value |= element,
            ) ||
        !DeviceContext.checkExpressionSyntax(_widget.condition);
  }

  /// Runs the commands in [blockList] the if the [condition] is valid.
  @override
  Future<void> run(Device device) async {
    if (CommandList.programShouldBeCanceled.value) return;

    _isRunning = true;
    onUpdate();

    if (device.context.value.evaluateExpression(_widget.condition)) {
      for (AbstractCommand command in blockList.blocks) {
        if (CommandList.programShouldBeCanceled.value) {
          _isRunning = false;
          onUpdate();
          return;
        }
        await command.run(device);
      }
    }

    _isRunning = false;
    onUpdate();
  }

  /// Serializing the block into a [Map], which can be serialized into a string.
  @override
  Map<String, dynamic> toJSON() {
    return {
      "type": type,
      "condition": _widget.condition,
      "items": blockList.toJSON(),
    };
  }
}
