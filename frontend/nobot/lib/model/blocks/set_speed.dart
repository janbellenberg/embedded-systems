import 'package:flutter/material.dart';
import 'package:uuid/uuid.dart';

import '../../services/mqtt/base.dart';
import '../../services/mqtt/command_builder.dart';
import '../../widgets/blocks/set_speed_widget.dart';
import '../db/device.dart';
import 'command.dart';
import 'command_list.dart';

/// Sets the motor speed of the robot.
class SetSpeedCommand implements AbstractCommand {
  /// Type of the block.
  static String get type => "SPEED";

  int speed = 50;

  /// Whether the current block is currently running.
  @override
  bool get isRunning => _isRunning;
  bool _isRunning = false;

  /// Corresponding widget for the block.
  @override
  Widget get widget => _widget;

  @override
  set widget(Widget widget) {
    if (widget is SetSpeedWidget) {
      _widget = widget;
    }
  }

  late final SetSpeedWidget _widget;

  /// [onUpdate] will be called, if the program content changes.
  @override
  final void Function() onUpdate;

  /// [onDelete] will be called, if the block should be deleted.
  @override
  final void Function(AbstractCommand item) onDelete;

  /// Validates, if the entered speed is a valid int.
  @override
  bool get hasErrors => int.tryParse(_widget.controller.text) == null;

  /// Create a new set speed block.
  SetSpeedCommand({
    required this.onUpdate,
    required this.onDelete,
  }) {
    widget = SetSpeedWidget(key: const Uuid().v4(), data: this);
  }

  /// Parses a set speed block from a API response.
  SetSpeedCommand.fromJSON({
    required Map<String, dynamic> data,
    required this.onUpdate,
    required this.onDelete,
  }) {
    speed = int.parse(data["speed"].toString());
    widget = SetSpeedWidget(key: const Uuid().v4(), data: this);
  }

  /// Sends the new [speed] to the [device].
  @override
  Future<void> run(Device device) async {
    if (CommandList.programShouldBeCanceled.value) return;

    _isRunning = true;
    onUpdate();

    await MQTT.sendCommand(
      device.mac ?? "",
      device.accessKey,
      CommandBuilder.setSpeed(speed),
    );

    _isRunning = false;
    onUpdate();
  }

  /// Serializing the block into a [Map], which can be serialized into a string.
  @override
  Map<String, dynamic> toJSON() {
    return {
      "type": type,
      "speed": speed,
    };
  }
}
