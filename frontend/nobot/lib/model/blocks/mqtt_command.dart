import 'package:flutter/material.dart';
import 'package:uuid/uuid.dart';

import '../../services/mqtt/base.dart';
import '../../services/mqtt/command_builder.dart';
import '../../widgets/blocks/mqtt_command_widget.dart';
import '../db/device.dart';
import 'command.dart';
import 'command_list.dart';

/// Sending a MQTT command to the device.
class MqttCommand implements AbstractCommand {
  /// Type of the block.
  static String get type => "MQTT";

  Map<String, dynamic> command = CommandBuilder.forward();

  /// Whether the current block is currently running.
  @override
  bool get isRunning => _isRunning;
  bool _isRunning = false;

  /// Corresponding widget for the block.
  @override
  Widget get widget => _widget;

  @override
  set widget(Widget widget) {
    if (widget is MqttCommandWidget) {
      _widget = widget;
    }
  }

  late final MqttCommandWidget _widget;

  /// [onUpdate] will be called, if the program content changes.
  @override
  final void Function() onUpdate;

  /// [onDelete] will be called, if the block should be deleted.
  @override
  final void Function(AbstractCommand item) onDelete;

  /// MQTT command has never errors.
  @override
  bool get hasErrors => false;

  /// Create a new mqtt block.
  MqttCommand({
    required this.onUpdate,
    required this.onDelete,
  }) {
    widget = MqttCommandWidget(key: const Uuid().v4(), data: this);
  }

  /// Parses a mqtt command block from a API response.
  MqttCommand.fromJSON({
    required Map<String, dynamic> data,
    required this.onUpdate,
    required this.onDelete,
  }) {
    command = {
      "command": data["message"],
    };

    widget = MqttCommandWidget(key: const Uuid().v4(), data: this);
  }

  /// Sends the command to the device.
  @override
  Future<void> run(Device device) async {
    if (CommandList.programShouldBeCanceled.value) return;

    _isRunning = true;
    onUpdate();

    await MQTT.sendCommand(
      device.mac ?? "",
      device.accessKey,
      command,
      waitForFinished: false,
    );

    _isRunning = false;
    onUpdate();
  }

  /// Serializing the block into a [Map], which can be serialized into a string.
  @override
  Map<String, dynamic> toJSON() {
    return {
      "type": type,
      "message": command["command"],
    };
  }
}
