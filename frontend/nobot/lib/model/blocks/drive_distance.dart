import 'dart:async';

import 'package:flutter/material.dart';
import 'package:uuid/uuid.dart';

import '../../services/mqtt/base.dart';
import '../../services/mqtt/command_builder.dart';
import '../../widgets/blocks/drive_distance_widget.dart';
import '../db/device.dart';
import 'command.dart';
import 'command_list.dart';

/// Driving [forward] or backward for a given [distance].
class DriveDistance implements AbstractCommand {
  /// Type of the block.
  static String get type => "DIST-DRIVE";

  /// Whether the robot should drive forward or backward.
  bool forward = true;

  /// Length (in cm) of the route, the device should drive.
  int distance = 10;

  /// Whether the current block is currently running.
  @override
  bool get isRunning => _isRunning;
  bool _isRunning = false;

  /// Corresponding widget for the block.
  @override
  Widget get widget => _widget;
  @override
  set widget(Widget widget) {
    if (widget is DriveDistanceWidget) {
      _widget = widget;
    }
  }

  late final DriveDistanceWidget _widget;

  /// [onUpdate] will be called, if the program content changes.
  @override
  final void Function() onUpdate;

  /// [onDelete] will be called, if the block should be deleted.
  @override
  final void Function(AbstractCommand item) onDelete;

  /// Validates, if the entered distance string is a valid int.
  @override
  bool get hasErrors => int.tryParse(_widget.controller.text) == null;

  /// Creates a new distance driving block.
  DriveDistance({
    required this.onUpdate,
    required this.onDelete,
  }) {
    widget = DriveDistanceWidget(key: const Uuid().v4(), data: this);
  }

  /// Parses a distance driving block from a API response.
  DriveDistance.fromJSON({
    required Map<String, dynamic> data,
    required this.onUpdate,
    required this.onDelete,
  }) {
    forward = data["forward"];
    distance = int.parse(data["distance"].toString());
    widget = DriveDistanceWidget(key: const Uuid().v4(), data: this);
  }

  /// Runs the current block by instructing the [device] to drive [forward] or not,
  /// for the given [distance].
  @override
  Future<void> run(Device device) async {
    if (CommandList.programShouldBeCanceled.value) return;

    _isRunning = true;
    onUpdate();

    if (forward) {
      await MQTT.sendCommand(
        device.mac ?? "",
        device.accessKey,
        CommandBuilder.forwardPosition(distance),
        waitForFinished: true,
      );
    } else {
      await MQTT.sendCommand(
        device.mac ?? "",
        device.accessKey,
        CommandBuilder.backwardPosition(distance),
        waitForFinished: true,
      );
    }

    _isRunning = false;
    onUpdate();
  }

  /// Serializing the block into a [Map], which can be serialized into a string.
  @override
  Map<String, dynamic> toJSON() {
    return {
      "type": type,
      "forward": forward,
      "distance": distance,
    };
  }
}
