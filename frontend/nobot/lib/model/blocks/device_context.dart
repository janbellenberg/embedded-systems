import 'package:expression_language/expression_language.dart';
import 'package:petitparser/context.dart';
import 'package:petitparser/core.dart';

/// Wrapper for all device-related information, which may be included into
/// conditions.
class DeviceContext {
  /// X position of the device, relative to the start point.
  double positionX = 0;

  /// Y position of the device, relative to the start point.
  double positionY = 0;

  /// Angle of the robot in degrees.
  double angle = 0;

  /// Distance, measured by the front ultrasonic sensor in cm.
  double distanceFront = 0;

  /// Distance, measured by the back ultrasonic sensor in cm.
  double distanceBack = 0;

  /// Evaluates a [condition] and returns the result of the value of the expression.
  ///
  /// Before parsing the expression, all placeholders will be replaced by the actual values.
  bool evaluateExpression(String condition) {
    condition = condition
        .replaceAll("\$x", positionX.toString())
        .replaceAll("\$y", positionY.toString())
        .replaceAll("\$angle", angle.toString())
        .replaceAll("\$frontDistance", distanceFront.toString())
        .replaceAll("\$backDistance", distanceBack.toString());

    ExpressionGrammarParser expressionGrammarDefinition =
        ExpressionGrammarParser({});
    Parser parser = expressionGrammarDefinition.build();

    Result result = parser.parse(condition);

    Expression<bool> expression = result.value;
    return expression.evaluate();
  }

  /// Validates the syntax of the [expression] by trying to parse it.
  ///
  /// Before parsing, all placeholder will be replaced by 0s.
  static bool checkExpressionSyntax(String expression) {
    try {
      expression = expression
          .replaceAll("\$x", "0")
          .replaceAll("\$y", "0")
          .replaceAll("\$angle", "0")
          .replaceAll("\$frontDistance", "0")
          .replaceAll("\$backDistance", "0");

      ExpressionGrammarParser expressionGrammarDefinition =
          ExpressionGrammarParser({});
      Parser parser = expressionGrammarDefinition.build();

      Result result = parser.parse(expression);
      return result.isSuccess;
    } on Exception {
      return false;
    }
  }
}
