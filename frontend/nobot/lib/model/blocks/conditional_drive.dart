import 'dart:async';

import 'package:flutter/material.dart';
import 'package:uuid/uuid.dart';

import '../../services/mqtt/base.dart';
import '../../services/mqtt/command_builder.dart';
import '../../widgets/blocks/conditional_drive_widget.dart';
import '../db/device.dart';
import 'command.dart';
import 'command_list.dart';
import 'device_context.dart';

/// Driving forward or backward, as long as the given condition is true.
class ConditionalDrive implements AbstractCommand {
  /// Type of the block.
  static String get type => "COND-DRIVE";

  /// Whether the robot should drive forward or backward.
  bool forward = true;

  /// Condition that will be validated continuously.
  String condition = "true";

  /// Whether the current block is currently running.
  @override
  bool get isRunning => _isRunning;
  bool _isRunning = false;

  /// Corresponding widget for the block.
  @override
  Widget get widget => _widget;
  @override
  set widget(Widget widget) {
    if (widget is ConditionalDriveWidget) {
      _widget = widget;
    }
  }

  late final ConditionalDriveWidget _widget;

  /// [onUpdate] will be called, if the program content changes.
  @override
  final void Function() onUpdate;

  /// [onDelete] will be called, if the block should be deleted.
  @override
  final void Function(AbstractCommand item) onDelete;

  /// Validates, if the entered condition is valid.
  @override
  bool get hasErrors {
    return !DeviceContext.checkExpressionSyntax(_widget.condition);
  }

  /// Create a new conditional drive block.
  ConditionalDrive({
    required this.onUpdate,
    required this.onDelete,
  }) {
    widget = ConditionalDriveWidget(key: const Uuid().v4(), data: this);
  }

  /// Parses a conditional drive block from a API response.
  ConditionalDrive.fromJSON({
    required Map<String, dynamic> data,
    required this.onUpdate,
    required this.onDelete,
  }) {
    forward = data["forward"];
    condition = data["condition"].toString();
    widget = ConditionalDriveWidget(key: const Uuid().v4(), data: this);
  }

  /// Runs the current block by instructing the [device] to drive [forward] or not,
  /// evaluating the [condition] until it is false and then stopping the [device].
  @override
  Future<void> run(Device device) async {
    if (CommandList.programShouldBeCanceled.value) return;

    _isRunning = true;
    onUpdate();

    await MQTT.sendCommand(
      device.mac ?? "",
      device.accessKey,
      forward ? CommandBuilder.forward() : CommandBuilder.backward(),
      waitForFinished: false,
    );

    if (CommandList.programShouldBeCanceled.value) {
      _isRunning = false;
      onUpdate();
      return;
    }

    Completer c = Completer();

    validator() {
      if ((!device.context.value.evaluateExpression(_widget.condition) ||
              CommandList.programShouldBeCanceled.value) &&
          !c.isCompleted) {
        c.complete();
      }
    }

    device.context.addListener(validator);

    await c.future;

    device.context.removeListener(validator);

    if (CommandList.programShouldBeCanceled.value) {
      _isRunning = false;
      onUpdate();
      return;
    }

    await MQTT.sendCommand(
      device.mac ?? "",
      device.accessKey,
      CommandBuilder.stop(),
      waitForFinished: false,
    );

    _isRunning = false;
    onUpdate();
  }

  /// Serializing the block into a [Map], which can be serialized into a string.
  @override
  Map<String, dynamic> toJSON() {
    return {
      "type": type,
      "forward": forward,
      "condition": condition,
    };
  }
}
