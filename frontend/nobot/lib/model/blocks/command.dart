import 'package:flutter/material.dart';

import '../db/device.dart';

/// Super-class for all command blocks.
abstract class AbstractCommand {
  /// Create a new command.
  AbstractCommand({required this.onUpdate, required this.onDelete});

  /// Execute the command on the [device].
  Future<void> run(Device device);

  /// Serialize the command to a [Map]
  Map<String, dynamic> toJSON();

  /// Corresponding widget.
  late final Widget widget;

  /// Callback for updates.
  final void Function() onUpdate;

  /// Callback for deleting the program.
  final void Function(AbstractCommand item) onDelete;

  /// Whether the user-defined command parameters contain errors.
  bool get hasErrors;

  /// Whether the command is currently running.
  bool get isRunning;
}
