import 'dart:async';
import 'package:flutter/material.dart';
import 'package:nobot/services/mqtt/command_builder.dart';

import '../../services/mqtt/base.dart';
import '../db/device.dart';
import 'conditional_drive.dart';
import 'drive_distance.dart';
import 'empty.dart';
import 'rotate.dart';
import 'set_speed.dart';
import 'sleep.dart';
import 'command.dart';
import 'if_condition.dart';
import 'mqtt_command.dart';
import 'while_loop.dart';

/// Wrapper for the actual program content.
class CommandList {
  static ValueNotifier<bool> programShouldBeCanceled = ValueNotifier(false);

  /// List of commands in this program.
  List<AbstractCommand> get blocks => List.unmodifiable(_blocks);
  final List<AbstractCommand> _blocks = List.empty(growable: true);

  /// Validating the program by checking for errors recursively on every block.
  bool get hasErrors {
    if (_blocks.isEmpty) return true;
    return _blocks.map((e) => e.hasErrors).reduce(
          (value, element) => value |= element,
        );
  }

  /// [onUpdate] will be called, if the program content changes.
  void Function() onUpdate;

  /// Create an empty program.
  ///
  /// The [onUpdate] will be called, if the program changes.
  CommandList(this.onUpdate);

  /// Parses a command list from a API response by checking the `type`
  /// and calling the according constructor.
  CommandList.fromJSON(List<dynamic> raw, this.onUpdate) {
    for (Map<String, dynamic> block in raw) {
      if (block["type"] == MqttCommand.type) {
        _blocks.add(
          MqttCommand.fromJSON(
            data: block,
            onUpdate: onUpdate,
            onDelete: deleteItem,
          ),
        );
      } else if (block["type"] == WhileLoop.type) {
        _blocks.add(
          WhileLoop.fromJSON(
            data: block,
            onUpdate: onUpdate,
            onDelete: deleteItem,
          ),
        );
      } else if (block["type"] == IfCondition.type) {
        _blocks.add(
          IfCondition.fromJSON(
            data: block,
            onUpdate: onUpdate,
            onDelete: deleteItem,
          ),
        );
      } else if (block["type"] == Sleep.type) {
        _blocks.add(
          Sleep.fromJSON(
            data: block,
            onUpdate: onUpdate,
            onDelete: deleteItem,
          ),
        );
      } else if (block["type"] == EmptyCommand.type) {
        _blocks.add(
          EmptyCommand.fromJSON(
            data: block,
            onUpdate: onUpdate,
            onDelete: deleteItem,
          ),
        );
      } else if (block["type"] == SetSpeedCommand.type) {
        _blocks.add(
          SetSpeedCommand.fromJSON(
            data: block,
            onUpdate: onUpdate,
            onDelete: deleteItem,
          ),
        );
      } else if (block["type"] == ConditionalDrive.type) {
        _blocks.add(
          ConditionalDrive.fromJSON(
            data: block,
            onUpdate: onUpdate,
            onDelete: deleteItem,
          ),
        );
      } else if (block["type"] == DriveDistance.type) {
        _blocks.add(
          DriveDistance.fromJSON(
            data: block,
            onUpdate: onUpdate,
            onDelete: deleteItem,
          ),
        );
      } else if (block["type"] == Rotate.type) {
        _blocks.add(
          Rotate.fromJSON(
            data: block,
            onUpdate: onUpdate,
            onDelete: deleteItem,
          ),
        );
      }
    }
  }

  /// Converts the program into a [Map] by recursively calling every block's `toJSON` method.
  List<Map<String, dynamic>> toJSON() {
    return _blocks.map((block) => block.toJSON()).toList();
  }

  /// Run the program by recursively calling ever block's `run` method.
  Future<void> run(Device device) async {
    for (AbstractCommand cmd in _blocks) {
      if (programShouldBeCanceled.value) break;

      await cmd.run(device);
      await Future.delayed(const Duration(milliseconds: 500));
    }

    programShouldBeCanceled.value = false;

    MQTT.sendCommand(
      device.mac ?? "",
      device.accessKey,
      CommandBuilder.stop(),
      waitForFinished: false,
    );
  }

  /// Appends a new [command] to the program and invokes [onUpdate] callback.
  void addBlock(AbstractCommand command) {
    _blocks.add(command);
    onUpdate();
  }

  /// Moves a block from the [oldIndex] to the [newIndex].
  ///
  /// After the change, the [onUpdate] callback will be called.
  void changeItemPosition(int oldIndex, int newIndex) {
    if (oldIndex < newIndex) {
      newIndex -= 1;
    }
    final AbstractCommand item = _blocks.removeAt(oldIndex);
    _blocks.insert(newIndex, item);

    onUpdate();
  }

  /// Removes an [item] from the program and invokes [onUpdate] callback.
  void deleteItem(AbstractCommand item) {
    _blocks.remove(item);
    onUpdate();
  }
}
