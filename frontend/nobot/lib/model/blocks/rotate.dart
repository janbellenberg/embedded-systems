import 'dart:async';

import 'package:flutter/material.dart';
import 'package:uuid/uuid.dart';

import '../../services/mqtt/base.dart';
import '../../services/mqtt/command_builder.dart';
import '../../widgets/blocks/rotate_widget.dart';
import '../db/device.dart';
import 'command.dart';
import 'command_list.dart';

/// Rotating the robot to [left] or right by a certain [angle].
class Rotate implements AbstractCommand {
  /// Type of the block.
  static String get type => "ROTATE";

  bool left = true;
  int angle = 10;

  /// Whether the current block is currently running.
  @override
  bool get isRunning => _isRunning;
  bool _isRunning = false;

  /// Corresponding widget for the block.
  @override
  Widget get widget => _widget;

  @override
  set widget(Widget widget) {
    if (widget is RotateWidget) {
      _widget = widget;
    }
  }

  late final RotateWidget _widget;

  /// [onUpdate] will be called, if the program content changes.
  @override
  final void Function() onUpdate;

  /// [onDelete] will be called, if the block should be deleted.
  @override
  final void Function(AbstractCommand item) onDelete;

  /// Validates, if the entered angle is a valid int.
  @override
  bool get hasErrors => int.tryParse(_widget.controller.text) == null;

  /// Create a new rotate block.
  Rotate({
    required this.onUpdate,
    required this.onDelete,
  }) {
    widget = RotateWidget(key: const Uuid().v4(), data: this);
  }

  /// Parses a rotate block from a API response.
  Rotate.fromJSON({
    required Map<String, dynamic> data,
    required this.onUpdate,
    required this.onDelete,
  }) {
    left = data["left"];
    angle = int.parse(data["angle"].toString());
    widget = RotateWidget(key: const Uuid().v4(), data: this);
  }

  /// send the turn command with the [angle] to the [device].
  @override
  Future<void> run(Device device) async {
    if (CommandList.programShouldBeCanceled.value) return;

    _isRunning = true;
    onUpdate();

    if (left) {
      await MQTT.sendCommand(
        device.mac ?? "",
        device.accessKey,
        CommandBuilder.turnLeftAngle(angle),
        waitForFinished: true,
      );
    } else {
      await MQTT.sendCommand(
        device.mac ?? "",
        device.accessKey,
        CommandBuilder.turnRightAngle(angle),
        waitForFinished: true,
      );
    }

    _isRunning = false;
    onUpdate();
  }

  /// Serializing the block into a [Map], which can be serialized into a string.
  @override
  Map<String, dynamic> toJSON() {
    return {
      "type": type,
      "left": left,
      "angle": angle,
    };
  }
}
