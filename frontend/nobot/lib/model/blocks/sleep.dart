import 'dart:async';

import 'package:flutter/material.dart';
import 'package:uuid/uuid.dart';

import '../../widgets/blocks/sleep_widget.dart';
import '../db/device.dart';
import 'command.dart';
import 'command_list.dart';

/// Delays the program execution for a [duration] in seconds.
class Sleep implements AbstractCommand {
  /// Type of the block.
  static String get type => "SLEEP";

  double duration = 1.0;

  /// Whether the current block is currently running.
  @override
  bool get isRunning => _isRunning;
  bool _isRunning = false;

  /// Corresponding widget for the block.
  @override
  Widget get widget => _widget;

  @override
  set widget(Widget widget) {
    if (widget is SleepWidget) {
      _widget = widget;
    }
  }

  late final SleepWidget _widget;

  /// [onUpdate] will be called, if the program content changes.
  @override
  final void Function() onUpdate;

  /// [onDelete] will be called, if the block should be deleted.
  @override
  final void Function(AbstractCommand item) onDelete;

  /// Validates, if the entered duration is a valid double.
  @override
  bool get hasErrors => double.tryParse(_widget.controller.text) == null;

  /// Create a new sleep block.
  Sleep({
    required this.onUpdate,
    required this.onDelete,
  }) {
    widget = SleepWidget(key: const Uuid().v4(), data: this);
  }

  /// Parses a sleep block from a API response.
  Sleep.fromJSON({
    required Map<String, dynamic> data,
    required this.onUpdate,
    required this.onDelete,
  }) {
    duration = double.parse(data["duration"].toString());
    widget = SleepWidget(key: const Uuid().v4(), data: this);
  }

  /// Delays the program execution for the [duration].
  @override
  Future<void> run(Device device) async {
    if (CommandList.programShouldBeCanceled.value) return;

    _isRunning = true;
    onUpdate();

    int start = DateTime.now().millisecondsSinceEpoch;

    Completer c = Completer();
    Timer.periodic(const Duration(milliseconds: 50), (timer) {
      int now = DateTime.now().millisecondsSinceEpoch;
      if (now - start > duration * 1000 ||
          CommandList.programShouldBeCanceled.value) {
        timer.cancel();
        c.complete();
      }
    });

    await c.future;

    _isRunning = false;
    onUpdate();
  }

  /// Serializing the block into a [Map], which can be serialized into a string.
  @override
  Map<String, dynamic> toJSON() {
    return {
      "type": type,
      "duration": duration,
    };
  }
}
