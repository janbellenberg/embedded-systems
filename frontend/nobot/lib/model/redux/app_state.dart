import '../db/user.dart';

/// Representation of all information, that should be available globally.
class AppState {
  /// ID of the current session.
  String? sessionID;

  // Address of the server, that should be used.
  String hostname = "nobot.janbellenberg.de";

  /// Number of running API tasks, if >0 a waiting screen is been displayed.
  int runningTasks = 0;

  /// Additional indicator for showing the waiting screen.
  /// It helps to prevent the screen from flickering at startup
  /// when its takes a few ms to send the first request to the server.
  bool setupDone = false;

  /// Whether a connection to the server could be created.
  /// If false, a offline screen will be shown.
  bool serverAvailable = true;

  /// Representation of all the data from db.
  User? user;
}
