import 'dart:developer';

import 'package:flutter/foundation.dart';

import '../db/device.dart';
import '../db/program.dart';
import 'actions.dart';
import 'app_state.dart';

/// Redux reducer function for dispatching an [action] on the current [state] by
AppState appReducer(AppState state, dynamic action) {
  if (action is! Action) return state;

  if (action.type == ActionTypes.updateSessionID) {
    state.sessionID = action.payload;
  } else if (action.type == ActionTypes.startTask) {
    state.runningTasks++;
  } else if (action.type == ActionTypes.stopTask) {
    state.runningTasks--;
  } else if (action.type == ActionTypes.setupDone) {
    state.setupDone = true;
  } else if (action.type == ActionTypes.setOffline) {
    state.serverAvailable = false;
  } else if (action.type == ActionTypes.setHostname) {
    state.hostname = action.payload;

    if (kDebugMode) {
      log("use server ${action.payload}");
    }
  } else if (action.type == ActionTypes.setUser) {
    state.user = action.payload;
  } else if (action.type == ActionTypes.setDevices) {
    state = _setDevices(state, action.payload);
  } else if (action.type == ActionTypes.setDevice) {
    state = _setDevice(state, action.payload);
  } else if (action.type == ActionTypes.clearDevices) {
    state = _clearDevices(state);
  } else if (action.type == ActionTypes.setPrograms) {
    state = _setPrograms(state, action.payload);
  } else if (action.type == ActionTypes.clearPrograms) {
    state = _clearPrograms(state);
  } else if (action.type == ActionTypes.addProgram) {
    state = _addProgram(state, action.payload);
  } else if (action.type == ActionTypes.updateProgram) {
    state = _updateProgram(state, action.payload);
  } else if (action.type == ActionTypes.deleteProgram) {
    state = _deleteProgram(state, action.payload);
  } else if (action.type == ActionTypes.reset) {
    return AppState();
  }

  return state;
}

/// Manipulates the [state] by clearing all programs.
/// Returns the new state.
AppState _clearPrograms(AppState state) {
  if (state.user != null) {
    state.user!.programs.clear();
  }

  return state;
}

/// Manipulates the [state] by setting the list of programs to the [payload].
/// Returns the new state.
AppState _setPrograms(AppState state, dynamic payload) {
  if (state.user != null) {
    state.user!.programs = payload;
  }

  return state;
}

/// Manipulates the [state] by adding [payload] to the list of programs.
/// Returns the new state.
AppState _addProgram(AppState state, dynamic payload) {
  if (payload is! Program) return state;
  if (state.user == null) return state;

  state.user!.programs.add(payload);
  return state;
}

/// Manipulates the [state] by updating the program with [payload]'s id with the payload.
/// Returns the new state.
AppState _updateProgram(AppState state, dynamic payload) {
  if (payload is! Program) return state;

  if (state.user == null) return state;

  state.user!.programs = state.user!.programs
      .map(
        (e) => e.id == payload.id ? payload : e,
      )
      .toList();

  return state;
}

/// Manipulates the [state] by removing the program with the id [payload].
/// Returns the new state.
AppState _deleteProgram(AppState state, dynamic payload) {
  if (payload is! int) return state;
  if (state.user == null) return state;

  state.user!.programs.removeWhere((element) => element.id == payload);
  return state;
}

/// Manipulates the [state] by clearing the list of devices.
/// Returns the new state.
AppState _clearDevices(AppState state) {
  if (state.user != null) {
    state.user!.devices.clear();
  }

  return state;
}

/// Manipulates the [state] by updating the device with [payload]'s id with payload.
/// Returns the new state.
AppState _setDevice(AppState state, dynamic payload) {
  if (payload is! Device) return state;

  int deviceID = payload.id ?? -1;
  if (state.user != null) {
    state.user!.devices = state.user!.devices
        .map((e) => (e.id == deviceID ? payload : e))
        .toList();
  }

  return state;
}

/// Manipulates the [state] by setting the list of devices to [payload].
/// Returns the new state.
AppState _setDevices(AppState state, dynamic payload) {
  if (state.user != null) {
    state.user!.devices = payload;
  }

  return state;
}
