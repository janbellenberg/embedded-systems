/// List of actions, that are supported by the redux reducer function.
enum ActionTypes {
  updateSessionID,
  setOffline,
  startTask,
  stopTask,
  setupDone,
  setHostname,
  setUser,
  setDevices,
  setDevice,
  clearDevices,
  setPrograms,
  addProgram,
  updateProgram,
  deleteProgram,
  clearPrograms,
  reset,
}

/// Representation of a redux action with its [type] and optional [payload].
class Action {
  ActionTypes type;
  dynamic payload;

  Action(this.type, {this.payload});
}
