import 'package:flutter/material.dart';

import '../constants.dart';
import '../controller/device.dart';
import '../core/page_wrapper.dart';
import '../model/db/device.dart';
import '../pages/scan_qr_page.dart';
import '../services/error/error_toast.dart';
import '../widgets/text_button_widget.dart';
import 'dialog_wrapper.dart';

/// Dialog for entering the MAC address of a robot, that should be added.
class AddDeviceDialog extends StatefulWidget {
  const AddDeviceDialog({super.key});

  @override
  State<AddDeviceDialog> createState() => _AddDeviceDialogState();
}

class _AddDeviceDialogState extends State<AddDeviceDialog> {
  bool _macIsValid = false;
  final TextEditingController _controller = TextEditingController();

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return DialogWrapper(
      title: "Gerät hinzufügen",
      children: [
        // enable QR-Code scanner on mobile platform
        if (!isDesktop)
          SizedBox(
            width: 300.0,
            child: Column(
              children: [
                TextButtonWidget(
                  onPressed: () {
                    PageWrapper.routeToPage(
                      ScanQrPage(onSuccess: (code) {
                        setState(() {
                          _macIsValid = Device.validateMAC(code);
                        });

                        if (_macIsValid) {
                          addDevice(code);
                          Navigator.pop(context);
                          showMessage(
                              "Gerät ist verfügbar, wenn es aktiviert wird.");
                        }
                      }),
                      context,
                    );
                  },
                  text: "QR Code scannen",
                  leadingIcon: const Icon(Icons.qr_code),
                ),
                const Padding(
                  padding: EdgeInsets.symmetric(
                    vertical: 20.0,
                  ),
                  child: Text(
                    "oder manuell eingeben",
                    style: TextStyle(
                      fontSize: 17.0,
                    ),
                  ),
                ),
              ],
            ),
          ),
        // textfield for adding mac manual.
        TextField(
          cursorColor: Theme.of(context).colorScheme.primary,
          maxLength: 17,
          decoration: const InputDecoration(
            labelText: 'Geräte-ID',
          ),
          controller: _controller,
          onChanged: (value) {
            setState(() => _macIsValid = Device.validateMAC(value));
          },
        ),
        if (!_macIsValid)
          const Text(
            "Geräte-ID ist falsch",
            style: TextStyle(color: Colors.red),
          ),
        Padding(
          padding: const EdgeInsets.only(top: 20.0),
          child: TextButtonWidget(
            text: "Gerät hinzufügen",
            onPressed: () {
              if (_macIsValid) {
                addDevice(_controller.text).then((_) {
                  Navigator.pop(context);
                  showMessage("Gerät ist verfügbar, wenn es aktiviert wird.");
                });
              }
            },
          ),
        )
      ],
    );
  }
}
