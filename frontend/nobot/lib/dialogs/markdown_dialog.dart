import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_markdown/flutter_markdown.dart';
import 'package:url_launcher/url_launcher.dart';

import 'dialog_wrapper.dart';

/// Dialog for rendering markdown from a asset file.
class MarkdownDialog extends StatelessWidget {
  /// Dialog for rendering markdown from a asset file.
  const MarkdownDialog({required this.filename, super.key});

  final String filename;

  @override
  Widget build(BuildContext context) {
    return DialogWrapper(
      isSubPage: true,
      children: [
        SizedBox(
          height: MediaQuery.of(context).size.height * 0.8,
          width: MediaQuery.of(context).size.width * 0.8,
          child: Padding(
            padding: const EdgeInsets.all(20.0),
            child: FutureBuilder(
              future: rootBundle.loadString("assets/docs/$filename"),
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.done) {
                  return SingleChildScrollView(
                    child: MarkdownBody(
                      selectable: true,
                      styleSheet: MarkdownStyleSheet.fromTheme(
                        Theme.of(context),
                      ).copyWith(
                        textScaleFactor: 1.1,
                        blockquoteDecoration: BoxDecoration(
                          color: Theme.of(context).colorScheme.background,
                        ),
                      ),
                      data: snapshot.data ?? "",
                      onTapLink: (text, url, title) {
                        launchUrl(Uri.parse(url ?? ""));
                      },
                    ),
                  );
                }
                return const Center(
                  child: SizedBox(
                    height: 50,
                    width: 50,
                    child: CircularProgressIndicator(),
                  ),
                );
              },
            ),
          ),
        ),
      ],
    );
  }
}
