import 'dart:ui';
import 'package:flutter/material.dart';

// Wrapper widget for dialogs with blurred background.
class DialogWrapper extends StatelessWidget {
  /// Creates a [SimpleDialog] with a [title], [children] and blurred background.
  ///
  /// If dialog is a page on mobile and only a dialog on desktop [isSubPage] should be true,
  /// to disable padding and the title.
  const DialogWrapper({
    this.title = "NoBot",
    this.children,
    this.isSubPage = false,
    Key? key,
  }) : super(key: key);

  final bool isSubPage;
  final String title;
  final List<Widget>? children;

  @override
  Widget build(BuildContext context) {
    return BackdropFilter(
      filter: ImageFilter.blur(sigmaX: 5, sigmaY: 5),
      child: SimpleDialog(
        surfaceTintColor: Theme.of(context).colorScheme.background,
        backgroundColor: Theme.of(context).colorScheme.background,
        shadowColor: const Color.fromARGB(255, 97, 97, 97),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15.0),
        ),
        contentPadding:
            isSubPage ? const EdgeInsets.all(0) : const EdgeInsets.all(25.0),
        title: isSubPage
            ? null
            : Text(
                title,
                overflow: TextOverflow.fade,
                style: const TextStyle(
                  fontSize: 25.0,
                ),
              ),
        children: children,
      ),
    );
  }
}
