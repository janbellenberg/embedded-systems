import 'package:flutter/material.dart';

import 'dialog_wrapper.dart';

/// Dialog for selecting the operating system (e.g. for software download)
///
/// Only the operating systems with a callback will be shown in the dialog.
class OsSelectionDialog extends StatelessWidget {
  /// Dialog for selecting the operating system for installing the server
  /// on a custom computer.
  const OsSelectionDialog({
    this.onWindowsSelected,
    this.onLinuxSelected,
    this.onMacSelected,
    this.onAndroidSelected,
    this.onIOSSelected,
    this.children = const [],
    super.key,
  });

  final Function()? onWindowsSelected;
  final Function()? onLinuxSelected;
  final Function()? onMacSelected;
  final Function()? onAndroidSelected;
  final Function()? onIOSSelected;
  final List<ListTile> children;

  @override
  Widget build(BuildContext context) {
    return DialogWrapper(
      title: "Betriebssystem wählen",
      children: [
        if (onWindowsSelected != null)
          ListTile(
            leading: const Icon(Icons.window),
            title: const Text("Windows"),
            onTap: onWindowsSelected,
          ),
        if (onMacSelected != null)
          ListTile(
            leading: const Icon(Icons.apple),
            title: const Text("macOS"),
            onTap: onMacSelected,
          ),
        if (onLinuxSelected != null)
          ListTile(
            leading: const Icon(Icons.bolt),
            title: const Text("Linux"),
            onTap: onLinuxSelected,
          ),
        if (onAndroidSelected != null)
          ListTile(
            leading: const Icon(Icons.android),
            title: const Text("Android"),
            onTap: onAndroidSelected,
          ),
        if (onIOSSelected != null)
          ListTile(
            leading: const Icon(Icons.phone_iphone),
            title: const Text("iOS"),
            onTap: onIOSSelected,
          ),
        for (ListTile child in children) child
      ],
    );
  }
}
