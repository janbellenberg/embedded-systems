import 'package:flutter/material.dart';

import '../model/blocks/command.dart';
import '../model/blocks/command_list.dart';
import '../model/blocks/conditional_drive.dart';
import '../model/blocks/drive_distance.dart';
import '../model/blocks/empty.dart';
import '../model/blocks/if_condition.dart';
import '../model/blocks/mqtt_command.dart';
import '../model/blocks/rotate.dart';
import '../model/blocks/set_speed.dart';
import '../model/blocks/sleep.dart';
import '../model/blocks/while_loop.dart';
import 'dialog_wrapper.dart';

/// Dialog for selecting the type of block, which should be added to the program.
class AddBlockDialog extends StatelessWidget {
  /// Creates the dialog with a reference to the [commandList] of the program.
  const AddBlockDialog({required this.commandList, super.key});

  /// List of commands in the program.
  final CommandList commandList;

  /// Helper method for instantiating a new concrete [AbstractCommand] base on the [type].
  void _createItem(String type) {
    AbstractCommand newBlock;
    switch (type) {
      case "MQTT":
        newBlock = MqttCommand(
          onUpdate: commandList.onUpdate,
          onDelete: commandList.deleteItem,
        );
        break;

      case "WHILE":
        newBlock = WhileLoop(
          onUpdate: commandList.onUpdate,
          onDelete: commandList.deleteItem,
        );
        break;

      case "IF":
        newBlock = IfCondition(
          onUpdate: commandList.onUpdate,
          onDelete: commandList.deleteItem,
        );
        break;

      case "SLEEP":
        newBlock = Sleep(
          onUpdate: commandList.onUpdate,
          onDelete: commandList.deleteItem,
        );
        break;

      case "EMPTY":
        newBlock = EmptyCommand(
          onUpdate: commandList.onUpdate,
          onDelete: commandList.deleteItem,
        );
        break;

      case "SPEED":
        newBlock = SetSpeedCommand(
          onUpdate: commandList.onUpdate,
          onDelete: commandList.deleteItem,
        );
        break;

      case "COND-DRIVE":
        newBlock = ConditionalDrive(
          onUpdate: commandList.onUpdate,
          onDelete: commandList.deleteItem,
        );
        break;
      case "DIST-DRIVE":
        newBlock = DriveDistance(
          onUpdate: commandList.onUpdate,
          onDelete: commandList.deleteItem,
        );
        break;
      case "ROTATE":
        newBlock = Rotate(
          onUpdate: commandList.onUpdate,
          onDelete: commandList.deleteItem,
        );
        break;

      default:
        return;
    }

    commandList.addBlock(newBlock);
  }

  @override
  Widget build(BuildContext context) {
    return DialogWrapper(
      title: "Block hinzufügen",
      children: [
        ListTile(
          title: const Text("Einfacher Befehl"),
          onTap: () {
            _createItem(MqttCommand.type);
            Navigator.pop(context);
          },
        ),
        ListTile(
          title: const Text("Schleife"),
          onTap: () {
            _createItem(WhileLoop.type);
            Navigator.pop(context);
          },
        ),
        ListTile(
          title: const Text("Abfrage"),
          onTap: () {
            _createItem(IfCondition.type);
            Navigator.pop(context);
          },
        ),
        ListTile(
          title: const Text("Warten"),
          onTap: () {
            _createItem(Sleep.type);
            Navigator.pop(context);
          },
        ),
        ListTile(
          title: const Text("Leerer Block"),
          onTap: () {
            _createItem(EmptyCommand.type);
            Navigator.pop(context);
          },
        ),
        ListTile(
          title: const Text("Fahren mit Bedingung"),
          onTap: () {
            _createItem(ConditionalDrive.type);
            Navigator.pop(context);
          },
        ),
        ListTile(
          title: const Text("Fahren mit Distanz"),
          onTap: () {
            _createItem(DriveDistance.type);
            Navigator.pop(context);
          },
        ),
        ListTile(
          title: const Text("Drehen mit Winkel"),
          onTap: () {
            _createItem(Rotate.type);
            Navigator.pop(context);
          },
        ),
        ListTile(
          title: const Text("Geschwindigkeit setzen"),
          onTap: () {
            _createItem(SetSpeedCommand.type);
            Navigator.pop(context);
          },
        ),
        TextButton(
          onPressed: () => Navigator.pop(context),
          child: const Text(
            "Abbrechen",
            style: TextStyle(
              color: Colors.red,
            ),
          ),
        ),
      ],
    );
  }
}
