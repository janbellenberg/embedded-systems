import 'package:flutter/material.dart';

import '../controller/program.dart';
import '../widgets/text_button_widget.dart';
import 'dialog_wrapper.dart';

/// Dialog for entering the name of a new program, that should be added.
class AddProgramDialog extends StatelessWidget {
  /// Dialog for entering the name of a new program, that should be added.
  AddProgramDialog({super.key});

  final TextEditingController _controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return DialogWrapper(
      title: "Programm hinzufügen",
      children: [
        TextField(
          autofocus: true,
          decoration: const InputDecoration(hintText: "Name"),
          controller: _controller,
          maxLength: 50,
        ),
        Padding(
          padding: const EdgeInsets.only(top: 20.0),
          child: TextButtonWidget(
            onPressed: () {
              if (_controller.text.isNotEmpty) {
                addProgram(_controller.text);
                Navigator.pop(context);
              }
            },
            text: "Hinzufügen",
            isPrimary: true,
          ),
        )
      ],
    );
  }
}
