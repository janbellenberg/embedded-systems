import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

import 'controller/info.dart';
import 'core/page_wrapper.dart';
import 'model/redux/app_state.dart';
import 'model/redux/store.dart';
import 'pages/fatal_page.dart';
import 'pages/home_page.dart';
import 'services/shared_preferences/server.dart'
    if (dart.library.html) 'services/shared_preferences/server_web.dart';
import 'services/shared_preferences/session.dart';
import 'model/redux/actions.dart' as redux;
import 'themes/dark.dart';
import 'themes/light.dart';

/// Start point of the app.
void main() {
  WidgetsFlutterBinding.ensureInitialized();

  // define custom error screen, instead of the flutter-default red screen.
  ErrorWidget.builder = (FlutterErrorDetails details) => const FatalPage();

  // load server address
  readServerAddress().then((value) {
    store.dispatch(
      redux.Action(
        redux.ActionTypes.setHostname,
        payload: value,
      ),
    );

    // load session id
    readSessionID().then((value) {
      store.dispatch(
        redux.Action(redux.ActionTypes.updateSessionID, payload: value),
      );

      if (value == null) {
        store.dispatch(
          redux.Action(redux.ActionTypes.setupDone),
        );
      }
    });

    // check app version
    checkAppVersion().then((value) {
      store.dispatch(
        redux.Action(redux.ActionTypes.setupDone),
      );
    });
  });

  // start the app
  runApp(const MyApp());
}

/// Entry point widget for the app.
class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return StoreProvider<AppState>(
      store: store,
      child: StoreConnector<AppState, AppState>(
        converter: ((store) => store.state),
        builder: ((context, state) {
          return MaterialApp(
            title: 'NoBot',
            theme: lightTheme,
            darkTheme: darkTheme,
            themeMode: state.user?.darkmode ?? false
                ? ThemeMode.dark
                : ThemeMode.light,
            debugShowCheckedModeBanner: false,
            supportedLocales: const [Locale("de", "DE")],
            localizationsDelegates: const [
              GlobalCupertinoLocalizations.delegate,
              GlobalMaterialLocalizations.delegate,
              GlobalWidgetsLocalizations.delegate,
            ],
            home: Builder(builder: (context) {
              SystemChrome.setSystemUIOverlayStyle(
                SystemUiOverlayStyle(
                  statusBarColor: Theme.of(context).colorScheme.primary,
                  statusBarBrightness: Brightness.light,
                  systemNavigationBarColor:
                      Theme.of(context).colorScheme.background,
                  systemNavigationBarIconBrightness:
                      Theme.of(context).colorScheme.brightness ==
                              Brightness.dark
                          ? Brightness.light
                          : Brightness.dark,
                ),
              );
              return const PageWrapper(
                HomePage(),
              );
            }),
          );
        }),
      ),
    );
  }
}
