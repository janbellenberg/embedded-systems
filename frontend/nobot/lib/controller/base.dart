import 'dart:developer';

import 'package:flutter/foundation.dart';

import '../model/redux/store.dart';
import '../model/redux/actions.dart' as redux;
import '../services/error/error_toast.dart';
import '../services/error/error_wrapper.dart';
import '../services/http_error.dart';

/// Runs the [operation] with a loading screen and applies the [parser] to it.
///
/// If no loading screen should be shown, set [background] to true.
/// If the [operation] throws and [HttpException], it gets handled,
/// by showing the offline screen for a [HTTPError.CONNECTION_ERROR],
/// showing the login screen for [HTTPError.NOT_AUTHORIZED] or just showing the error message else.
///
/// Returns the parsed data or null, if an error occurred.
Future<T?> runApiService<T>({
  required Future<Map<String, dynamic>?> Function() operation,
  required T Function(Map<String, dynamic> parser) parser,
  bool background = false,
}) async {
  // activate wait screen
  if (!background) {
    store.dispatch(redux.Action(redux.ActionTypes.startTask));
  }

  Map<String, dynamic>? result;
  try {
    // perform api fetch
    result = await operation();
  } on HttpException catch (e) {
    // handle error
    if (kDebugMode) log(e.message);

    if (e.error == HTTPError.CONNECTION_ERROR) {
      // go to offline page, because the app was not able to connect to the server
      store.dispatch(redux.Action(redux.ActionTypes.setOffline));
    } else if (e.error == HTTPError.NOT_AUTHORIZED) {
      // goto login screen, if application cannot load correctly
      showError(e.message);
      store.dispatch(
        redux.Action(redux.ActionTypes.updateSessionID, payload: null),
      );
    } else {
      showError(e.message);
    }

    if (!background) {
      store.dispatch(redux.Action(redux.ActionTypes.stopTask));
    }

    return null;
  } catch (e) {
    if (kDebugMode) log(e.toString());

    showError(HttpException.unknownMessage, showOffline: true);

    if (!background) {
      store.dispatch(redux.Action(redux.ActionTypes.stopTask));
    }

    return null;
  }

  T? parsed;

  if (result != null) {
    try {
      parsed = parser(result);
    } catch (e) {
      showError(HttpException.unknownMessage);
    }
  }

  // disable wait screen
  if (!background) {
    store.dispatch(redux.Action(redux.ActionTypes.stopTask));
  }

  return parsed;
}
