import 'package:flutter/services.dart';
import 'package:yaml/yaml.dart';

import '../services/error/error_toast.dart';
import 'base.dart';
import '../services/graphql/info.dart' as service;

/// Invokes the GraphQL Query [service.getVersion] for fetching the latest version of the app and
/// comparing it to the value specified in the pubspec.yml file.
///
/// Performs the API Operation with the [runApiService] wrapper.
/// Only the major and the minor number are considered.
Future<void> checkAppVersion() async {
  double? latestVersion = await runApiService(
      operation: () => service.getVersion(),
      parser: (result) =>
          double.tryParse(result["info"]["appVersion"].toString()) ?? -1);

  if (latestVersion == null || latestVersion < 0) {
    return;
  }

  dynamic pubspec = loadYaml(await rootBundle.loadString("pubspec.yaml"));
  String versionString = pubspec["version"]
      .toString()
      .split("+")[0]
      .split(".")
      .sublist(0, 2)
      .join(".");

  double currentVersion = double.parse(versionString);

  if (currentVersion < latestVersion) {
    showMessage("Es ist eine neuere Version verfügbar.");
  }
}
