import '../model/db/user.dart';
import '../model/redux/actions.dart';
import '../model/redux/store.dart';
import 'base.dart';
import '../services/graphql/user.dart' as service;
import '../services/shared_preferences/session.dart';

/// Invokes the GraphQL Query [service.loadProgramsOfUser]
/// for fetching all needed information about the logged in user.
///
/// Performs the API Operation with the [runApiService] wrapper.
/// The data will be parsed by [User.fromAPI]. If no result was fetched,
/// the app goes to the login page by setting the session id to null.
/// Else, the parsed data will be stored in the redux [store].
Future<void> loadUserInfo() async {
  User? result = await runApiService(
    operation: () => service.loadUserInfo(),
    parser: (result) => User.fromAPI(result["user"]),
  );

  if (result == null) {
    store.dispatch(
      Action(
        ActionTypes.updateSessionID,
        payload: null,
      ),
    );
    return;
  }

  store.dispatch(
    Action(
      ActionTypes.setUser,
      payload: result,
    ),
  );
}

/// Invokes the GraphQL Mutation [service.updatePassword] to update the [password] of the logged in user.
///
/// Performs the API Operation with the [runApiService] wrapper. No data will be fetched.
Future<void> updatePassword(String password) async {
  await runApiService(
    operation: () => service.updatePassword(password),
    parser: (_) {},
  );
}

/// Updates the design in the redux [store] and invokes the GraphQL Mutation [service.updateDesign]
/// in the background to update the [design] of the logged in user.
///
/// design: true => dark, false => light
/// Performs the API Operation with the [runApiService] wrapper. No data will be fetched.
Future<void> updateDesign(bool design) async {
  store.dispatch(
    Action(
      ActionTypes.setUser,
      payload: store.state.user!..darkmode = design,
    ),
  );

  await runApiService(
    operation: () => service.updateDesign(design),
    parser: (_) {},
    background: true,
  );
}

/// Invokes the GraphQL Mutation [service.signup] to create a new user account with a [username] and a [password].
///
/// Performs the API Operation with the [runApiService] wrapper and fetches the new session id.
/// After a successful api call, the new session id will be stored in the redux [store] and persisted at the device's storage.
Future<bool> signup(String username, String password) async {
  String? session = await runApiService(
    operation: () => service.signup(username, password),
    parser: (result) => result["addUser"]["sessionID"].toString(),
  );

  if (session == null) return false;

  await saveSessionID(session); // persist session id

  store.dispatch(Action(
    ActionTypes.updateSessionID,
    payload: session,
  ));

  return true;
}

/// Invokes the GraphQL Mutation [service.login] to create a new session by authenticating by the [username] and [password].
///
/// Performs the API Operation with the [runApiService] wrapper and fetches the new session id.
/// After a successful api call, the new session id will be stored in the redux [store] and persisted at the devices storage.
Future<void> login(String username, String password) async {
  String? session = await runApiService(
      operation: () => service.login(username, password),
      parser: (result) => result["login"]["sessionID"].toString());

  if (session == null) return;

  await saveSessionID(session);

  store.dispatch(Action(
    ActionTypes.updateSessionID,
    payload: session,
  ));
}

/// Invokes the GraphQL Mutation [service.logout] to end the current session and going back to the login screen.
///
/// Performs the API Operation with the [runApiService] wrapper. No data will be fetched.
/// After the api-call the session id is being removed from the redux [store] and the devices storage.
Future<void> logout() async {
  await runApiService(
    operation: () => service.logout(),
    parser: (result) {},
  );

  await saveSessionID(null);

  store.dispatch(Action(
    ActionTypes.updateSessionID,
    payload: null,
  ));

  store.dispatch(Action(
    ActionTypes.setUser,
    payload: null,
  ));
}

/// Invokes the GraphQL Mutation [service.deleteUser] to delete the users account with all programs, associated devices and sessions.
///
/// Performs the API Operation with the [runApiService] wrapper. No data will be fetched.
/// After the api-call the session id is being removed from the redux [store] and the devices storage.
Future<void> deleteUser() async {
  await runApiService(
    operation: () => service.deleteUser(),
    parser: (result) {},
  );

  await saveSessionID(null);

  store.dispatch(Action(
    ActionTypes.updateSessionID,
    payload: null,
  ));
}
