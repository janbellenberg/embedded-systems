import '../model/db/device.dart';
import '../model/redux/actions.dart';
import '../model/redux/store.dart';
import 'base.dart';
import 'user.dart';
import '../services/graphql/device.dart' as service;

/// Invokes the GraphQL mutation [service.addDevice] for creating a new device
/// by its [mac] address for the currently logged in user.
///
/// Performs the API Operation with the [runApiService] wrapper. No result gets parsed.
/// Because the device must first be activated by the device itself before use, no data will be fetched.
Future<void> addDevice(String mac) async {
  await runApiService(
    operation: () => service.addDevice(mac),
    parser: (_) {},
  );
}

/// Invokes the GraphQL mutation [service.updateDeviceName] for updating the [name] of a device, specified by the [id].
///
/// The device must be owned by the currently logged in user.
/// Performs the API Operation with the [runApiService] wrapper.
/// After updating, the new device data gets fetched, parsed by [Device.fromAPI]
/// and stored in the redux [store].
Future<void> updateDeviceName(int id, String name) async {
  Device? result = await runApiService(
    operation: () => service.updateDeviceName(id, name),
    parser: (result) => Device.fromAPI(result["updateDevice"]),
  );

  // don't proceed if an error occurred.
  if (result == null) return;

  store.dispatch(
    Action(
      ActionTypes.setDevice,
      payload: result,
    ),
  );
}

/// Invokes the GraphQL mutation [service.deleteDevice] for deleting a device, specified by the [id].
///
/// The device must be owned by the currently logged in user.
/// Performs the API Operation with the [runApiService] wrapper.
/// After updating, the user with all devices, is being re-fetched, by using the [loadUserInfo] method.
Future<void> deleteDevice(int id) async {
  await runApiService(
    operation: () => service.deleteDevice(id),
    parser: (_) {},
  );

  // reload data
  await loadUserInfo();
}
