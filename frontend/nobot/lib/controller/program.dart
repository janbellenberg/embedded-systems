import '../model/db/program.dart';
import '../model/db/user.dart';
import '../model/redux/actions.dart';
import '../model/redux/store.dart';
import '../services/graphql/user.dart' as user_service;
import '../services/graphql/program.dart' as service;
import 'base.dart';

/// Invokes the GraphQL Query [user_service.loadProgramsOfUser] to load the list
/// of programs, created by the user.
///
/// Performs the API Operation with the [runApiService] wrapper.
/// Fetches the id and the name of each program and the content of the first program
/// and stores the information in the [store].
Future<void> loadProgramList() async {
  User? result = await runApiService(
    operation: () => user_service.loadProgramsOfUser(),
    parser: (result) => User.fromAPI(result["user"]),
  );

  if (result == null) return;

  store.dispatch(
    Action(
      ActionTypes.setPrograms,
      payload: result.programs,
    ),
  );

  if (result.programs.isNotEmpty) {
    Program? program = await runApiService(
      operation: () => service.getProgramById(result.programs[0].id ?? -1),
      parser: (result) => Program.fromAPI(result["program"]),
    );

    if (program?.content == null) return;

    result.programs[0].content = program!.content;

    store.dispatch(
      Action(
        ActionTypes.updateProgram,
        payload: result.programs[0],
      ),
    );
  }
}

/// Invokes the GraphQL Query [service.getProgramById] to load the content of a program with the [id].
///
/// Performs the API Operation with the [runApiService] wrapper.
/// The fetched data will be written to the redux [store], so ALL attributes of the program must be fetched.
Future<void> loadProgram(int id) async {
  Program? result = await runApiService(
    operation: () => service.getProgramById(id),
    parser: (data) => Program.fromAPI(data["program"]),
  );

  if (result == null) return;

  store.dispatch(
    Action(
      ActionTypes.updateProgram,
      payload: result,
    ),
  );
}

/// Invokes the GraphQL Mutation [service.addProgram] to add a new program by with the [name].
///
/// Performs the API Operation with the [runApiService] wrapper.
/// The new program get fetched, parsed by [Program.fromAPI] and
/// appended to the redux [store].
Future<void> addProgram(String name) async {
  Program? result = await runApiService(
    operation: () => service.addProgram(name),
    parser: (data) => Program.fromAPI(data["addProgram"]),
  );

  if (result == null) return;

  store.dispatch(
    Action(
      ActionTypes.addProgram,
      payload: result,
    ),
  );
}

/// Invokes the GraphQL Mutation [service.updateProgram]
/// for updating the json-serialized [content] of the program with the specified [id].
///
/// Performs the API Operation with the [runApiService] wrapper in the background.
/// No data will be fetched and parsed.
Future<void> updateProgram(int id, String content) async {
  await runApiService(
    operation: () => service.updateProgram(id, content),
    parser: (_) {},
    background: true,
  );
}

/// Invokes the GraphQL Mutation [service.deleteProgram]
/// for deleting the program with the specified [id].
/// Performs the API Operation with the [runApiService] wrapper in the background.
/// No data will be fetched and parsed, however the program gets deleted from the redux [store].
Future<void> deleteProgram(int id) async {
  await runApiService(
    operation: () => service.deleteProgram(id),
    parser: (_) {},
  );

  store.dispatch(
    Action(
      ActionTypes.deleteProgram,
      payload: id,
    ),
  );
}
