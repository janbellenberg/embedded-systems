import 'package:flutter/material.dart';

/// Creates a widget for a [title] - [value] pair.
///
/// A [unit] for the [value] can also be specified.
/// By enabling the [whiteSpaceAfterValue], a whitespace will be inserted between [value] and [unit].
class NameValueWidget extends StatelessWidget {
  const NameValueWidget({
    required this.title,
    required this.value,
    this.unit = "",
    this.margin = EdgeInsets.zero,
    this.whiteSpaceAfterValue = true,
    Key? key,
  }) : super(key: key);

  final String title;
  final dynamic value;
  final String unit;
  final EdgeInsets margin;
  final bool whiteSpaceAfterValue;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: margin,
      child: LayoutBuilder(
        builder: (context, constraints) {
          return Row(
            children: [
              SizedBox(
                width: constraints.maxWidth / 2,
                child: Text(
                  "$title:",
                  textAlign: TextAlign.right,
                  style: const TextStyle(
                    fontSize: 18.0,
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 10.0),
                child: Text(
                  '$value${whiteSpaceAfterValue ? " " : ""}$unit',
                  style: const TextStyle(
                    fontSize: 18.0,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              )
            ],
          );
        },
      ),
    );
  }
}
