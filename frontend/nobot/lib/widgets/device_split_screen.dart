import 'package:flutter/material.dart';

import '../model/db/device.dart';
import 'device_control_widget.dart';
import 'programming_widget.dart';

/// Creates the split screen view for the [DeviceControlWidget] and the [ProgrammingWidget].
///
/// This widget should only be used on large screens!
class DeviceSplitScreen extends StatelessWidget {
  const DeviceSplitScreen({
    super.key,
    required this.device,
    required this.height,
  });

  final Device device;
  final double height;

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        SizedBox(
          width: 400,
          height: height - 100,
          child: SingleChildScrollView(
            child: DeviceControlWidget(device),
          ),
        ),
        Container(
          width: 1,
          height: (height - 100) * 0.8,
          color: Theme.of(context).cardColor,
        ),
        Expanded(
          child: SizedBox(
            height: height - 100,
            child: ProgrammingWidget(device),
          ),
        ),
      ],
    );
  }
}
