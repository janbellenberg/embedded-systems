import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';

import '../../model/blocks/sleep.dart';
import '../../model/redux/app_state.dart';

/// Widget for the [Sleep] block.
///
/// This widget should be used inside of a list widget, because it is a [ListTile].
/// It contains a [TextField] for the duration in seconds.
class SleepWidget extends StatefulWidget {
  SleepWidget({
    required String key,
    required this.data,
  }) : super(key: ValueKey(key));

  final Sleep data;
  late final TextEditingController controller = TextEditingController.fromValue(
    TextEditingValue(
      text: data.duration.toString(),
    ),
  );

  @override
  State<SleepWidget> createState() => _SleepWidgetState();
}

class _SleepWidgetState extends State<SleepWidget> {
  bool get _isValid => double.tryParse(widget.controller.text) != null;

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, AppState>(
      converter: ((store) => store.state),
      builder: (context, state) {
        return ListTile(
          title: Container(
            decoration: BoxDecoration(
              border: Border(
                left: widget.data.isRunning
                    ? const BorderSide(color: Colors.red, width: 3.0)
                    : BorderSide(color: Theme.of(context).highlightColor),
              ),
            ),
            padding: const EdgeInsets.only(left: 10.0),
            child: Row(
              children: [
                const Text("Warte"),
                Container(
                  width: 300.0,
                  margin: const EdgeInsets.symmetric(horizontal: 20.0),
                  child: TextField(
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(
                      errorText: _isValid ? null : "Nicht gültig",
                    ),
                    onChanged: (value) => setState(() {
                      widget.data.duration = double.tryParse(value) ?? 0;
                      widget.data.onUpdate();
                    }),
                    controller: widget.controller,
                  ),
                ),
                const Text("Sekunden"),
                const Spacer(),
                Padding(
                  padding: const EdgeInsets.only(right: 20.0),
                  child: IconButton(
                    onPressed: () => widget.data.onDelete(widget.data),
                    icon: const Icon(Icons.delete),
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
