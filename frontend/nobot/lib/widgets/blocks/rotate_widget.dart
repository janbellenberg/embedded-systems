import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';

import '../../model/blocks/rotate.dart';
import '../../model/redux/app_state.dart';

/// Widget for the [Rotate] block.
///
/// This widget should be used inside of a list widget, because it is a [ListTile].
/// It contains a [DropdownButton] for the direction (left / right) and a [TextField] for the angle.
class RotateWidget extends StatefulWidget {
  late final TextEditingController controller = TextEditingController.fromValue(
    TextEditingValue(
      text: data.angle.toString(),
    ),
  );
  String get condition => controller.text;

  final Rotate data;

  RotateWidget({
    required String key,
    required this.data,
  }) : super(key: ValueKey(key));

  @override
  State<RotateWidget> createState() => _RotateWidgetState();
}

class _RotateWidgetState extends State<RotateWidget> {
  bool get _isValid => double.tryParse(widget.controller.text) != null;

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, AppState>(
      converter: ((store) => store.state),
      builder: (context, state) {
        return ListTile(
          title: Container(
            decoration: BoxDecoration(
              border: Border(
                left: widget.data.isRunning
                    ? const BorderSide(color: Colors.red, width: 3.0)
                    : BorderSide(color: Theme.of(context).highlightColor),
              ),
            ),
            padding: const EdgeInsets.only(left: 10.0),
            child: Row(
              children: [
                const Text(
                  "Drehe",
                ),
                Container(
                  margin: const EdgeInsets.symmetric(horizontal: 20.0),
                  child: DropdownButton(
                    items: const [
                      DropdownMenuItem(
                        value: "left",
                        child: Text(
                          "Links",
                        ),
                      ),
                      DropdownMenuItem(
                        value: "right",
                        child: Text(
                          "Rechts",
                        ),
                      ),
                    ],
                    value: widget.data.left ? "left" : "right",
                    onChanged: (value) {
                      widget.data.left = (value ?? "left") == "left";
                      widget.data.onUpdate();
                    },
                  ),
                ),
                const Text(
                  "für",
                ),
                Container(
                  width: 300.0,
                  margin: const EdgeInsets.symmetric(horizontal: 20.0),
                  child: TextField(
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(
                      errorText: _isValid ? null : "Nicht gültig",
                    ),
                    onChanged: (value) => setState(() {
                      widget.data.angle = int.tryParse(value) ?? 0;
                      widget.data.onUpdate();
                    }),
                    controller: widget.controller,
                  ),
                ),
                const Text(
                  "Grad",
                ),
                const Spacer(),
                Padding(
                  padding: const EdgeInsets.only(right: 20.0),
                  child: IconButton(
                    onPressed: () => setState(
                      () => widget.data.onDelete(widget.data),
                    ),
                    icon: const Icon(Icons.delete),
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
