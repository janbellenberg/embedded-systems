import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';

import '../../model/blocks/empty.dart';
import '../../model/redux/app_state.dart';

/// Widget for the [EmptyCommand] block.
///
/// This widget should be used inside of a list widget, because it is a [ListTile].
/// It only contains a [Text], which indicates the the block does nothing.
class EmptyCommandWidget extends StatelessWidget {
  final EmptyCommand data;

  EmptyCommandWidget({
    required String key,
    required this.data,
  }) : super(key: ValueKey(key));

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, AppState>(
      converter: ((store) => store.state),
      builder: (context, state) {
        return ListTile(
          title: Container(
            decoration: BoxDecoration(
              border: Border(
                left: data.isRunning
                    ? const BorderSide(color: Colors.red, width: 3.0)
                    : BorderSide(color: Theme.of(context).highlightColor),
              ),
            ),
            padding: const EdgeInsets.only(left: 10.0),
            child: Row(
              children: [
                const Text(
                  "Tue nichts",
                ),
                const Spacer(),
                Padding(
                  padding: const EdgeInsets.only(right: 20.0),
                  child: IconButton(
                    onPressed: () => () => data.onDelete(data),
                    icon: const Icon(Icons.delete),
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
