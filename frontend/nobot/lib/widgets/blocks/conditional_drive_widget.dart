import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';

import '../../model/blocks/conditional_drive.dart';
import '../../model/blocks/device_context.dart';
import '../../model/redux/app_state.dart';

/// Widget for the [ConditionalDrive] block.
///
/// This widget should be used inside of a list widget, because it is a [ListTile].
/// It contains a [DropdownButton] for the direction and a [TextField] for the condition.
class ConditionalDriveWidget extends StatefulWidget {
  late final TextEditingController _conditionController;
  String get condition => _conditionController.text;

  final ConditionalDrive data;

  ConditionalDriveWidget({
    required String key,
    required this.data,
  }) : super(key: ValueKey(key)) {
    _conditionController = TextEditingController.fromValue(
      TextEditingValue(
        text: data.condition,
      ),
    );
  }

  @override
  State<ConditionalDriveWidget> createState() => _ConditionalDriveWidgetState();
}

class _ConditionalDriveWidgetState extends State<ConditionalDriveWidget> {
  bool _conditionValid = true;

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, AppState>(
      converter: ((store) => store.state),
      builder: (context, state) {
        return ListTile(
          title: Container(
            decoration: BoxDecoration(
              border: Border(
                left: widget.data.isRunning
                    ? const BorderSide(color: Colors.red, width: 3.0)
                    : BorderSide(color: Theme.of(context).highlightColor),
              ),
            ),
            padding: const EdgeInsets.only(left: 10.0),
            child: Row(
              children: [
                const Text(
                  "Fahre",
                ),
                Container(
                  margin: const EdgeInsets.symmetric(horizontal: 20.0),
                  child: DropdownButton(
                    items: const [
                      DropdownMenuItem(
                        value: "forward",
                        child: Text(
                          "Vorwärts",
                        ),
                      ),
                      DropdownMenuItem(
                        value: "backward",
                        child: Text(
                          "Rückwärts",
                        ),
                      ),
                    ],
                    value: widget.data.forward ? "forward" : "backward",
                    onChanged: (value) {
                      widget.data.forward = (value ?? "forward") == "forward";
                      widget.data.onUpdate();
                    },
                  ),
                ),
                const Text(
                  "solange",
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Container(
                  width: 300.0,
                  margin: const EdgeInsets.only(left: 20.0),
                  child: TextField(
                    decoration: InputDecoration(
                      errorText: _conditionValid ? null : "Nicht gültig",
                    ),
                    onChanged: _conditionChanged,
                    controller: widget._conditionController,
                  ),
                ),
                const Spacer(),
                Padding(
                  padding: const EdgeInsets.only(right: 20.0),
                  child: IconButton(
                    onPressed: () => setState(
                      () => widget.data.onDelete(widget.data),
                    ),
                    icon: const Icon(Icons.delete),
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  void _conditionChanged(String value) {
    setState(() {
      _conditionValid = DeviceContext.checkExpressionSyntax(widget.condition);
    });

    widget.data.condition = value;
    widget.data.onUpdate();
  }
}
