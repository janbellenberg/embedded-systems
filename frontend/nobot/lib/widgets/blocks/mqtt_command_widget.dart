import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';

import '../../model/blocks/mqtt_command.dart';
import '../../model/redux/app_state.dart';

/// Widget for the [MqttCommand] block.
///
/// This widget should be used inside of a list widget, because it is a [ListTile].
/// It contains a [DropdownButton] for the command, which should be sent to the device..
class MqttCommandWidget extends StatelessWidget {
  final MqttCommand data;

  MqttCommandWidget({
    required String key,
    required this.data,
  }) : super(key: ValueKey(key));

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, AppState>(
      converter: ((store) => store.state),
      builder: (context, state) {
        return ListTile(
          title: Container(
            decoration: BoxDecoration(
              border: Border(
                left: data.isRunning
                    ? const BorderSide(color: Colors.red, width: 3.0)
                    : BorderSide(color: Theme.of(context).highlightColor),
              ),
            ),
            padding: const EdgeInsets.only(left: 10.0),
            child: Row(
              children: [
                const Text("Sende"),
                Container(
                  margin: const EdgeInsets.only(left: 20.0),
                  child: DropdownButton(
                    items: const [
                      DropdownMenuItem(
                        value: "forward",
                        child: Text(
                          "Vorwärts fahren",
                        ),
                      ),
                      DropdownMenuItem(
                        value: "backward",
                        child: Text(
                          "Rückwärts fahren",
                        ),
                      ),
                      DropdownMenuItem(
                        value: "left",
                        child: Text(
                          "Links drehen",
                        ),
                      ),
                      DropdownMenuItem(
                        value: "right",
                        child: Text(
                          "Rechts drehen",
                        ),
                      ),
                      DropdownMenuItem(
                        value: "stop",
                        child: Text(
                          "Stop",
                        ),
                      ),
                      DropdownMenuItem(
                        value: "set-zero",
                        child: Text(
                          "Position zurücksetzen",
                        ),
                      ),
                    ],
                    value: data.command["command"],
                    onChanged: (value) {
                      data.command = {"command": value ?? "forward"};
                      data.onUpdate();
                    },
                  ),
                ),
                const Spacer(),
                Padding(
                  padding: const EdgeInsets.only(right: 20.0),
                  child: IconButton(
                    onPressed: () => data.onDelete(data),
                    icon: const Icon(Icons.delete),
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
