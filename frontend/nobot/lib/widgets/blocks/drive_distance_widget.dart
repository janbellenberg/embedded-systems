import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';

import '../../model/blocks/drive_distance.dart';
import '../../model/redux/app_state.dart';

/// Widget for the [DriveDistance] block.
///
/// This widget should be used inside of a list widget, because it is a [ListTile].
/// It contains a [DropdownButton] for the direction and a [TextField] for the distance in cm.
class DriveDistanceWidget extends StatefulWidget {
  late final TextEditingController controller = TextEditingController.fromValue(
    TextEditingValue(
      text: data.distance.toString(),
    ),
  );
  String get condition => controller.text;

  final DriveDistance data;

  DriveDistanceWidget({
    required String key,
    required this.data,
  }) : super(key: ValueKey(key));

  @override
  State<DriveDistanceWidget> createState() => _DriveDistanceWidgetState();
}

class _DriveDistanceWidgetState extends State<DriveDistanceWidget> {
  bool get _isValid => double.tryParse(widget.controller.text) != null;

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, AppState>(
      converter: ((store) => store.state),
      builder: (context, state) {
        return ListTile(
          title: Container(
            decoration: BoxDecoration(
              border: Border(
                left: widget.data.isRunning
                    ? const BorderSide(color: Colors.red, width: 3.0)
                    : BorderSide(color: Theme.of(context).highlightColor),
              ),
            ),
            padding: const EdgeInsets.only(left: 10.0),
            child: Row(
              children: [
                const Text(
                  "Fahre",
                ),
                Container(
                  margin: const EdgeInsets.symmetric(horizontal: 20.0),
                  child: DropdownButton(
                    items: const [
                      DropdownMenuItem(
                        value: "forward",
                        child: Text(
                          "Vorwärts",
                        ),
                      ),
                      DropdownMenuItem(
                        value: "backward",
                        child: Text(
                          "Rückwärts",
                        ),
                      ),
                    ],
                    value: widget.data.forward ? "forward" : "backward",
                    onChanged: (value) {
                      widget.data.forward = (value ?? "forward") == "forward";
                      widget.data.onUpdate();
                    },
                  ),
                ),
                const Text(
                  "für",
                ),
                Container(
                  width: 300.0,
                  margin: const EdgeInsets.symmetric(horizontal: 20.0),
                  child: TextField(
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(
                      errorText: _isValid ? null : "Nicht gültig",
                    ),
                    onChanged: (value) => setState(() {
                      widget.data.distance = int.tryParse(value) ?? 0;
                      widget.data.onUpdate();
                    }),
                    controller: widget.controller,
                  ),
                ),
                const Text(
                  "cm",
                ),
                const Spacer(),
                Padding(
                  padding: const EdgeInsets.only(right: 20.0),
                  child: IconButton(
                    onPressed: () => setState(
                      () => widget.data.onDelete(widget.data),
                    ),
                    icon: const Icon(Icons.delete),
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
