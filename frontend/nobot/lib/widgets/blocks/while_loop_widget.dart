import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';

import '../../dialogs/add_block_dialog.dart';
import '../../model/blocks/device_context.dart';
import '../../model/blocks/while_loop.dart';
import '../../model/redux/app_state.dart';
import '../text_button_widget.dart';

/// Widget for the [WhileLoop] block.
///
/// This widget should be used inside of a list widget, because it is a [ListTile].
/// A [TextField] for the condition and a [ReorderableListView] for the sub-widgets.
class WhileLoopWidget extends StatefulWidget {
  late final TextEditingController _conditionController;
  String get condition => _conditionController.text;

  final WhileLoop data;

  WhileLoopWidget({
    required String key,
    required this.data,
  }) : super(key: ValueKey(key)) {
    _conditionController = TextEditingController.fromValue(
      TextEditingValue(
        text: data.condition,
      ),
    );
  }

  @override
  State<WhileLoopWidget> createState() => _WhileLoopWidgetState();
}

class _WhileLoopWidgetState extends State<WhileLoopWidget> {
  bool _conditionValid = true;

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, AppState>(
      converter: ((store) => store.state),
      builder: (context, state) {
        return ListTile(
          title: Container(
            decoration: BoxDecoration(
              border: Border(
                left: widget.data.isRunning
                    ? const BorderSide(color: Colors.red, width: 3.0)
                    : BorderSide(color: Theme.of(context).highlightColor),
              ),
            ),
            padding: const EdgeInsets.only(left: 10.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    const Text(
                      "Solange",
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    Container(
                      width: 300.0,
                      margin: const EdgeInsets.only(left: 20.0),
                      child: TextField(
                        decoration: InputDecoration(
                          errorText: _conditionValid ? null : "Nicht gültig",
                        ),
                        onChanged: _conditionChanged,
                        controller: widget._conditionController,
                      ),
                    ),
                    const Spacer(),
                    Padding(
                      padding: const EdgeInsets.only(right: 20.0),
                      child: IconButton(
                        onPressed: () => setState(
                          () => widget.data.onDelete(widget.data),
                        ),
                        icon: const Icon(Icons.delete),
                      ),
                    ),
                  ],
                ),
                Container(
                  margin: const EdgeInsets.symmetric(
                    horizontal: 20.0,
                    vertical: 10.0,
                  ),
                  color: Theme.of(context).cardColor.withOpacity(0.3),
                  child: ReorderableListView(
                    physics: const ClampingScrollPhysics(),
                    shrinkWrap: true,
                    onReorder: (oldIdx, newIdx) => setState(
                      () => widget.data.blockList.changeItemPosition(
                        oldIdx,
                        newIdx,
                      ),
                    ),
                    children: widget.data.blockList.blocks
                        .map(
                          (e) => e.widget,
                        )
                        .toList(),
                  ),
                ),
                TextButtonWidget(
                  text: "Element zu Schleife hinzufügen",
                  fontSize: 15.0,
                  isPrimary: false,
                  onPressed: () => _addItem(context),
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  void _conditionChanged(String value) {
    setState(() {
      _conditionValid = DeviceContext.checkExpressionSyntax(widget.condition);
    });

    widget.data.condition = value;
    widget.data.onUpdate();
  }

  void _addItem(BuildContext context) {
    showDialog(
      context: context,
      builder: (context) {
        return AddBlockDialog(
          commandList: widget.data.blockList,
        );
      },
    );
  }
}
