import 'package:flutter/material.dart';

import '../model/db/device.dart';
import 'button_widget.dart';

/// Creates a square button, which shows the name and the mac address of a device.
class DeviceButton extends StatelessWidget {
  const DeviceButton({
    super.key,
    required this.device,
    required this.onPressed,
  });

  final Device device;
  final Function()? onPressed;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(
        vertical: 10.0,
        horizontal: 20.0,
      ),
      child: Button(
        onPressed: onPressed,
        width: 150.0,
        height: 150.0,
        isPrimary: false,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Padding(
              padding: const EdgeInsets.only(
                top: 10.0,
                bottom: 5.0,
              ),
              child: Text(
                device.name ?? "Unbekanntes Gerät",
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 25.0,
                  color: Theme.of(context).dividerColor,
                ),
              ),
            ),
            Text(
              device.mac ?? "",
              style: TextStyle(
                fontFamily: "Montserrat",
                fontSize: 12.0,
                color: Theme.of(context).dividerColor.withOpacity(
                      0.5,
                    ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
