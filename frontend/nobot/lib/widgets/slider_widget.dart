import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

/// Creates a unique-looking number slider, which reacts to pan gestures and mouse scrolling.
///
/// The [value] supports numbers from 0 to 1, which can be used as percentages.
/// If the current [value] changes, the [onValueChanged] event will be emitted.
class SliderWidget extends StatelessWidget {
  const SliderWidget({
    required this.value,
    required this.onValueChanged,
    super.key,
  });

  final void Function(double val) onValueChanged;
  final double value;

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        return Listener(
          // process mouse scroll signal and update value by 5%
          onPointerSignal: (signal) {
            if (signal is PointerScrollEvent) {
              double newValue = value;
              if (signal.scrollDelta.dy < 0) {
                newValue = value + 0.05;
              } else if (signal.scrollDelta.dy > 0) {
                newValue = value - 0.05;
              }

              if (newValue < 0) {
                newValue = 0;
              } else if (newValue > 1) {
                newValue = 1;
              }

              onValueChanged(newValue);
            }
          },
          // process pan gesture and update the value, so that the slider
          // matches the actual position of the pointer.
          child: GestureDetector(
            onPanUpdate: (DragUpdateDetails details) {
              double percentage =
                  details.localPosition.dx / constraints.maxWidth;

              if (percentage < 0) {
                percentage = 0;
              } else if (percentage > 1) {
                percentage = 1;
              }

              onValueChanged(percentage);
            },
            child: Padding(
              padding: const EdgeInsets.symmetric(
                vertical: 30.0,
              ),
              child: ClipRRect(
                clipBehavior: Clip.hardEdge,
                borderRadius: BorderRadius.circular(40.0),
                child: MouseRegion(
                  cursor: SystemMouseCursors.resizeColumn,
                  child: Container(
                    color: Theme.of(context).cardColor,
                    width: MediaQuery.of(context).size.width * 0.8,
                    child: Align(
                      alignment: Alignment.centerLeft,
                      child: Container(
                        color: Theme.of(context).colorScheme.primary,
                        width: constraints.maxWidth * value,
                        height: 40.0,
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
