import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';

import 'actions_widget.dart';
import '../constants.dart';
import '../model/redux/app_state.dart';

/// Creates a wrapper for all pages with a title bar and the three-dots icon.
///
/// The [child] is the actual content and is wrapped by a [RefreshIndicator].
/// The [RefreshIndicator] or the action menu can trigger the [onRefresh] event.
///
/// Besides updating the content, further [actions] can be added to the menu.
///
/// The [title] will be displayed in the title bar. If the user should [canGoBack],
/// a back arrow will be shown.

class ContentWrapper extends StatelessWidget {
  const ContentWrapper({
    required this.child,
    this.onRefresh,
    this.actions = const [],
    this.title = "NoBot",
    this.canGoBack = false,
    super.key,
  });

  final Widget child;
  final List<Widget> actions;
  final Future<void> Function()? onRefresh;
  final String title;
  final bool canGoBack;

  void _showActionMenu(BuildContext context) {
    showModalBottomSheet(
      isScrollControlled: true,
      context: context,
      constraints: const BoxConstraints(maxWidth: 400.0),
      builder: (context) => ActionMenu(
        children: [
          ...actions,
          if (isDesktop && actions.isNotEmpty && onRefresh != null)
            Divider(color: Theme.of(context).cardColor),
          if (isDesktop && onRefresh != null)
            ListTile(
              leading: Icon(
                Icons.refresh,
                color: Theme.of(context).colorScheme.primary,
              ),
              title: const Text("Aktualisieren"),
              onTap: onRefresh,
            ),
        ],
      ),
      barrierColor: Colors.transparent,
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(
          top: Radius.circular(20),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, AppState>(
        converter: (store) => store.state,
        builder: (context, state) {
          return Container(
            color: Theme.of(context).colorScheme.primary,
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.symmetric(
                    vertical: 15.0,
                    horizontal: 10.0,
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      if (canGoBack)
                        IconButton(
                          padding: const EdgeInsets.all(0),
                          icon: const Icon(
                            Icons.navigate_before,
                            size: 30.0,
                            color: Colors.white,
                          ),
                          onPressed: () => Navigator.pop(context),
                        ),
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.symmetric(
                            horizontal: 20.0,
                          ),
                          child: Text(
                            title,
                            overflow: TextOverflow.fade,
                            maxLines: 1,
                            softWrap: false,
                            style: const TextStyle(
                              fontSize: 30.0,
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ),
                      if (actions.isNotEmpty ||
                          (isDesktop && onRefresh != null))
                        IconButton(
                          padding: const EdgeInsets.all(0),
                          icon: const Icon(
                            Icons.more_vert,
                            size: 30.0,
                            color: Colors.white,
                          ),
                          onPressed: () => _showActionMenu(context),
                        ),
                    ],
                  ),
                ),
                Expanded(
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                      color: Theme.of(context).colorScheme.background,
                      borderRadius: const BorderRadius.only(
                        topLeft: Radius.circular(30.0),
                        topRight: Radius.circular(30.0),
                      ),
                    ),
                    child: onRefresh == null
                        ? child
                        : RefreshIndicator(
                            onRefresh: onRefresh!,
                            child: child,
                          ),
                  ),
                ),
              ],
            ),
          );
        });
  }
}
