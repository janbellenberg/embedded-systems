import 'package:flutter/foundation.dart';

import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

import '../services/shared_preferences/server.dart'
    if (dart.library.html) '../services/shared_preferences/server_web.dart';
import '../core/page_wrapper.dart';
import '../dialogs/markdown_dialog.dart';
import '../dialogs/os_selection_dialog.dart';
import '../dialogs/info_dialog.dart';
import '../pages/settings_page.dart';

/// Creates the bottom bar for the web-version of the login page.
///
/// It currently links to the info dialog and the custom server download.
/// In future use, legal information (Impressum, Datenschutz) can be added.
class WebBottomBar extends StatelessWidget {
  const WebBottomBar({
    Key? key,
  }) : super(key: key);

  void _openServerDownload(String filename, BuildContext context) {
    Navigator.pop(context);
    showDialog(
      context: context,
      builder: (context) {
        return MarkdownDialog(
          filename: filename,
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: usesDefaultServer(),
        builder: (context, snapshot) {
          return Positioned(
            left: 0,
            bottom: 10,
            child: SizedBox(
              width: MediaQuery.of(context).size.width,
              child: SingleChildScrollView(
                padding: const EdgeInsets.symmetric(
                  horizontal: 10.0,
                ),
                scrollDirection: Axis.horizontal,
                reverse: true,
                child: Row(
                  children: kIsWeb
                      ? <Widget>[
                          TextButton(
                            onPressed: () {
                              showDialog(
                                context: context,
                                builder: (context) => const InfoDialog(),
                                barrierColor: Colors.transparent,
                              );
                            },
                            child: const Text("Copyright"),
                          ),
                          if (snapshot.data == true)
                            TextButton(
                              onPressed: () {
                                showDialog(
                                  context: context,
                                  builder: (context) => OsSelectionDialog(
                                    onWindowsSelected: () {
                                      launchUrl(
                                        Uri.parse(
                                            "/download/nobot-installer.exe"),
                                      );
                                    },
                                    onAndroidSelected: () {
                                      launchUrl(
                                        Uri.parse("/download/bin/nobot.apk"),
                                      );
                                    },
                                    children: [
                                      ListTile(
                                        leading: const Icon(Icons.window),
                                        title: const Text("Windows (portabel)"),
                                        onTap: () {
                                          launchUrl(
                                            Uri.parse(
                                              "/download/bin/nobot-portable-windows.zip",
                                            ),
                                          );
                                        },
                                      ),
                                    ],
                                  ),
                                  barrierColor: Colors.transparent,
                                );
                              },
                              child: const Text("Anwendung herunterladen"),
                            ),
                          if (snapshot.data == true)
                            TextButton(
                              onPressed: () {
                                showDialog(
                                  context: context,
                                  builder: (context) => OsSelectionDialog(
                                    onWindowsSelected: () =>
                                        _openServerDownload(
                                            "self-hosting-windows.md", context),
                                    onLinuxSelected: () => _openServerDownload(
                                        "self-hosting-linux.md", context),
                                    onMacSelected: () => _openServerDownload(
                                        "self-hosting-macos.md", context),
                                  ),
                                  barrierColor: Colors.transparent,
                                );
                              },
                              child: const Text("Server herunterladen"),
                            ),
                        ]
                      : <Widget>[
                          IconButton(
                            onPressed: () {
                              PageWrapper.routeToPage(
                                const SettingsPage(),
                                context,
                                overrideLogin: true,
                              );
                            },
                            icon: Icon(
                              Icons.settings,
                              color: Theme.of(context).primaryColor,
                            ),
                          )
                        ],
                ),
              ),
            ),
          );
        });
  }
}
