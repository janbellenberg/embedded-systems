import 'dart:math';

import 'package:flutter/material.dart';

import '../model/db/device.dart';
import '../services/mqtt/base.dart';
import '../services/mqtt/command_builder.dart';
import 'button_widget.dart';

/// Creates a joystick-like widget for steering the robot.
///
/// The specific commands will automatically be sent to the [device].
class JoystickWidget extends StatelessWidget {
  const JoystickWidget({
    required this.device,
    Key? key,
  }) : super(key: key);

  final Device device;

  @override
  Widget build(BuildContext context) {
    if (device.mac == null) return Container();

    return Padding(
      padding: const EdgeInsets.only(top: 10.0),
      child: Column(
        children: [
          Button(
            width: 200.0,
            child: Transform.rotate(
              angle: 90 * pi / 180,
              child: const Icon(
                Icons.navigate_before,
                color: Colors.white,
                size: 40.0,
              ),
            ),
            onPressed: () {
              MQTT.sendCommand(
                device.mac!,
                device.accessKey,
                CommandBuilder.forward(),
              );
            },
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Button(
                height: 100.0,
                isPrimary: false,
                child: Transform.rotate(
                  angle: 0,
                  child: Icon(
                    Icons.navigate_before,
                    color: Theme.of(context).dividerColor,
                    size: 40.0,
                  ),
                ),
                onPressed: () {
                  MQTT.sendCommand(
                    device.mac!,
                    device.accessKey,
                    CommandBuilder.turnLeft(),
                  );
                },
              ),
              Padding(
                padding: const EdgeInsets.symmetric(
                  horizontal: 20.0,
                  vertical: 20.0,
                ),
                child: Button(
                  isPrimary: false,
                  borderColor: Colors.red,
                  onPressed: () {
                    MQTT.sendCommand(
                      device.mac!,
                      device.accessKey,
                      CommandBuilder.stop(),
                    );
                  },
                  child: const SizedBox(
                    width: 100,
                    height: 75,
                  ),
                ),
              ),
              Button(
                isPrimary: false,
                height: 100.0,
                child: Transform.rotate(
                  angle: 180 * pi / 180,
                  child: Icon(
                    Icons.navigate_before,
                    color: Theme.of(context).dividerColor,
                    size: 40.0,
                  ),
                ),
                onPressed: () {
                  MQTT.sendCommand(
                    device.mac!,
                    device.accessKey,
                    CommandBuilder.turnRight(),
                  );
                },
              ),
            ],
          ),
          Button(
            isPrimary: false,
            width: 200.0,
            child: Transform.rotate(
              angle: 270 * pi / 180,
              child: Icon(
                Icons.navigate_before,
                color: Theme.of(context).dividerColor,
                size: 40.0,
              ),
            ),
            onPressed: () {
              MQTT.sendCommand(
                device.mac!,
                device.accessKey,
                CommandBuilder.backward(),
              );
            },
          ),
        ],
      ),
    );
  }
}
