import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';

import '../controller/program.dart';
import '../dialogs/add_block_dialog.dart';
import '../dialogs/add_program_dialog.dart';
import '../model/db/device.dart';
import '../model/blocks/command_list.dart';
import '../model/db/program.dart';
import '../model/redux/app_state.dart';
import '../model/redux/store.dart';
import 'button_widget.dart';
import 'text_button_widget.dart';

/// Creates a widget for the block programming of a [device],
/// including a dropdown for selecting a program.
class ProgrammingWidget extends StatefulWidget {
  const ProgrammingWidget(this.device, {super.key});
  final Device device;

  @override
  State<ProgrammingWidget> createState() => _ProgrammingWidgetState();
}

class _ProgrammingWidgetState extends State<ProgrammingWidget> {
  int? _programID;
  bool _programRunning = false;

  @override
  void initState() {
    super.initState();
    loadProgramList().then((value) {
      if (_programID == null && store.state.user!.programs.isNotEmpty) {
        setState(() {
          _programID = store.state.user!.programs.first.id;
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, AppState>(
      converter: ((store) => store.state),
      builder: (context, state) {
        if (state.user == null) return Container();

        int programIndex = state.user!.programs.indexWhere(
          (element) => element.id == _programID,
        );

        if (programIndex < 0) {
          if (state.user!.programs.isNotEmpty) {
            _programID = store.state.user!.programs.first.id;
            programIndex = 0;
          } else {
            return Center(
              child: SizedBox(
                height: 100,
                width: 300,
                child: TextButtonWidget(
                  onPressed: _addProgram,
                  text: "Programm erstellen",
                  leadingIcon: const Icon(Icons.add),
                ),
              ),
            );
          }
        }

        Program program = state.user!.programs[programIndex];
        CommandList? blocks = program.parsed;
        bool hasErrors = blocks.hasErrors;

        return Stack(
          children: [
            Padding(
              padding: const EdgeInsets.all(15.0),
              child: Column(
                children: [
                  // Header with actions & selecting programs
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      TextButtonWidget(
                        width: 170.0,
                        onPressed:
                            hasErrors ? null : () => _runProgram(programIndex),
                        text: "Ausführen",
                        leadingIcon: const Icon(Icons.play_arrow),
                      ),
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.symmetric(
                            horizontal: 30.0,
                          ),
                          child: DropdownButton(
                            isExpanded: true,
                            items: state.user!.programs
                                .map((item) => DropdownMenuItem(
                                      value: item.id,
                                      child: Text(
                                        item.name ?? "Unbekanntes Programm",
                                      ),
                                    ))
                                .toList(),
                            value: _programID,
                            onChanged: (val) {
                              if (val == null) return;
                              loadProgram(val);
                              setState(
                                () => _programID = val,
                              );
                            },
                          ),
                        ),
                      ),
                      Row(
                        children: [
                          Tooltip(
                            message: program.isSynchronized
                                ? "Synchronisiert"
                                : "Upload läuft...",
                            child: Container(
                              height: 35,
                              width: 35,
                              margin: const EdgeInsets.only(right: 20.0),
                              child: program.isSynchronized
                                  ? Icon(
                                      Icons.check,
                                      color:
                                          Theme.of(context).colorScheme.primary,
                                      size: 35.0,
                                    )
                                  : CircularProgressIndicator(
                                      color:
                                          Theme.of(context).colorScheme.primary,
                                    ),
                            ),
                          ),
                          Button(
                            isPrimary: false,
                            onPressed: _addProgram,
                            child: Icon(
                              Icons.add,
                              color: Theme.of(context).colorScheme.primary,
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(
                              left: 10.0,
                            ),
                            child: Button(
                              isPrimary: false,
                              onPressed: () {
                                if (_programID == null) return;
                                deleteProgram(_programID!);
                              },
                              child: const Icon(
                                Icons.delete,
                                color: Colors.red,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                  // actual ui for blocks
                  if (_programID != null && programIndex >= 0)
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.symmetric(
                          vertical: 20.0,
                        ),
                        child: Column(
                          children: [
                            Expanded(
                              child: ReorderableListView(
                                onReorder: blocks.changeItemPosition,
                                children: blocks
                                    .blocks // map the block items to their widgets.
                                    .map(
                                      (e) => e.widget,
                                    )
                                    .toList(),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(top: 10.0),
                              child: TextButtonWidget(
                                text: "Element zu Programm hinzufügen",
                                fontSize: 15.0,
                                onPressed: () => _addItem(
                                  context,
                                  programIndex,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                ],
              ),
            ),
            // show an overlay, if the program is running.
            if (_programRunning)
              ClipRRect(
                child: Container(
                  color: Theme.of(context).colorScheme.background.withOpacity(
                        0.7,
                      ),
                  child: Center(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        SizedBox(
                          height: 150.0,
                          width: 150.0,
                          child: CircularProgressIndicator(
                            color: Theme.of(context).colorScheme.primary,
                            strokeWidth: 4.0,
                          ),
                        ),
                        const Text(
                          "Programm wird ausgeführt",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontSize: 30.0,
                          ),
                        ),
                        TextButtonWidget(
                          text: "Abbrechen",
                          onPressed: () {
                            CommandList.programShouldBeCanceled.value = true;
                          },
                          isPrimary: false,
                          borderColor: Colors.red,
                        ),
                      ],
                    ),
                  ),
                ),
              ),
          ],
        );
      },
    );
  }

  void _runProgram(int programIndex) {
    setState(() => _programRunning = true);
    store.state.user!.programs[programIndex].parsed.run(widget.device).then(
          (value) => setState(
            () => _programRunning = false,
          ),
        );
  }

  void _addItem(BuildContext context, int programIndex) {
    showDialog(
      context: context,
      builder: (context) {
        return AddBlockDialog(
          commandList: store.state.user!.programs[programIndex].parsed,
        );
      },
    );
  }

  void _addProgram() {
    showDialog(
      context: context,
      builder: (context) => AddProgramDialog(),
    );
  }
}
