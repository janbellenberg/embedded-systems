import 'package:flutter/material.dart';

/// Creates a button with a [child] widget.
///
/// If clicked, the [onPressed] callback will be emitted.
/// The dimensions of the button can be set via [height] and [width].
/// For further styling the border can be changed by [borderColor] and [borderWidth].
/// If the button [isPrimary], the color of the button changes to the primary color of the app theme.
class Button extends StatelessWidget {
  const Button({
    Key? key,
    required this.child,
    required this.onPressed,
    this.isPrimary = true,
    this.width,
    this.height,
    this.borderColor,
    this.borderWidth = 2.0,
  }) : super(key: key);

  final Widget child;
  final Function()? onPressed;
  final bool isPrimary;
  final double? width;
  final double? height;
  final Color? borderColor;
  final double borderWidth;

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: onPressed,
      style: ButtonStyle(
        padding: MaterialStateProperty.all<EdgeInsets>(
          EdgeInsets.zero,
        ),
        backgroundColor: MaterialStateProperty.resolveWith<Color>(
          (states) {
            if (states.contains(MaterialState.disabled)) {
              return Theme.of(context).colorScheme.background;
            }

            if (isPrimary) {
              return Theme.of(context).colorScheme.primary;
            }

            return Theme.of(context).cardColor;
          },
        ),
        shadowColor: MaterialStateProperty.resolveWith<Color>(
          ((states) => Colors.transparent),
        ),
        shape: MaterialStateProperty.all<RoundedRectangleBorder>(
          RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(30.0),
            side: borderColor == null
                ? BorderSide.none
                : BorderSide(
                    color: borderColor!,
                    width: borderWidth,
                  ),
          ),
        ),
        mouseCursor: MaterialStateProperty.resolveWith<MouseCursor>(
          (Set<MaterialState> states) {
            return states.contains(MaterialState.disabled)
                ? SystemMouseCursors.forbidden
                : SystemMouseCursors.click;
          },
        ),
      ),
      child: Container(
        width: width,
        height: height,
        padding: const EdgeInsets.symmetric(
          vertical: 12.0,
          horizontal: 7.0,
        ),
        child: child,
      ),
    );
  }
}
