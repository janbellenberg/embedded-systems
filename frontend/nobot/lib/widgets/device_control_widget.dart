import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';

import '../model/db/device.dart';
import '../model/redux/actions.dart' as redux;
import '../model/redux/app_state.dart';
import '../model/redux/store.dart';
import '../services/mqtt/base.dart';
import '../services/mqtt/command_builder.dart';
import 'joystick_widget.dart';
import 'name_value_widget.dart';
import 'slider_widget.dart';
import 'text_button_widget.dart';

/// Creates the remote-control widget for the [device] with all information and the joystick.
///
/// During the initialization, the widget subscribes to the device's position, distance and status.
class DeviceControlWidget extends StatefulWidget {
  const DeviceControlWidget(this.device, {super.key});

  final Device device;

  @override
  State<DeviceControlWidget> createState() => _DeviceControlWidgetState();
}

class _DeviceControlWidgetState extends State<DeviceControlWidget> {
  double _currentSpeed = 0.5;
  double _lastSendSpeed = 0.5;

  @override
  void initState() {
    super.initState();
    setupMQTT();
  }

  Future<void> setupMQTT() async {
    await MQTT.subscribe(
      "${widget.device.mac}/position",
      (data) => widget.device.position = data,
    );

    await MQTT.subscribe(
      "${widget.device.mac}/distance",
      (data) => widget.device.distance = data,
    );

    await MQTT.subscribe(
      "${widget.device.mac}/status",
      (data) => widget.device.status = data,
    );
  }

  @override
  void dispose() {
    super.dispose();

    MQTT.unsubscribe("${widget.device.mac}/position");
    MQTT.unsubscribe("${widget.device.mac}/distance");
    MQTT.unsubscribe("${widget.device.mac}/status");
  }

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, AppState>(
      converter: ((store) => store.state),
      builder: (context, state) {
        return Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: 20.0,
          ),
          child: Column(
            children: [
              NameValueWidget(
                title: "Distanz vorne",
                value: widget.device.context.value.distanceFront,
                unit: "cm",
              ),
              NameValueWidget(
                title: "Distanz hinten",
                value: widget.device.context.value.distanceBack,
                unit: "cm",
              ),
              NameValueWidget(
                margin: const EdgeInsets.only(top: 10.0),
                title: "Batterie",
                value: widget.device.batteryLevel ?? 0,
                whiteSpaceAfterValue: false,
                unit: "%",
              ),
              NameValueWidget(
                margin: const EdgeInsets.only(top: 10.0),
                title: "Position X",
                value: widget.device.context.value.positionX,
                unit: "cm",
              ),
              NameValueWidget(
                title: "Position Y",
                value: widget.device.context.value.positionY,
                unit: "cm",
              ),
              NameValueWidget(
                title: "Winkel",
                value: widget.device.context.value.angle,
                whiteSpaceAfterValue: false,
                unit: "°",
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 20.0),
                child: TextButtonWidget(
                  text: "Position zurücksetzen",
                  onPressed: () {
                    MQTT.sendCommand(
                      widget.device.mac ?? "",
                      widget.device.accessKey,
                      CommandBuilder.setToZero(),
                    );
                  },
                  isPrimary: false,
                  borderColor: Colors.red,
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(
                  horizontal: 10.0,
                  vertical: 15.0,
                ),
                child: TextField(
                  keyboardType: TextInputType.number,
                  decoration: const InputDecoration(
                    labelText: "Zugriffsschlüssel",
                  ),
                  onChanged: (value) {
                    widget.device.accessKey = value;

                    store.dispatch(redux.Action(
                      redux.ActionTypes.setDevice,
                      payload: widget.device,
                    ));
                  },
                ),
              ),
              JoystickWidget(
                device: widget.device,
              ),
              SliderWidget(
                value: _currentSpeed,
                onValueChanged: (val) {
                  setState(() => _currentSpeed = val);

                  if ((val - _lastSendSpeed).abs() > 0.05) {
                    _lastSendSpeed = val;
                    MQTT.sendCommand(
                      widget.device.mac ?? "",
                      widget.device.accessKey,
                      CommandBuilder.setSpeed((val * 100).toInt()),
                    );
                  }
                },
              )
            ],
          ),
        );
      },
    );
  }
}
