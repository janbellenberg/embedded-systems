import 'package:flutter/material.dart';

/// Creates a button with [text].
///
/// If clicked, the [onPressed] callback will be emitted.
/// If needed, a [leadingIcon] can be provided, which will be shown left to the text.
/// The dimensions of the button can be set via [height] and [width].
/// For further styling the border can be changed by [borderColor] and [borderWidth].
/// The [fontSize] of the text can also be changed.
/// If the button [isPrimary], the color of the button changes to the primary color of the app theme.
class TextButtonWidget extends StatelessWidget {
  const TextButtonWidget({
    Key? key,
    required this.text,
    required this.onPressed,
    this.leadingIcon,
    this.isPrimary = true,
    this.width,
    this.height,
    this.borderColor,
    this.borderWidth = 2.0,
    this.fontSize = 20.0,
  }) : super(key: key);

  final String text;
  final Icon? leadingIcon;
  final Function()? onPressed;
  final bool isPrimary;
  final double fontSize;
  final double? width;
  final double? height;
  final Color? borderColor;
  final double borderWidth;

  @override
  Widget build(BuildContext context) {
    Widget textWidget;

    if (height == null) {
      textWidget = Text(
        text,
        overflow: TextOverflow.fade,
        maxLines: 1,
        softWrap: false,
        textAlign: TextAlign.center,
        style: TextStyle(
          fontFamily: "Montserrat",
          fontSize: fontSize,
          color: isPrimary ? Colors.white : Theme.of(context).dividerColor,
        ),
      );
    } else {
      textWidget = Center(
        child: Text(
          text,
          overflow: TextOverflow.fade,
          softWrap: true,
          textAlign: TextAlign.center,
          style: TextStyle(
            fontFamily: "Montserrat",
            fontSize: fontSize,
            color: isPrimary ? Colors.white : Theme.of(context).dividerColor,
          ),
        ),
      );
    }

    return ElevatedButton(
      onPressed: onPressed,
      style: ButtonStyle(
        backgroundColor: MaterialStateProperty.resolveWith<Color>(
          (states) {
            if (states.contains(MaterialState.disabled)) {
              return Theme.of(context).cardColor;
            }

            if (isPrimary) {
              return Theme.of(context).colorScheme.primary;
            }

            return Theme.of(context).cardColor;
          },
        ),
        shadowColor: MaterialStateProperty.resolveWith<Color>(
          ((states) => Colors.transparent),
        ),
        shape: MaterialStateProperty.all<RoundedRectangleBorder>(
          RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(30.0),
            side: borderColor == null
                ? BorderSide.none
                : BorderSide(
                    color: borderColor!,
                    width: borderWidth,
                  ),
          ),
        ),
        mouseCursor: MaterialStateProperty.resolveWith<MouseCursor>(
          (Set<MaterialState> states) {
            return states.contains(MaterialState.disabled)
                ? SystemMouseCursors.forbidden
                : SystemMouseCursors.click;
          },
        ),
      ),
      child: Container(
        width: width,
        height: height,
        padding: const EdgeInsets.symmetric(
          vertical: 12.0,
          horizontal: 7.0,
        ),
        child: leadingIcon == null
            ? textWidget
            : Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  if (leadingIcon != null)
                    Padding(
                      padding: const EdgeInsets.only(right: 10.0),
                      child: leadingIcon!,
                    ),
                  textWidget,
                ],
              ),
      ),
    );
  }
}
