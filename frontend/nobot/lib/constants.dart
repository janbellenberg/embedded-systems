import 'dart:io';
import 'package:flutter/foundation.dart';

/// True if instance is running on a desktop os or in the web.
bool get isDesktop =>
    kIsWeb || Platform.isWindows || Platform.isLinux || Platform.isMacOS;

const bool useProdServer = true;
