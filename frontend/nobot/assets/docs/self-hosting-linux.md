# Linux

- Ubuntu 21.10/22.04 
-	Debian 11 oder höher
- Fedora 36/37 

## Schritt 1:
Für den NoBot-Server muss zunächst die Virtualisierungssoftware Docker heruntergeladen werden. Eine ausführliche Anleitung für die Installation  finden Sie [hier](https://docs.docker.com/desktop/install/linux-install/).

## Schritt2:
Nachdem Docker gestartet ist, kann der NoBot-Server heruntergeladen und eingerichtet werden. Laden Sie sich [diese Datei](https://nobot.janbellenberg.de/download/nobot.sh) herunter und führen sie aus.

Der Server wird nun heruntergeladen und eingerichtet. Über Docker können Sie den Server von nun an stoppen und starten.