# macOS

## Systemvoraussetzungen:
- macOS 11 oder höher

## Schritt 1:
Für den NoBot-Server muss zunächst die Virtualisierungssoftware Docker heruntergeladen werden. Eine ausführliche Anleitung für die Installation unter allen Betriebssystemen finden Sie [hier](https://docs.docker.com/get-docker/).

Zur Installation von Docker laden Sie sich den [Installer](https://desktop.docker.com/mac/main/amd64/Docker.dmg?utm_source=docker&utm_medium=webreferral&utm_campaign=docs-driven-download-mac-amd64) herunter und führen diesen aus.

## Schritt 2:
Nachdem Docker gestartet ist, kann der NoBot-Server heruntergeladen und eingerichtet werden. Laden Sie sich [diese Datei](https://nobot.janbellenberg.de/download/nobot.sh) herunter und führen sie aus.

Der Server wird nun heruntergeladen und eingerichtet. Über Docker können Sie den Server von nun an stoppen und starten.