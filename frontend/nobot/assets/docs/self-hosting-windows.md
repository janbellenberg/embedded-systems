# Windows

## Systemvoraussetzungen:
- Windows 11 Version 21H2 oder höher
- Windows 10 Version 21H1 oder höher

## Schritt 1:
Unter Windows muss vor der eigentlichen Installation von dem NoBot-Server zunächst das sogenannte „Windows Subsystem für Linux“ (WSL2) von Microsoft installiert werden. Dazu müssen Sie im Startmenü nach `cmd.exe` suchen und die Anwendung als Administrator ausführen. Mit dem Befehl
> **wsl --install**

wird die das WSL2 installiert. Vor dem nächsten Schritt muss das System **neugestartet** werden.

## Schritt 2:
Nun muss die Virtualisierungssoftware Docker heruntergeladen und installiert werden. Eine ausführliche Anleitung für die Installation unter allen Betriebssystemen finden Sie [hier](https://docs.docker.com/get-docker/).

Zur Installation von Docker laden Sie sich den [Installer](https://desktop.docker.com/win/main/amd64/Docker%20Desktop%20Installer.exe) herunter und führen diesen aus.

## Schritt 3:
Nachdem Docker gestartet ist, kann der NoBot-Server heruntergeladen und eingerichtet werden. Laden Sie sich [diese Datei](https://nobot.janbellenberg.de/download/nobot.cmd) herunter und führen sie aus.

Der Server wird nun heruntergeladen und eingerichtet. Über Docker können Sie den Server von nun an stoppen und starten.