#include <math.h>
#include "odometry.h"

// see http://www.informatik.uni-leipzig.de/~pantec/khepera/diffeq/

Odometry::Odometry(unsigned short pinLeft, unsigned short pinRight, double circumference, double width)
    : m_pinLeft(pinLeft),
      m_pinRight(pinRight),
      m_circumference(circumference),
      m_width(width)
{
  this->m_leftImpulses = 0;
  this->m_rightImpulses = 0;

  pinMode(pinLeft, INPUT_PULLUP);
  pinMode(pinRight, INPUT_PULLUP);
}

Odometry::~Odometry()
{
  detachInterrupt(this->m_pinLeft);
  detachInterrupt(this->m_pinRight);
}

void Odometry::leftInterrupt(const DrivingMode &mode)
{
  if(mode == FORWARD) {
    this->m_leftImpulses++;
  } else if(mode == BACKWARD) {
    this->m_leftImpulses--;
  }
}

void Odometry::rightInterrupt(const DrivingMode &mode)
{
  if(mode == FORWARD) {
    this->m_rightImpulses++;
  } else if(mode == BACKWARD) {
    this->m_rightImpulses--;
  }
}

unsigned short Odometry::pinLeft() const {
  return this->m_pinLeft;
}
unsigned short Odometry::pinRight() const {
  return this->m_pinRight;
}

void Odometry::setZero() {
  this->m_leftImpulses = 0;
  this->m_rightImpulses = 0;
}

double Odometry::getLeftDistance()
{
  return (this->m_leftImpulses * 1.0 / Odometry::interruptsPerRevolution) * this->m_circumference;
}

double Odometry::getRightDistance()
{
  return (this->m_rightImpulses * 1.0 / Odometry::interruptsPerRevolution) * this->m_circumference;
}

double Odometry::getAverageDistance()
{
  double l = this->getLeftDistance();
  double r = this->getRightDistance();
  return (l + r) / 2;
}

double Odometry::getXPos()
{
  double l = this->getLeftDistance();
  double r = this->getRightDistance();

  double alpha = this->getAngleAsRad();

  if (l == r)
  {
    return r * cos(alpha);
  }

  double ds = this->getAverageDistance();

  return (ds / alpha) * (cos((M_PI / 2) + (0.5 * M_PI) - alpha) + 1);
}

double Odometry::getYPos()
{
  double l = this->getLeftDistance();
  double r = this->getRightDistance();

  double alpha = this->getAngleAsRad();

  if (l == r)
  {
    return l * sin(alpha);
  }

  double ds = this->getAverageDistance();

  return (ds / alpha) * (sin((M_PI / 2) + (0.5 * M_PI) - alpha));
}

int Odometry::getAngle()
{
  return this->getAngleAsRad() * 180.0 / M_PI;
}

double Odometry::getAngleAsRad() {
  double l = this->getLeftDistance();
  double r = this->getRightDistance();

  return (r - l) / this->m_width;
}

long Odometry::leftImpulses() const{
  return this->m_leftImpulses;
}

long Odometry::rightImpulses() const{

  return this->m_rightImpulses;
}

