#ifndef _ODOMETRY_H_
#define _ODOMETRY_H_

#include <Arduino.h>
#include "Motor.h"

/**
 * With odometry, the position of the robot can be calculated,
 * based on the movement of the wheels.
 * 
 * Source: http://www.informatik.uni-leipzig.de/~pantec/khepera/diffeq/
 */
class Odometry
{
public:
  /**
   * Constructor
   * @param pinLeft esp32 pin of the left photo electric sensor.
   * @param pinRight esp32 pin of the right photo electric sensor.
   * @param circumference of the wheels.
   * @param width distance between the two wheels.
   */
  Odometry(unsigned short pinLeft, unsigned short pinRight, double circumference, double width);

  /**
   * Destructor
   *
   * Detaches all interrupts.
   */
  virtual ~Odometry();

  /**
   * Calculates the x-position of the vehicle.
   * @returns x position in cm.
   */
  double getXPos();

  /**
   * Calculates the y-position of the vehicle.
   * @returns y position in cm.
   */
  double getYPos();

  /**
   * Calculates the angle of the vehicle.
   *
   * If 0°, the device is orientated north.
   * @returns angle in degrees.
   */
  int getAngle();

  /**
   * Calculates the distance, both wheels have traveled.
   * @returns average distance in cm.
   */
  double getAverageDistance();

  /**
   * Handles an event for the left sensor.
   *
   * Should be called in an interrupt function.
   *
   * @param mode the direction, the left motor is rotating.
   */
  void leftInterrupt(const DrivingMode &mode);

  /**
   * Handles an event for the right sensor.
   *
   * Should be called in an interrupt function.
   *
   * @param mode the direction, the right motor is rotating.
   */
  void rightInterrupt(const DrivingMode &mode);

  /**
   * Getter
   * @returns the amount of impulses, the left sensor detected.
   */
  long leftImpulses() const;

  /**
   * Getter
   * @returns the amount of impulses, the right sensor detected.
   */
  long rightImpulses() const;

  /**
   * Getter
   * @returns the pin for the left sensor interrupt.
   */
  unsigned short pinLeft() const;

  /**
   * Getter
   * @returns the pin for the right sensor interrupt.
   */
  unsigned short pinRight() const;

  /**
   * Resets the impulses to zero, so that the location and angle is zero.
   */
  void setZero();

private:
  /**
   * Amount of interrupts, caused by each sensor for every revolution.
   */
  static const int interruptsPerRevolution = 20;

  /**
   * Converts the amount of interrupts
   * of the left sensor into a distance.
   *
   * @returns distance in cm.
   */
  double getLeftDistance();

  /**
   * Converts the amount of interrupts
   * of the right sensor into a distance.
   *
   * @returns distance in cm.
   */
  double getRightDistance();

  /**
   * Calculates the angle of the vehicle.
   *
   * @returns angle in radiants.
   */
  double getAngleAsRad();

  unsigned short m_pinLeft;
  unsigned short m_pinRight;

  volatile long m_leftImpulses;
  volatile long m_rightImpulses;

  double m_circumference;
  double m_width;
};

#endif