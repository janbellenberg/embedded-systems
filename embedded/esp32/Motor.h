#ifndef _MOTOR_H_
#define _MOTOR_H_

/**
 * Enum for the rotation direction of a motor.
*/
enum DrivingMode {
  FORWARD, BACKWARD, STOPPED
};

/**
 * Gear motor for moving the robot.
*/
class Motor {
public:
  /**
   * Constructor
   * @param pin1 esp32 pin, which is used for driving forward (must be PWM compatible).
   * @param pin2 esp32 pin, which is used for driving backward (must be PWM compatible).
  */
  Motor(const unsigned short &pin1, const unsigned short &pin2);

  /**
   * Sends a PWM signal to the motor driver, which lets the motor drive forward.
  */
  void forward();

  /**
   * Sends a PWM signal to the motor driver, which lets the motor drive backward.
  */
  void backward();

  /**
   * Stops all PWM signals.
  */
  void stop();

  /**
   * Sets the duty cycle of the PWM signal.
   * @param speed in percent (0-100)
  */
  void setSpeed(const unsigned short &speed);

  /**
   * Getter
   * @returns the current direction, in which the motor is driving.
  */
  const DrivingMode drivingMode() const;

private:
  DrivingMode mode;
  unsigned short m_pin1;
  unsigned short m_pin2;
  unsigned short m_speed;
};

#endif