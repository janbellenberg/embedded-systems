#include "LcdPrinter.h"

LcdPrinter::LcdPrinter(LiquidCrystal_I2C &attachedLcd) : attachedLcd(attachedLcd)
{
  attachedLcd.init();
  attachedLcd.clear();
  attachedLcd.backlight();
  // Creates new LCD led-values that are associated with a certain byte value.
  attachedLcd.createChar(batteryChar, battery);
  attachedLcd.createChar(keyChar, key);
}

void LcdPrinter::update()
{
  // Does nothing if both lines are not supposed to move.
  if (fMoving != 0 || sMoving != 0)
  {

    // Repainting all lines including the special chars.
    attachedLcd.setCursor(0, 0);
    attachedLcd.print(firstLine.substring(fStartIndex, fStartIndex + 16));
    attachedLcd.setCursor(0, 1);
    attachedLcd.print(secondLine.substring(sStartIndex, sStartIndex + 16));
    repaintSpecialChars();

    if (fIgnoreNextUpdates <= 0)
    {
      // Adjusting current index from wich to display 16 characters of firstLine.
      if (fMoving != 0)
      {
        (fMoving == 1) ? fStartIndex++ : fStartIndex--;

        if (fStartIndex <= 0)
        {
          fMoving = 1;
          fIgnoreNextUpdates = changeDelay;
        }
        if (fStartIndex + 16 >= firstLine.length())
        {
          fMoving = -1;
          fIgnoreNextUpdates = changeDelay;
        }
      }
    }
    else
    {
      fIgnoreNextUpdates--;
    }

    if (sIgnoreNextUpdates <= 0)
    {
      // Adjusting current index from wich to display 16 characters of secondLine.
      if (sMoving != 0)
      {
        (sMoving == 1) ? sStartIndex++ : sStartIndex--;

        if (sStartIndex <= 0)
        {
          sMoving = 1;
          sIgnoreNextUpdates = changeDelay;
        }
        if (sStartIndex + 16 >= secondLine.length())
        {
          sMoving = -1;
          sIgnoreNextUpdates = changeDelay;
        }
      }
    }
    else
    {
      sIgnoreNextUpdates--;
    }
  }
}

void LcdPrinter::repaintSpecialChars()
{
  // Only prints characters that should be visible taking into account sliding of the lines.
  for (int i = 0; i < specialChars.lastFreeIndex; i++)
  {
    int x = specialChars.slots[i].x;
    int y = specialChars.slots[i].y;
    if (y == 0)
    {
      int diff = x - fStartIndex; // Calculates the relative position of the character.
      if (diff >= 0 && diff < 16)
      {
        attachedLcd.setCursor(diff, 0);
        attachedLcd.write(specialChars.slots[i].b);
      }
    }
    else
    {
      int diff = x - sStartIndex; // Calculates the relative position of the character.
      if (diff >= 0 && diff < 16)
      {
        attachedLcd.setCursor(diff, 1);
        attachedLcd.write(specialChars.slots[i].b);
      }
    }
  }
}

void LcdPrinter::rememberWriteAt(int x, int y, uint8_t b)
{
  specialChars.addChar({x, y, b});
  attachedLcd.setCursor(x, y);
  attachedLcd.write(b);
}

void LcdPrinter::setFirstLine(String line)
{
  firstLine = line;
  fStartIndex = 0;
  attachedLcd.setCursor(0, 0);
  attachedLcd.print(firstLine.substring(fStartIndex, fStartIndex + 16));

  // Changes value if string exceeds 16 characters to allow initial right movement by the update() method.
  if (firstLine.length() > 16)
  {
    fMoving = 1;
  }
  else
  {
    fMoving = 0;
  }
}

void LcdPrinter::setSecondLine(String line)
{
  secondLine = line;
  sStartIndex = 0;
  attachedLcd.setCursor(0, 1);
  attachedLcd.print(secondLine.substring(sStartIndex, sStartIndex + 16));

  // Changes value if string exceeds 16 characters to allow initial right movement by the update() method.
  if (secondLine.length() > 16)
  {
    sMoving = 1;
  }
  else
  {
    sMoving = 0;
  }
}