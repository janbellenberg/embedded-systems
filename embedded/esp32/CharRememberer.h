#ifndef CHARREMEMBERER_H
#define CHARREMEMBERER_H

#include <Arduino.h>

/**
 * Struct, that holds a position and the byte value of a character.
 */
struct CharSlot {
  int x, y;
  uint8_t b;
};

/**
 * Class, that allocates memory for up to 10 CharSlots.
 */
class CharRememberer {
public:
  CharSlot *slots;
  int lastFreeIndex = 0;

  /**
   * Constructor allocating the array.
   */
  CharRememberer() { slots = new CharSlot[10]; }

  /**
   * Destructor
  */
  ~CharRememberer() {
    delete[] slots;
  }

  /**
   * Adds a CharSlot to the array at the index specified by the
   * member lastFreeIndex. Does nothing if 10 CharSlots have already been added.
   * @param slot CharSlot to add
   */
  void addChar(CharSlot slot);

  /**
   * Clears array of CharSlots.
   */
  void clear();
};

#endif