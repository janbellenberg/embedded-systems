#include <Arduino.h>
#include "Motor.h"

Motor::Motor(const unsigned short &pin1, const unsigned short &pin2) : m_pin1(pin1), m_pin2(pin2)
{
  ledcSetup(0, 5000, 8);
  this->m_speed = 127;
  this->mode = STOPPED;
}

void Motor::forward()
{
  ledcDetachPin(this->m_pin2);
  ledcAttachPin(this->m_pin1, 0);

  ledcWrite(0, this->m_speed);

  this->mode = FORWARD;
}

void Motor::backward()
{
  ledcDetachPin(this->m_pin1);
  ledcAttachPin(this->m_pin2, 0);

  ledcWrite(0, this->m_speed);

  this->mode = BACKWARD;
}

void Motor::stop()
{
  ledcWrite(0, 0);

  ledcDetachPin(this->m_pin2);
  ledcDetachPin(this->m_pin1);

  this->mode = STOPPED;
}

void Motor::setSpeed(const unsigned short &speed)
{
  this->m_speed = speed / 100.0 * 255.0;
  ledcWrite(0, this->m_speed);
}

const DrivingMode Motor::drivingMode() const
{
  return this->mode;
}