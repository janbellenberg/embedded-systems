#include "eeprom.h"

const unsigned int Settings::EEPROM_SIZE = 512;

Settings::Settings() {
#ifdef DEBUGGING
    // default values for debugging
    m_ssid = "Jan";
    m_key = "jHgi0geWHwvsg8ks";
    m_jwt = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJFODozMTpDRDpBRDo2MToyMCJ9.GWaI8_4qXXp73wg8EU7y_5P5uKMO10uq6N-UPhjXDvs";
    m_server = "192.168.137.1";
#else
    this->m_ssid = "";
    this->m_key = "";
    this->m_jwt = "";
    this->m_server = "";  
#endif
}

String Settings::SSID() const {
  return this->m_ssid;
}

void Settings::setSSID(const String &ssid) {
  this->m_ssid = ssid;
}

String Settings::KEY() const {
  return this->m_key;
}

void Settings::setKEY(const String &key) {
  this->m_key = key;
}

String Settings::JWT() const {
  return this->m_jwt;
}

void Settings::setJWT(const String &jwt) {
  this->m_jwt = jwt;
}

String Settings::SERVER() const {
  return this->m_server;
}

void Settings::setSERVER(const String &server) {
  this->m_server = server;
}

void Settings::write() const {
  int addr = 0;

  byte ssidLen = this->m_ssid.length();
  byte keyLen = this->m_key.length();
  byte jwtLen = this->m_jwt.length();
  byte serverLen = this->m_server.length();

  EEPROM.write(addr, ssidLen);
  addr += sizeof(byte);
  EEPROM.write(addr, keyLen);
  addr += sizeof(byte);
  EEPROM.write(addr, jwtLen);
  addr += sizeof(byte);
  EEPROM.write(addr, serverLen);
  addr += sizeof(byte);

  for (int i = 0; i < ssidLen; i++)
  {
    EEPROM.write(addr + i, this->m_ssid[i]);
  }

  addr += ssidLen;

  for (int i = 0; i < keyLen; i++)
  {
    EEPROM.write(addr + i, this->m_key[i]);
  }

  addr += keyLen;

  for (int i = 0; i < jwtLen; i++)
  {
    EEPROM.write(addr + i, this->m_jwt[i]);
  }

  addr += jwtLen;

  for (int i = 0; i < serverLen; i++)
  {
    EEPROM.write(addr + i, this->m_server[i]);
  }

  addr += serverLen;

  EEPROM.commit();
}