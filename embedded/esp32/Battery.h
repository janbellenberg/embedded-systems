#ifndef _BATTERY_H_
#define _BATTERY_H_

/**
 * Battery management
 */
class Battery
{
public:
  /**
   * Constructor
   * @param pin Number of the ESP32 pin, which should be used as ADC input.
   */
  Battery(const unsigned short &pin);

  /**
   * Calculates the percentage from the battery voltage, measured by the ADC.
   * @returns a value between 0 (for 6 volts) and 100 (9 volts).
   */
  int getPercentage() const;

  void measure();

private:
  unsigned short m_pin;
  unsigned short m_value;
};

#endif