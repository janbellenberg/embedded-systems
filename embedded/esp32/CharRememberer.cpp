#include "CharRememberer.h"

void CharRememberer::addChar(CharSlot slot) {
  if(lastFreeIndex < 10) {
    slots[lastFreeIndex] = slot;
  }
  lastFreeIndex++;
}

void CharRememberer::clear() {
  delete[] slots;
  slots = new CharSlot[10];
  lastFreeIndex = 0;
}