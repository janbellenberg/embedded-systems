#include "USSensor.h"
#include <Arduino.h>

USSensor::USSensor(const unsigned short &trigger, const unsigned short &echo) : m_trigger(trigger), m_echo(echo) {
  pinMode(trigger, OUTPUT);
  pinMode(echo, INPUT);

  for(int i = 0; i < AVG_VALUES; i++) {
    this->distances[i] = 0;
  }
}

double USSensor::getDistance() {
  // trigger a measurements
  digitalWrite(this->m_trigger, LOW);
  delayMicroseconds(2);
  digitalWrite(this->m_trigger, HIGH);
  delayMicroseconds(10);
  digitalWrite(this->m_trigger, LOW);
  
  int duration = pulseIn(this->m_echo, HIGH);
  
  // convert the time, a signal needs, into the distance.
  double distance = duration * 0.034 / 2;

  // shifting values in array for average calculation
  for(int i = AVG_VALUES - 1; i > 0; i-- ) {
    this->distances[i] = this->distances[i - 1];
  }

  this->distances[0] = distance;

  // calculate average  
  double avg = 0;
  for(int i = 0; i < AVG_VALUES; i++ ) {
    if(this->distances[i] > 0) {
      avg += this->distances[i];
    }
  }

  return avg / AVG_VALUES;
}