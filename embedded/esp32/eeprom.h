#if !defined(_EEPROM_H_) && !defined(DEBUGGING)
#define _EEPROM_H_

#include <Arduino.h>
#include <EEPROM.h>

/**
 * Management of all settings, which should be stored in the EEPROM
 * - SSID and password of the WiFi
 * - JWT for authenticating against the server
 * - IP address or hostname of the server (empty, if default server)
*/
class Settings {
  public:    
    /**
     * Writes the settings to the EEPROM
    */
    void write() const;

    /**
     * Initializes the EEPROM.
     * This method must be called before every other action.
    */
    static void init() {
      EEPROM.begin(Settings::EEPROM_SIZE);
    }

    /**
     * Reads the settings from the server.
     * @returns a new instance of the settings.
    */
    static Settings *read() {
      int addr = 0;
      Settings *settings = new Settings();
      byte ssidLen, keyLen, jwtLen, serverLen;

      // The first 4 bytes contain the sizes of the strings.

      ssidLen = EEPROM.read(addr);
      addr += sizeof(byte);

      keyLen = EEPROM.read(addr);
      addr += sizeof(byte);

      jwtLen = EEPROM.read(addr);
      addr += sizeof(byte);

      serverLen = EEPROM.read(addr);
      addr += sizeof(byte);

      Serial.println(ssidLen);
      Serial.println(keyLen);
      Serial.println(jwtLen);
      Serial.println(serverLen);

      for(int i = 0; i < ssidLen; i++) {
        settings->m_ssid += (char) EEPROM.read(addr + i);
      }
      addr += ssidLen;
      
      for(int i = 0; i < keyLen; i++) {
        settings->m_key += (char) EEPROM.read(addr + i);
      }
      addr += keyLen;

      for(int i = 0; i < jwtLen; i++) {
        settings->m_jwt += (char) EEPROM.read(addr + i);
      }
      addr += jwtLen;

      for(int i = 0; i < serverLen; i++) {
        settings->m_server += (char) EEPROM.read(addr + i);
      }
      addr += serverLen;

      return settings;
    }

    /**
     * Erases all settings from the EEPROM.
    */
    static void clear() {
      Serial.println("clearing eeprom");
      for(int i = 0; i < Settings::EEPROM_SIZE; i++) {
        EEPROM.write(i, 0);
      }

      EEPROM.commit();
    }
    
    /**
     * Getter
     * @returns name (ssid) of the WiFi, which should be used.
    */
    String SSID() const;

    /**
     * Setter
     * @param SSID name (ssid) of the WiFi, which should be used.
    */
    void setSSID(const String &SSID);

    /**
     * Getter
     * @returns password of the WiFi, which should be used.
    */
    String KEY() const;

    /**
     * Setter
     * @param KEY password for the WiFi, which should be used.
    */
    void setKEY(const String &KEY);

    /**
     * Getter
     * @returns JSON Web Token, which should be used for authentication.
    */
    String JWT() const;

    /**
     * Setter
     * @param JWT JSON Web Token, which should be used for authentication.
    */
    void setJWT(const String &JWT);

    /**
     * Getter
     * @returns address of the server, empty for default server.
    */
    String SERVER() const;

    /**
     * Setter
     * @param SERVER address of the server, empty for default server.
    */
    void setSERVER(const String &SERVER);

  private:
    static const unsigned int EEPROM_SIZE;

    Settings();

    String m_ssid;
    String m_key;
    String m_jwt;
    String m_server;
};

#endif