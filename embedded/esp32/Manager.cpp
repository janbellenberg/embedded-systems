#include "Manager.h"
#include <WiFi.h>

void Manager::printStatus()
{
  switch (currentMode)
  {
  case ESP_setup:
    printer->clearSpecialChars();
    printer->setFirstLine("NoBot      Setup");
    printer->setSecondLine(WiFi.macAddress());
    break;
  case ESP_client:
    printer->clearSpecialChars();
    printer->setFirstLine("NoBot     Client");
    printer->setSecondLine("  " + String(this->accessCode) + "   " + formatBatteryPerc(this->battery));
    printer->rememberWriteAt(0, 1, printer->keyChar);
    printer->rememberWriteAt(15, 1, printer->batteryChar);
    break;
  default:
    printer->setFirstLine("NoBot: error");
  }
}

String Manager::formatBatteryPerc(int perc) {
  if(perc <= 9)
    return String("  " + String(perc) + "%");
  if(perc >= 10 && perc <= 99)
    return String(" " + String(perc) + "%");
  return String(perc) + "%";
}