#ifndef MANAGER_H
#define MANAGER_H

#include <Arduino.h>
#include <LiquidCrystal_I2C.h>
#include "LcdPrinter.h"

/**
 * Enum representing current operation mode of ESP-32.
 */
enum Mode
{
  ESP_setup,
  ESP_client,
};

/**
 * Class that acts as the interface between LCD and controller.
 * This class also provides the LCD object for the LcdPrinter instance.
 * Helps to display status notifications.
 */
class Manager
{
private:
  LiquidCrystal_I2C *lcd;
  LcdPrinter *printer;

  Mode currentMode = ESP_client;
  int accessCode = 0;
  int battery = 0;

public:
  /**
   * Constructor, wich initialises the display features.
   */
  Manager()
  {
    lcd = new LiquidCrystal_I2C(0x27, 16, 2);
    printer = new LcdPrinter((*lcd));
  }

  /**
   * Destructor
   */
  ~Manager()
  {
    delete lcd;
    delete printer;
  }

  /**
   * Sets a new mode through the passed enum.
   * A mode depended status is then printed on the display.
   * @param mode New operating mode
   */
  void setMode(Mode mode)
  {
    this->currentMode = mode;
    this->printStatus();
  }

  /**
   * Sets an accesscode for a connecting client and updates the display accordingly.
   * However, this accesscode is only displayed during client mode.
   * @param code Accesscode for client
   */
  void setAccessCode(int code)
  {
    this->accessCode = code;
    this->printStatus();
  }

  /**
   * Sets and updates the displayed battery percentage.
   * Similar to the accesscode this is only visible in client mode.
   * @param battery Batterypercentage (0-100) %
   */
  void setBatteryLevel(int battery)
  {
    this->battery = battery;
    this->printStatus();
  }

  /**
   * Prints a status notification on the display.
   * This is dependent on the member currebtMode.
   */
  void printStatus();

  /**
   * Formats the percentage to be easily displayable in a short string.
   * @param perc Batterypercentage
   * @return Returns string of length 4 representing the percentage
   */
  String formatBatteryPerc(int perc);

  /**
   * Invokes the LcdPrinter.update() method to enable the slide feature of the Lcdprinter class.
   */
  void updateLcd() { printer->update(); }
};

#endif