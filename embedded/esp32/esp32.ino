/******************************************************
 *                                                    *
 *    NoBot ESP32 code                                *
 *    Jan Bellenberg, Joel Lutz, Justin Bröker        *
 *    Eingebettete Systeme WiSe 22/23                 *
 *                                                    *
*******************************************************/

// this directive can be defined,
// to reduce build time and binary size
// of the program during debugging.
// #define DEBUGGING

#include <WiFi.h>
#include <PubSubClient.h>
#include <ArduinoJson.h>

#ifndef DEBUGGING
#include <WebServer.h>
#include <EEPROM.h>

#include "eeprom.h"
#include "graphql.h"
#include "http.h"
#endif

#include "Battery.h"
#include "Motor.h"
#include "USSensor.h"
#include "Odometry.h"
#include "Manager.h"

// define values for odometry
#define CIRCUMFERENCE 21
#define WIDTH 19.5
#define INTERRUPT_WAIT_TIME 30

int accessCode = 0;
int _getRandom(int min, int max);

bool useDefaultServer = true;

void callback(char* topicBuffer, byte* messageBuffer, unsigned int length);
void reconnect();
void updateMQTT(void * parameter);
void updateDisplay(void *parameters);
void processCommand(String command);

// ODOMETRY
volatile unsigned long leftOldTime = 0;
volatile unsigned long rightOldTime = 0;
void IRAM_ATTR leftInterrupt();
void IRAM_ATTR rightInterrupt();

// WiFi & MQTT
WiFiClient espClient;
PubSubClient client(espClient);

// US SENSOR
USSensor frontSensor(26, 33);
USSensor backSensor(27, 25);

Battery battery(32);

// MOTORS
Motor leftMotor(16, 17);
Motor rightMotor(18, 19);

Odometry odometry(34, 35, CIRCUMFERENCE, WIDTH);

Settings *settings;

#ifndef DEBUGGING
WebServer server(80);
#endif

// DISPLAY
Manager *manager = NULL;
Mode mode = ESP_client;

// DRIVING TASKS
TaskHandle_t commandRunner = NULL;
typedef struct {
  int value;
  String confirmID;
  TaskHandle_t *handle;
} CommandParameter;

CommandParameter parameters;


// SETUP FUNCTION
void setup() {
  delay(500); // wait for all components to be initialized

  // start serial communication
  Serial.begin(9600);
  Serial.setDebugOutput(true);
  Serial.println("NoBot Robot");
  Serial.println(WiFi.macAddress());

  battery.measure();

  // read settings
#ifndef DEBUGGING
  Settings::init();
  settings = Settings::read();
  
  Serial.print("SSID: ");
  Serial.println(settings->SSID());  
  Serial.print("KEY: ");
  Serial.println(settings->KEY());  
  Serial.print("JWT: ");
  Serial.println(settings->JWT()); 
  Serial.print("Server: ");
  Serial.println(settings->SERVER());  

  useDefaultServer = settings->SERVER().length() == 0;
#endif

  // check, whether the client or setup mode is enabled by the switch.
  pinMode(2, INPUT);

  mode = digitalRead(2) == HIGH ? ESP_client : ESP_setup;

  // Initialize display manager
  manager = new Manager();
  manager->setMode(mode);

  xTaskCreate(
    updateDisplay,
    "Update Display",
    5000,
    NULL,
    tskIDLE_PRIORITY,
    NULL
  );

  if(mode == ESP_client) {
    // initialize interrupts for odometry
    attachInterrupt(odometry.pinLeft(), leftInterrupt, RISING);
    attachInterrupt(odometry.pinRight(), rightInterrupt, RISING);

    // generate an access code for commands
#ifdef DEBUGGING
    accessCode = 12345;//_getRandom(10000, 99999);
#else
    accessCode = _getRandom(10000, 99999);
#endif
    manager->setAccessCode(accessCode);

    // connect to WiFi
    WiFi.begin(settings->SSID().c_str(), settings->KEY().c_str());
    
    while(WiFi.status() != WL_CONNECTED) {
      delay(500); 
      Serial.print(".");
    }
    
    Serial.println("");
    Serial.println("WiFi connected");
    Serial.print("IP-Address of ESP module: ");
    Serial.println(WiFi.localIP());

    // Check, if the stored JWT is valid,
    // otherwise try to register as a new device
#ifndef DEBUGGING
    String query = VALIDATE_JWT;
    query.replace("$token", settings->JWT());
    DynamicJsonDocument res = sendQuery(query, settings->SERVER());
    if(res.containsKey("data") && res["data"].containsKey("validateJWT")) {
      if(!res["data"]["validateJWT"].as<bool>()) {
        query = REGISTER_DEVICE;
        query.replace("$mac", WiFi.macAddress());
        res = sendQuery(query, settings->SERVER());
        if(res.containsKey("data") && res["data"].containsKey("activateDevice")) {
          Serial.println("Got new JWT");
          settings->setJWT(res["data"]["activateDevice"].as<String>());
          settings->write();
        } else {
          // request failed or no user wants to set up this device.
          return;
        }
      } else {
        // JWT is valid
        Serial.println("Valid JWT");
      }
    } else {
      // request failed
      return;
    }
#endif

    // Initialize the MQTT client
    client.setServer(useDefaultServer ? PROD_SERVER : settings->SERVER().c_str(), 1883);
    client.setCallback(callback);

    // run parallel task for sending data to the MQTT server
    xTaskCreate(
      updateMQTT,
      "MQTT send data",
      10000,
      NULL,
      tskIDLE_PRIORITY,
      NULL
    );
  } else {
    // Initialize webserver for setup mode
#ifndef DEBUGGING
    // Settings::clear();
    WiFi.mode(WIFI_AP);
    WiFi.softAP("nobot-setup", NULL);   // SSID: "nobot-setup", no password
    Serial.println(WiFi.softAPIP());

    server.on("/", getIndexPage);
    server.on("/style.css", getStyle);
    server.on("/code.js", getJsCode);
    server.on("/get-wifis", listWifis);
    server.on("/save-wifi", saveWifi);
    server.on("/save-server", saveServer);
    server.begin();
#endif
  }
}

void loop() {
  if(mode == ESP_client) {
    // handle MQTT connections
    if (!client.connected()) {
        reconnect();
      }
      client.loop();
  } else {
#ifndef DEBUGGING
    // handle http connections on setup mode
    server.handleClient();
#endif
  }
}

/**
 * Callback for received MQTT packages
 * @param topicBuffer the topic, in which the data was published
 * @param messageBuffer the raw message package
 * @param length of the message buffer
*/
void callback(char* topicBuffer, byte* messageBuffer, unsigned int length) { 
  String topic(topicBuffer);
  String message;
  
  // convert message to buffer
  for (int i = 0; i < length; i++) {
    message += (char)messageBuffer[i];
  }

  Serial.println(message);

  // handle topic [mac]/[accessCode]/command
  if(topic == WiFi.macAddress() + String("/") + String(accessCode) + "/command") {
    processCommand(message);
  }

}

/**
 * Handle a command, which was received via MQTT
 * @param raw JSON-string, representing the command
*/
void processCommand(String raw) {
  // parse JSON
  DynamicJsonDocument commandJSON(1024);
  deserializeJson(commandJSON, raw);

  if(!commandJSON.containsKey("randomID")) return;
  
  // send ACK signal to the server
  String randomID = commandJSON["randomID"].as<String>();
  String ackTopic = WiFi.macAddress() + String("/") + randomID + "/confirm";
  client.publish(ackTopic.c_str(), "ACK");

  String command = commandJSON["command"].as<String>();

  if(command == "set-zero") {
    // reset the odometry
    odometry.setZero();
  } else if(command == "set-speed" && commandJSON.containsKey("speed")) {
    // update the speed of the motors
    leftMotor.setSpeed(commandJSON["speed"].as<int>());
    rightMotor.setSpeed(commandJSON["speed"].as<int>());
  } else if(command == "stop") {
    // stop a driving task, if currently running
    if(commandRunner != NULL) {
      vTaskSuspend(commandRunner);
      vTaskDelete(commandRunner);
      commandRunner = NULL;
    } 

    // stop the motors
    leftMotor.stop();
    rightMotor.stop();
    client.publish(ackTopic.c_str(), "FINISHED");
  } else if(commandRunner == NULL){
      // only run driving commands, 
      // if no other command is currently being executed.
      parameters.confirmID = randomID;
      parameters.handle = &commandRunner;

     if(command == "forward") {
      if(commandJSON.containsKey("distance")) {
        // drive forward for a certain distance
        // in a separate task
        parameters.value = commandJSON["distance"].as<int>();

        xTaskCreate(
          driveDistanceForward,
          "drive",
          5000,
          (void*)&parameters,
          tskIDLE_PRIORITY,
          &commandRunner);
      } else {
        // just drive forward
        leftMotor.forward();
        rightMotor.forward();
        client.publish(ackTopic.c_str(), "FINISHED");
      }
    } else if(command == "backward") {
      if(commandJSON.containsKey("distance")) {
        // drive backward for a certain distance
        // in a separate task
        parameters.value = commandJSON["distance"].as<int>();
        xTaskCreate(
          driveDistanceBackward,
          "drive",
          5000,
          (void*)&parameters,
          tskIDLE_PRIORITY,
          &commandRunner);
      } else {
        // just drive backward
        leftMotor.backward();
        rightMotor.backward();
        client.publish(ackTopic.c_str(), "FINISHED");
      }
    } else if(command == "left") {
      if(commandJSON.containsKey("angle")) {
        // turn left for a certain angle
        // in a separate task
        parameters.value = commandJSON["angle"].as<int>();
        xTaskCreate(
          rotateAngleLeft,
          "drive",
          5000,
          (void*)&parameters,
          tskIDLE_PRIORITY,
          &commandRunner);
      } else {
        // just turn left
        leftMotor.backward();
        rightMotor.forward();
        client.publish(ackTopic.c_str(), "FINISHED");
      }
    } else if(command == "right") {
      if(commandJSON.containsKey("angle")) {
        // turn right for a certain angle
        // in a separate task
        parameters.value = commandJSON["angle"].as<int>();
        xTaskCreate(
          rotateAngleLeft,
          "drive",
          5000,
          (void*)&parameters,
          tskIDLE_PRIORITY,
          &commandRunner);
      } else {
        // just turn right
        leftMotor.forward();
        rightMotor.backward();
        client.publish(ackTopic.c_str(), "FINISHED");
      }
    } 
  } 
}

/**
 * (Re)connect to the MQTT broker
*/
void reconnect() {
  // build the client ID
  String clientID = "device#" + WiFi.macAddress() + "#" + esp_random();

  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    
    if (client.connect(clientID.c_str(), "", settings->JWT().c_str())) {
      Serial.println("mqtt connected");
      client.subscribe((WiFi.macAddress() + String("/") + String(accessCode) + "/command").c_str());
      client.publish((WiFi.macAddress() + "/connected").c_str(), "connected");
    } else {
      Serial.print("failed ");
      Serial.println(client.state());
      delay(1000);
    }
  }
}

/**
 * Sending updates about position, ultrasonic distance and battery state to the server.
 * 
 * This method must be invoked in another task.
 * 
 * @param parameters Task parameters, always NULL
*/
void updateMQTT(void *parameter) {
  // run repeatedly every 100ms
  while(true) {
    delay(100);

    // skip, if reconnection is running.
    if(!client.connected()) {
      continue;
    }

    // build the topics
    String positionTopic = WiFi.macAddress() + "/position";
    String distanceTopic = WiFi.macAddress() + "/distance";
    String statusTopic = WiFi.macAddress() + "/status";

    /* BUILD STATUS STRING */
    DynamicJsonDocument statusJSON(1024);

    statusJSON["battery"] = battery.getPercentage();
    String status;
    serializeJson(statusJSON, status);
    client.publish(statusTopic.c_str(), status.c_str());


    /* BUILD POSITION STRING */
    String data = String(odometry.getXPos()) + ":" + String(odometry.getYPos()) + ":" + String(odometry.getAngle());
    client.publish(positionTopic.c_str(), data.c_str());

    /* BUILD DISTANCE STRING */
    String data2 = String(frontSensor.getDistance()) + ":" + String(backSensor.getDistance());
    client.publish(distanceTopic.c_str(), data2.c_str());

  }
}


void updateDisplay(void *parameters) {
  while(true) {
    battery.measure();
    if(mode == ESP_client) {
      int batteryLevel = battery.getPercentage();
      manager->setBatteryLevel(batteryLevel);
    }
    manager->updateLcd();
    delay(1000);
  }
}

/**
 * Driving forward for a certain distance.
 * 
 * Must be invoked in a separate task.
 * @param parameters must be a pointer to CommandParameter
*/
void driveDistanceForward(void *parameters) {
  CommandParameter params = *((CommandParameter *) parameters);
  double startPosition = odometry.getAverageDistance();

  leftMotor.forward();
  rightMotor.forward();

  while(odometry.getAverageDistance() - startPosition < params.value);

  leftMotor.stop();
  rightMotor.stop();

  String ackTopic = WiFi.macAddress() + String("/") + params.confirmID + "/confirm";
  client.publish(ackTopic.c_str(), "FINISHED");  
  
  *(params.handle) = NULL;
  vTaskDelete(NULL);

}

/**
 * Driving backward for a certain distance.
 * 
 * Must be invoked in a separate task.
 * @param parameters must be a pointer to CommandParameter
*/
void driveDistanceBackward(void *parameters) {
  CommandParameter params = *((CommandParameter *) parameters);
  double startPosition = odometry.getAverageDistance();

  leftMotor.backward();
  rightMotor.backward();

  while(odometry.getAverageDistance() - startPosition > -params.value);

  leftMotor.stop();
  rightMotor.stop();

  String ackTopic = WiFi.macAddress() + String("/") + params.confirmID + "/confirm";
  client.publish(ackTopic.c_str(), "FINISHED");  
  
  *(params.handle) = NULL;
  vTaskDelete(NULL);
}

/**
 * Turning left for a certain angle.
 * 
 * Must be invoked in a separate task.
 * @param parameters must be a pointer to CommandParameter
*/
void rotateAngleLeft(void *parameters) {
  CommandParameter params = *((CommandParameter *) parameters);
  double startAngle = odometry.getAngle();

  leftMotor.backward();
  rightMotor.forward();

  while(odometry.getAngle() - startAngle < params.value);

  leftMotor.stop();
  rightMotor.stop();

  String ackTopic = WiFi.macAddress() + String("/") + params.confirmID + "/confirm";
  client.publish(ackTopic.c_str(), "FINISHED");  
  
  *(params.handle) = NULL;
  vTaskDelete(NULL);
}

/**
 * Turning right for a certain angle.
 * 
 * Must be invoked in a separate task.
 * @param parameters must be a pointer to CommandParameter
*/
void rotateAngleRight(void *parameters) {
  CommandParameter params = *((CommandParameter *) parameters);
  double startAngle = odometry.getAngle();

  leftMotor.forward();
  rightMotor.backward();

  while(odometry.getAngle() - startAngle > -params.value);

  leftMotor.stop();
  rightMotor.stop();

  String ackTopic = WiFi.macAddress() + String("/") + params.confirmID + "/confirm";
  client.publish(ackTopic.c_str(), "FINISHED");

  *(params.handle) = NULL;
  vTaskDelete(NULL);
}

/**
 * Interrupt function for the left photo electric sensor.
*/
void IRAM_ATTR leftInterrupt() {
  // de-bounce the signal, because sometimes two short signals
  // arrive, instead of one.
  if((millis() - leftOldTime) > INTERRUPT_WAIT_TIME) {
    leftOldTime = millis();
    odometry.leftInterrupt(leftMotor.drivingMode());
  }
}

/**
 * Interrupt function for the right photo electric sensor.
*/
void IRAM_ATTR rightInterrupt() {
  // de-bounce the signal, because sometimes two short signals
  // arrive, instead of one.
  if((millis() - rightOldTime) > INTERRUPT_WAIT_TIME) {
    rightOldTime = millis();
    odometry.rightInterrupt(rightMotor.drivingMode());
  }
}

/**
 * Helper function for generating a random number.
 * 
 * @param min the lower boundary for the resulting number.
 * @param max the upper boundary for the resulting number.
 * @returns a random number between min and max.
*/
int _getRandom(int min, int max) {
  return esp_random() % (max - min) + min;
}


/* SETUP METHODS */

#ifndef DEBUGGING

/**
 * Sends the index.html to the browser.
*/
void getIndexPage() {
  server.send(200, "text/html", HOME);
}

/**
 * Sends the style.css to the browser.
*/
void getStyle() {
  server.send(200, "text/css", STYLES);
}

/**
 * Sends the code.js to the browser.
*/
void getJsCode() {
  server.send(200, "application/json", CODE);
}

/**
 * Searches for all WiFi's in the area and sends them as a JSON string to the browser.
*/
void listWifis()
{
  WiFiClient client;
  int n = WiFi.scanNetworks();

  String jsonResult = "[";

  for (int i = 0; i < n; i++)
  {
    // skip own config wifi
    if (WiFi.SSID(i) == "nobot-setup")
      continue;

    // filter doubles
    if(jsonResult.indexOf(WiFi.SSID(i)) > 0)
      continue;

    jsonResult += "{";

    // build ssid
    jsonResult += "\"ssid\":";
    jsonResult += "\"" + WiFi.SSID(i) + "\",";

    // check if wifi is encrypted
    jsonResult += "\"strength\":";
    jsonResult += "\"";
    jsonResult += WiFi.RSSI(i);
    jsonResult += "\"";

    jsonResult += "},";
  }

  // remove last comma
  jsonResult.remove(jsonResult.length() - 1);

  jsonResult += "]";
  
  server.send(200, "application/json", jsonResult.c_str());
}

/**
 * Receive the new server to use and store it in the EEPROM.
*/
void saveServer() {
  if (!server.hasArg("plain"))
  {
    server.send(400, "text/plain", "");
    return;
  }

  DynamicJsonDocument doc(1024);
  DeserializationError error = deserializeJson(doc, server.arg("plain"));

  if (error)
  {
    server.send(400, "text/plain", "");
    return;
  }

  Settings *config = Settings::read();
  config->setSERVER(doc["server"].as<String>());
  config->write();
  delete config;
  
  config = Settings::read();
  Serial.print("Save SERVER: ");
  Serial.println(config->SERVER());

  server.send(200, "text/plain", "");
  delete config;
}

/**
 * Receive the new WiFi settings and store them into the EEPROM.
*/
void saveWifi()
{
  if (!server.hasArg("plain"))
  {
    server.send(400, "text/plain", "");
    return;
  }

  DynamicJsonDocument doc(1024);
  DeserializationError error = deserializeJson(doc, server.arg("plain"));

  if (error)
  {
    server.send(400, "text/plain", "");
    return;
  }
  
  Settings *config = Settings::read();
  config->setSSID(doc["ssid"].as<String>());
  config->setKEY(doc["key"].as<String>());
  config->write();
  delete config;

  config = Settings::read();
  Serial.print("Save SSID: ");
  Serial.println(config->SSID());
  Serial.print("Save KEY: ");
  Serial.println(config->KEY());

  server.send(200, "text/plain", "");
  delete config;
}

#endif