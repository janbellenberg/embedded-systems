#ifndef LCDPRINTER_H
#define LCDPRINTER_H

#include <Arduino.h>
#include <LiquidCrystal_I2C.h>
#include "CharRememberer.h"

/**
 * Class to easily write text on a 16x2 LCD.
 * Both lines of text may exceed the limit of 16 characters per line.
 * Calling the update() method will swipe the visible lineparts left and right between intervals
 * to make the entire line visible in the limited rowsize of 16.
 */
class LcdPrinter {
private:
  //Custom LCD values for a battery chaped character.
  uint8_t battery[8] = {B01110, B11111, B10001, B11111, B11111, B11111, B11111, B11111};
  //Custom LCD values for a key chaped character.
  uint8_t key[8] = {B01100, B01110, B01100, B01110, B01100, B11111, B11011, B11111};

  LiquidCrystal_I2C& attachedLcd;
  CharRememberer specialChars;
  String firstLine = "";
  String secondLine = "";
  uint8_t fStartIndex = 0;
  uint8_t sStartIndex = 0;
  //fMoving determines if sliding of the first line is happening and its direction (-1 = left, 0 = no movement, 1 = right).
  uint8_t fMoving = 0;
  //sMoving determines if sliding of the second line is happening and its direction (-1 = left, 0 = no movement, 1 = right).
  uint8_t sMoving = 0;

public:
  uint8_t batteryChar = 0;
  uint8_t keyChar = 1;

  int updateDelay = 500;
  uint8_t changeDelay = 1; //A small delay when switching direction
  uint8_t fIgnoreNextUpdates = changeDelay;
  uint8_t sIgnoreNextUpdates = changeDelay;

private:
  /**
   * Repaints all characters that were defined using the rememberWriteAt() method using
   * their their desired position an byte value saved in the CharRemeberer instance.
   * Is applied after both parts of firstLine and secondLine are painted on the LCD.
   */
  void repaintSpecialChars();

public:
  /**
   * Constructor that also initialises the referenced instance according to the needs of this class.
   * @param attachedLcd LCD on wich the LcdPrinter should operate on
   */
  LcdPrinter(LiquidCrystal_I2C& attachedLcd);

  /**
   * This method should be repeatedly called by a loop method. The speed of calls
   * directly influences the slide speed. This method activly renders a 16 character part of each line
   * onto the LCD. If a line of the LCD does not exceed 16 chars, then the line will not move.
   */
  void update();

  /**
   * Getter for the entire first line that is painted on the LCD.
   * @return String value of first line
   */
  String getFirstLine() { return firstLine; }

  /**
   * Getter for the entire second line that is painted on the LCD.
   * @return String value of second line
   */
  String getSecondLine() { return secondLine; }

  /**
   * Method wich takes in a byte value of a char at the desired position on the display.
   * The method creates a CharSlot instance based on the parameters to save for example special
   * characters independently from the first and second linestring.
   * @param x X-Position in line
   * @param y Y-Postion that determines if char is associated with the first or second line
   * @param b Byte value of char
   */
  void rememberWriteAt(int x, int y, uint8_t b);

  /**
   * Directly set the text displayed in the first row of the LCD.
   * If string holds more than 16 characters then the update() method will cause
   * back and forth movement.
   * @param line New line to paint in first row
   */
  void setFirstLine(String line);

 /**
   * Directly set the text displayed in the second row of the LCD.
   * If string holds more than 16 characters then the update() method will cause
   * back and forth movement.
   * @param line New line to paint in second row
   */
  void setSecondLine(String line);

  /**
   * Clears all characters that were previously written with rememberWriteAt().
   */
  void clearSpecialChars() { specialChars.clear(); }
};

#endif