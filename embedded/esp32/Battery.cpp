#include "Battery.h"
#include <Arduino.h>

Battery::Battery(const unsigned short &pin) : m_pin(pin)
{
}

int Battery::getPercentage() const
{
  // 6V -> 1.62V -> 0%
  // 9V -> 2.1V -> 100%
  double voltage = this->m_value / 4096.0 * 3.3;
  int percentage = 238.1 * voltage - 376.19;

  if (percentage < 0)
  {
    percentage = 0;
  }
  else if (percentage > 100)
  {
    percentage = 100;
  }

  return percentage;
}

void Battery::measure() {
  this->m_value = analogRead(this->m_pin);
}