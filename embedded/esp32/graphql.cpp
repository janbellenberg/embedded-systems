#include "graphql.h"
#include <HTTPClient.h>

DynamicJsonDocument sendQuery(String query, String hostname)
{
  query.replace("\n", "\\n");
  query.replace("\"", "\\\"");

  query = "{\"operationName\":null,\"variables\":{},\"query\":\"" + query + "\"}";
  DynamicJsonDocument result(1024);

  // use default server + https, if no server was specified.
  if (hostname.length() < 1)
  {
    String server = String("https://") + PROD_SERVER;
    WiFiClientSecure client;
    client.setCACert(rootCA);

    HTTPClient http;
    http.begin(client, (server + "/api/graphql").c_str());
    http.addHeader("Content-Type", "application/json");
    int httpCode = http.POST(query);

    if (httpCode == 200)
    {
      deserializeJson(result, http.getString());
    }
    else
    {
      Serial.print("Code ");
      Serial.println(httpCode);
      Serial.println(http.getString());
    }

    http.end();
  }
  else
  {
    // use given server without https
    String server = String("http://") + hostname;
    WiFiClient client;
    client.connect(server.c_str(), 80);

    HTTPClient http;
    http.begin(client, (server + "/api/graphql").c_str());
    http.addHeader("Content-Type", "application/json");
    int httpCode = http.POST(query);

    if (httpCode == 200)
    {
      deserializeJson(result, http.getString());
    }
    else
    {
      Serial.println(http.getString());
    }

    http.end();
  }

  return result;
}