#ifndef _US_SENSOR_H_
#define _US_SENSOR_H_

// Number of past measurements, 
// which should be included in the average calculation.
#define AVG_VALUES 3

/**
 * Ultrasonic Sensor for distance measurement.
*/
class USSensor {
public:

  /**
   * Constructor
   * @param trigger esp32 pin for triggering a measurement.
   * @param echo esp32 pin, where the response of the sensor comes back.
  */
  USSensor(const unsigned short &trigger, const unsigned short &echo);

  /**
   * Measures the distance from the sensor to the next object.
   * @returns distance in cm.
  */
  double getDistance();

private:
  unsigned short m_trigger;
  unsigned short m_echo;

  /**
   * Array for the past values for the average calculation.
  */
  double distances[AVG_VALUES];

};

#endif