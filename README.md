# Eingebettete Systeme | NoBot education system

> Projekt für das Modul "Eingebettete Systeme" im WiSe 22/23.

## Zielsetzung
Das Projekt soll Schüler dazu animieren, sich mit Eingebetteten Systemen auseinanderzusetzen. Dies gelingt mit einem Roboter, den man selber zusammenbauen und mit Code-Blöcken programmieren kann.

## Tech Stack
| Bereich     | Plattform            | Sprachen       |
| ----------- | -------------------- | -------------- |
| Frontend    | Android, Web (Nginx) | Flutter & Dart |
| API         | GraphQL & MQTT       |
| Backend     | Node.js              | Typescript     |
| DB          | MariaDB & Redis      | SQL            |
| µController | ESP32                | C++            |

### Frontend
Das Frontend wird als App in Flutter umgesetzt und sowohl als Android App, als auch als Web-App angeboten. Die Web-App wird mit Nginx bereitgestellt.

### Backend & API
Das Backend wird mit Typescript in Node.js umgesetzt und stellt hauptsächlich einen MQTT Broker und einen GraphQL-Server zur Verfügung. Für weitere Anfragen (z.B. Login & Logout) wird REST verwendet.

### Datenbank
Die Daten werden in MySQL / MariaDB gespeichert. Als Cache und zur Verwaltung der Sessions wird Redis verwendet.

### Mikrocontroller
Für die Netzwerkkommunikation wird ein ESP32 verwendet, der auch über eine Kamera verfügt. Für die Hardware-Steuerung (Motoren & Sensoren) wird ein Arduino verwendet, jedoch kann dieser später durch einen alleinstehenden Mikrocontroller ersetzt werden.

### Deployment
Die serverseitige Anwendung wird mit Docker bereitgestellt. 

## Developer
- Jan Bellenberg [@janbellenberg](http://gitlab.janbellenberg.de/janbellenberg)
- Joel Lutz [@xxdevLPxx](http://gitlab.janbellenberg.de/xxdevLPxx)
- Justin Bröker [@Jstn](http://gitlab.janbellenberg.de/Jstn)
