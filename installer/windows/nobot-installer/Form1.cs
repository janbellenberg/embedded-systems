using IWshRuntimeLibrary;
using Microsoft.Win32;
using System;
using System.ComponentModel;
using System.IO;
using System.IO.Compression;
using System.Net;
using System.Threading;
using System.Windows.Forms;
using System.Diagnostics;

namespace nobot_installer
{
  public partial class Form1 : Form
  {
    private bool isRunning = false;

    public Form1()
    {
      InitializeComponent();

      this.destinationTextBox.Text = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles);
      this.updateUI();
    }

    private void updateUI()
    {
      this.destinationTextBox.Enabled = !isRunning;
      this.searchDestinationButton.Enabled = !isRunning;
      this.startInstallButton.Enabled = !isRunning && Directory.Exists(this.destinationTextBox.Text);
      this.createShortcutsCheckBox.Enabled = !isRunning;
      this.startMenuCheckBox.Enabled = !isRunning;
    }

    private void searchDestinationButton_Click(object sender, EventArgs e)
    {
      this.folderBrowserDialog.ShowDialog();

      if (this.folderBrowserDialog.SelectedPath.Trim().Length > 0)
      {
        this.destinationTextBox.Text = this.folderBrowserDialog.SelectedPath;
      }

      this.updateUI();
    }

    private void backgroundWorker_DoWork(object sender, DoWorkEventArgs e)
    {
      try
      {
        // remove all files, if already installed
        string installPath = Path.Combine(this.destinationTextBox.Text, "nobot");
        if (Directory.Exists(installPath))
        {
          Directory.Delete(installPath, true);
        }

        // create folder
        Directory.CreateDirectory(installPath);
        this.backgroundWorker.ReportProgress(100 / 6);

        // download from server
        string zipPath = Path.Combine(installPath, "nobot.zip");
        using (WebClient client = new WebClient())
        {
          client.DownloadFileCompleted += zipDownload_Completed;
          client.DownloadProgressChanged += zipDownload_ProgressChanged;
          var syncObject = new Object();
          lock (syncObject)
          {
            client.DownloadFileAsync(
               new Uri("https://nobot.janbellenberg.de/download/bin/windows.zip"),
               zipPath,
               syncObject
           );
            Monitor.Wait(syncObject);
          }
        }
        this.backgroundWorker.ReportProgress(2 * 100 / 6);

        // extract zip
        ZipFile.ExtractToDirectory(zipPath, installPath);
        System.IO.File.Delete(zipPath);
        this.backgroundWorker.ReportProgress(3 * 100 / 6);

        // create app data folder & store install path
        string appDataPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "NoBot");
        Directory.CreateDirectory(appDataPath);

        FileStream fs = new FileStream(Path.Combine(appDataPath, "install.path"), FileMode.Create);
        StreamWriter sw = new StreamWriter(fs);
        sw.Write(installPath);
        sw.Close();
        fs.Close();
        this.backgroundWorker.ReportProgress(4 * 100 / 6);


        WshShell shell = new WshShell();
        // create desktop shortcuts
        if (this.createShortcutsCheckBox.Checked)
        {
          object shDesktop = (object)"Desktop";
          string shortcutAddress = (string)shell.SpecialFolders.Item(ref shDesktop) + @"\NoBot.lnk";
          IWshShortcut desktopShortcut = (IWshShortcut)shell.CreateShortcut(shortcutAddress);
          desktopShortcut.Description = "NoBot Client";
          desktopShortcut.TargetPath = Path.Combine(installPath, "nobot.exe");
          desktopShortcut.Save();
        }


        // create start menu item
        if (this.startMenuCheckBox.Checked)
        {
          IWshShortcut allAppsShortcut = (IWshShortcut)shell.CreateShortcut(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "Microsoft", "Windows", "Start Menu", "Programs", "NoBot.lnk"));
          allAppsShortcut.TargetPath = Path.Combine(installPath, "nobot.exe");
          allAppsShortcut.Description = "NoBot Client";
          allAppsShortcut.Save();
          this.backgroundWorker.ReportProgress(5 * 100 / 6);
        }

        // register app in regestry
        RegistryKey reg = Registry.LocalMachine.CreateSubKey(@"SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\NoBot");
        reg.SetValue("DisplayIcon", Path.Combine(installPath, "nobot.exe"));
        reg.SetValue("DisplayName", "NoBot");
        reg.SetValue("Publisher", "NoBot");
        reg.SetValue("URLInfoAbout", "https://nobot.janbellenberg.de");
        reg.SetValue("NoModify", "1", RegistryValueKind.DWord);
        reg.SetValue("NoRepair", "1", RegistryValueKind.DWord);
        reg.SetValue("DisplayVersion", "1.0");
        reg.SetValue("UninstallString", Path.Combine(installPath, "uninstaller.exe"));
        this.backgroundWorker.ReportProgress(6 * 100 / 6);


        MessageBox.Show("Installation abgeschlossen", "NoBot", MessageBoxButtons.OK, MessageBoxIcon.Information);
      }
      catch (Exception ex)
      {
        MessageBox.Show("Es ist ein Fehler aufgetreten", "NoBot", MessageBoxButtons.OK, MessageBoxIcon.Error);
        this.backgroundWorker.ReportProgress(0);
        Console.WriteLine(ex.Message);
      }
      finally
      {
        this.Invoke(new Action(() => {
          this.isRunning = false;
          this.updateUI();
        }));
      }
    }

    private void zipDownload_Completed(object sender, AsyncCompletedEventArgs e)
    {
      lock (e.UserState)
      {
        Monitor.Pulse(e.UserState);
      }
    }

    private void zipDownload_ProgressChanged(object sender, DownloadProgressChangedEventArgs e)
    {
      double percentage = 1 / 6.0 * e.ProgressPercentage + 100.0 / 6;
      this.backgroundWorker.ReportProgress((int)percentage);
    }

    private void backgroundWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
    {
      this.progressBar.Value = e.ProgressPercentage;
    }

    private void startInstallButton_Click(object sender, EventArgs e)
    {
      this.isRunning = true;
      this.backgroundWorker.RunWorkerAsync();
      this.updateUI();
    }

    private void destinationTextBox_TextChanged(object sender, EventArgs e)
    {
      this.updateUI();
    }

    private void homepageLinkLabel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
    {
      Process.Start("https://nobot.janbellenberg.de");
    }
  }
}
