namespace nobot_installer
{
	partial class Form1
	{
		/// <summary>
		/// Erforderliche Designervariable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Verwendete Ressourcen bereinigen.
		/// </summary>
		/// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Vom Windows Form-Designer generierter Code

		/// <summary>
		/// Erforderliche Methode für die Designerunterstützung.
		/// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
			this.destinationLabel = new System.Windows.Forms.Label();
			this.destinationTextBox = new System.Windows.Forms.TextBox();
			this.searchDestinationButton = new System.Windows.Forms.Button();
			this.createShortcutsCheckBox = new System.Windows.Forms.CheckBox();
			this.panel1 = new System.Windows.Forms.Panel();
			this.copyrightLabel = new System.Windows.Forms.Label();
			this.homepageLinkLabel = new System.Windows.Forms.LinkLabel();
			this.startInstallButton = new System.Windows.Forms.Button();
			this.progressBar = new System.Windows.Forms.ProgressBar();
			this.titleLabel = new System.Windows.Forms.Label();
			this.folderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
			this.backgroundWorker = new System.ComponentModel.BackgroundWorker();
			this.startMenuCheckBox = new System.Windows.Forms.CheckBox();
			this.panel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// destinationLabel
			// 
			this.destinationLabel.AutoSize = true;
			this.destinationLabel.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.destinationLabel.Location = new System.Drawing.Point(12, 82);
			this.destinationLabel.Name = "destinationLabel";
			this.destinationLabel.Size = new System.Drawing.Size(97, 20);
			this.destinationLabel.TabIndex = 0;
			this.destinationLabel.Text = "Speicherort:";
			// 
			// destinationTextBox
			// 
			this.destinationTextBox.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.destinationTextBox.Location = new System.Drawing.Point(115, 79);
			this.destinationTextBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.destinationTextBox.Name = "destinationTextBox";
			this.destinationTextBox.Size = new System.Drawing.Size(372, 26);
			this.destinationTextBox.TabIndex = 1;
			this.destinationTextBox.TextChanged += new System.EventHandler(this.destinationTextBox_TextChanged);
			// 
			// searchDestinationButton
			// 
			this.searchDestinationButton.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.searchDestinationButton.Location = new System.Drawing.Point(493, 73);
			this.searchDestinationButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.searchDestinationButton.Name = "searchDestinationButton";
			this.searchDestinationButton.Size = new System.Drawing.Size(130, 39);
			this.searchDestinationButton.TabIndex = 2;
			this.searchDestinationButton.Text = "Durchsuchen";
			this.searchDestinationButton.UseVisualStyleBackColor = true;
			this.searchDestinationButton.Click += new System.EventHandler(this.searchDestinationButton_Click);
			// 
			// createShortcutsCheckBox
			// 
			this.createShortcutsCheckBox.AutoSize = true;
			this.createShortcutsCheckBox.Checked = true;
			this.createShortcutsCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
			this.createShortcutsCheckBox.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.createShortcutsCheckBox.Location = new System.Drawing.Point(16, 113);
			this.createShortcutsCheckBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.createShortcutsCheckBox.Name = "createShortcutsCheckBox";
			this.createShortcutsCheckBox.Size = new System.Drawing.Size(247, 24);
			this.createShortcutsCheckBox.TabIndex = 3;
			this.createShortcutsCheckBox.Text = "Desktopverknüpfung erstellen";
			this.createShortcutsCheckBox.UseVisualStyleBackColor = true;
			// 
			// panel1
			// 
			this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(113)))), ((int)(((byte)(87)))));
			this.panel1.Controls.Add(this.copyrightLabel);
			this.panel1.Controls.Add(this.homepageLinkLabel);
			this.panel1.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.panel1.ForeColor = System.Drawing.Color.White;
			this.panel1.Location = new System.Drawing.Point(0, 283);
			this.panel1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(635, 33);
			this.panel1.TabIndex = 4;
			// 
			// copyrightLabel
			// 
			this.copyrightLabel.AutoSize = true;
			this.copyrightLabel.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.copyrightLabel.Location = new System.Drawing.Point(384, 6);
			this.copyrightLabel.Name = "copyrightLabel";
			this.copyrightLabel.Size = new System.Drawing.Size(245, 20);
			this.copyrightLabel.TabIndex = 1;
			this.copyrightLabel.Text = "Copyright © NoBot 2023";
			// 
			// homepageLinkLabel
			// 
			this.homepageLinkLabel.ActiveLinkColor = System.Drawing.Color.White;
			this.homepageLinkLabel.AutoSize = true;
			this.homepageLinkLabel.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.homepageLinkLabel.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline;
			this.homepageLinkLabel.LinkColor = System.Drawing.Color.White;
			this.homepageLinkLabel.Location = new System.Drawing.Point(7, 6);
			this.homepageLinkLabel.Name = "homepageLinkLabel";
			this.homepageLinkLabel.Size = new System.Drawing.Size(92, 20);
			this.homepageLinkLabel.TabIndex = 0;
			this.homepageLinkLabel.TabStop = true;
			this.homepageLinkLabel.Text = "Homepage";
			this.homepageLinkLabel.VisitedLinkColor = System.Drawing.Color.White;
			this.homepageLinkLabel.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.homepageLinkLabel_LinkClicked);
			// 
			// startInstallButton
			// 
			this.startInstallButton.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.startInstallButton.Location = new System.Drawing.Point(238, 188);
			this.startInstallButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.startInstallButton.Name = "startInstallButton";
			this.startInstallButton.Size = new System.Drawing.Size(154, 39);
			this.startInstallButton.TabIndex = 5;
			this.startInstallButton.Text = "Installation starten";
			this.startInstallButton.UseVisualStyleBackColor = true;
			this.startInstallButton.Click += new System.EventHandler(this.startInstallButton_Click);
			// 
			// progressBar
			// 
			this.progressBar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.progressBar.Location = new System.Drawing.Point(10, 250);
			this.progressBar.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.progressBar.Name = "progressBar";
			this.progressBar.Size = new System.Drawing.Size(612, 26);
			this.progressBar.TabIndex = 6;
			// 
			// titleLabel
			// 
			this.titleLabel.AutoSize = true;
			this.titleLabel.Font = new System.Drawing.Font("Century Gothic", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.titleLabel.Location = new System.Drawing.Point(12, 10);
			this.titleLabel.Name = "titleLabel";
			this.titleLabel.Size = new System.Drawing.Size(323, 33);
			this.titleLabel.TabIndex = 7;
			this.titleLabel.Text = "Willkommen bei NoBot!";
			// 
			// folderBrowserDialog
			// 
			this.folderBrowserDialog.RootFolder = System.Environment.SpecialFolder.ProgramFiles;
			// 
			// backgroundWorker
			// 
			this.backgroundWorker.WorkerReportsProgress = true;
			this.backgroundWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker_DoWork);
			this.backgroundWorker.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.backgroundWorker_ProgressChanged);
			// 
			// startMenuCheckBox
			// 
			this.startMenuCheckBox.AutoSize = true;
			this.startMenuCheckBox.Checked = true;
			this.startMenuCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
			this.startMenuCheckBox.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.startMenuCheckBox.Location = new System.Drawing.Point(16, 145);
			this.startMenuCheckBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.startMenuCheckBox.Name = "startMenuCheckBox";
			this.startMenuCheckBox.Size = new System.Drawing.Size(236, 24);
			this.startMenuCheckBox.TabIndex = 8;
			this.startMenuCheckBox.Text = "Startmenueintrag hinzufügen";
			this.startMenuCheckBox.UseVisualStyleBackColor = true;
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.White;
			this.ClientSize = new System.Drawing.Size(634, 315);
			this.Controls.Add(this.startMenuCheckBox);
			this.Controls.Add(this.titleLabel);
			this.Controls.Add(this.progressBar);
			this.Controls.Add(this.startInstallButton);
			this.Controls.Add(this.panel1);
			this.Controls.Add(this.createShortcutsCheckBox);
			this.Controls.Add(this.searchDestinationButton);
			this.Controls.Add(this.destinationTextBox);
			this.Controls.Add(this.destinationLabel);
			this.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "Form1";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "NoBot";
			this.panel1.ResumeLayout(false);
			this.panel1.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

    #endregion

    private System.Windows.Forms.Label destinationLabel;
    private System.Windows.Forms.TextBox destinationTextBox;
    private System.Windows.Forms.Button searchDestinationButton;
    private System.Windows.Forms.CheckBox createShortcutsCheckBox;
    private System.Windows.Forms.Panel panel1;
    private System.Windows.Forms.Label copyrightLabel;
    private System.Windows.Forms.LinkLabel homepageLinkLabel;
    private System.Windows.Forms.Button startInstallButton;
    private System.Windows.Forms.ProgressBar progressBar;
    private System.Windows.Forms.Label titleLabel;
    private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog;
    private System.ComponentModel.BackgroundWorker backgroundWorker;
    private System.Windows.Forms.CheckBox startMenuCheckBox;
  }
}

