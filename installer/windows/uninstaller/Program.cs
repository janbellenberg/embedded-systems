using Microsoft.Win32;
using System;
using System.IO;
using System.Windows.Forms;
using System.Diagnostics;

namespace uninstaller
{
  internal static class Program
  {
    /// <summary>
    /// Der Haupteinstiegspunkt für die Anwendung.
    /// </summary>
    [STAThread]
    static void Main()
    {

      Application.EnableVisualStyles();
      Application.SetCompatibleTextRenderingDefault(false);

      DialogResult res = MessageBox.Show("Möchten Sie NoBot wirklich deinstallieren?", "NoBot", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

      if (res == DialogResult.No) { return; }

      Registry.LocalMachine.DeleteSubKeyTree(@"SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\NoBot", false);

      File.Delete(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Desktop), "NoBot.lnk"));
      File.Delete(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "Microsoft", "Windows", "Start Menu", "Programs", "NoBot.lnk"));

      string appDataPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "NoBot");
      string installPath = "";
      if (File.Exists(Path.Combine(appDataPath, "install.path")))
      {
        FileStream fs = new FileStream(Path.Combine(appDataPath, "install.path"), FileMode.Open);
        StreamReader sr = new StreamReader(fs);
        installPath = sr.ReadToEnd();
        sr.Close();
        fs.Close();
      }

    if (Directory.Exists(appDataPath))
      Directory.Delete(appDataPath, true);

      MessageBox.Show("NoBot wurde erfolgreich entfernt.", "NoBot", MessageBoxButtons.OK, MessageBoxIcon.Information);

      if (Directory.Exists(installPath))
        Process.Start(Environment.GetFolderPath(Environment.SpecialFolder.Windows) + "\\System32\\cmd.exe", "/C pushd \"C:\" > nul & rmdir \"" + installPath + "\" /Q /S");

    }
  }
}
