CREATE DATABASE IF NOT EXISTS nobot CHARACTER SET utf8 COLLATE utf8_german2_ci;

USE nobot;

CREATE TABLE IF NOT EXISTS users (
    `id` INT UNSIGNED AUTO_INCREMENT,
    `username` VARCHAR(50) NOT NULL UNIQUE,
    `password` VARCHAR(128) NOT NULL,
    `salt` VARCHAR(40) NOT NULL,
    `darkmode` TINYINT(1) NOT NULL DEFAULT 0,
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS devices (
    `id` INT UNSIGNED AUTO_INCREMENT,
    `name` VARCHAR(50) NOT NULL,
    `mac` VARCHAR(17) NOT NULL UNIQUE,
    `userID` INT UNSIGNED NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (userID) REFERENCES users(id) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE IF NOT EXISTS programs(
    `id` INT UNSIGNED AUTO_INCREMENT,
    `name` VARCHAR(50) NOT NULL,
    `content` TEXT NOT NULL,
    `userID` INT UNSIGNED NOT NULL,
    PRIMARY KEY(id),
    FOREIGN KEY (userID) REFERENCES users(id) ON DELETE CASCADE ON UPDATE CASCADE
);